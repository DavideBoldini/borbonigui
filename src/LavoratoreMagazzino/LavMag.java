package LavoratoreMagazzino;

import java.util.List;

import Reformat.Reformat;

public class LavMag {
	private String codL;
	private String cf;
	private String nome;
	private String cognome;
	private String dataN;
	private String telefono;
	private String via;
	private int num;
	private String citt�;
	private int salario;
	private String tipo;
	private String magazzino;
	
	//COSTRUTTORE
	
	public LavMag(List<String> lv) {
		super();
		this.codL = lv.get(0);
		this.cf = lv.get(1);
		this.nome = lv.get(2);
		this.cognome = lv.get(3);
		this.dataN = lv.get(4);
		this.telefono = lv.get(5);
		this.via = lv.get(6);
		this.num = Integer.parseInt(lv.get(7));
		this.citt� = lv.get(8);
		this.salario = Integer.parseInt(lv.get(9));
	    this.magazzino = lv.get(10);
		this.tipo = lv.get(11);
	}

	public String getCodL() {
		return Reformat.reformatString(codL);
	}

	public void setCodL(String codL) {
		this.codL = codL;
	}

	public String getCf() {
		return Reformat.reformatString(cf);
	}

	public void setCf(String cf) {
		this.cf = cf;
	}

	public String getNome() {
		return Reformat.reformatString(this.nome);
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return Reformat.reformatString(this.cognome);
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getDataN() {
		return Reformat.reformatString(this.dataN);
	}

	public void setDataN(String dataN) {
		this.dataN = dataN;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getVia() {
		return Reformat.reformatString(this.via);
	}

	public void setVia(String via) {
		this.via = via;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getCitt�() {
		return Reformat.reformatString(this.citt�);
	}

	public void setCitt�(String citt�) {
		this.citt� = citt�;
	}

	public int getSalario() {
		return salario;
	}

	public void setSalario(int salario) {
		this.salario = salario;
	}

	public String getTipo() {
		return Reformat.reformatString(tipo);
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getMagazzino() {
		return Reformat.reformatString(magazzino);
	}

	public void setMagazzino(String magazzino) {
		this.magazzino = magazzino;
	}

}
