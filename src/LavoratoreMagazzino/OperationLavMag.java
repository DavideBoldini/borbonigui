package LavoratoreMagazzino;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//
public class OperationLavMag {

	private Connection con;
	private Statement st;
	private PreparedStatement stVar;
	private ResultSet rs;

	public OperationLavMag() throws ClassNotFoundException, SQLException {
		super();
		this.gateBDopen();
	}

	private void gateBDopen() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		 this.con =
		 DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root",
		"Texhnolyze@99");
		//this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Boldini99");
		this.st = this.con.createStatement();
	}

	private void gateBDclose() throws SQLException {
		this.con.close();
	}

	public ResultSet getRS_LavMag() {
		try {
			String query = "select * from lavoratore_magazzino";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	public ResultSet getRS_LavOP(String NomeMag) {
		try {
			String query = "select * from lavoratore_magazzino where lavoratore_magazzino.Tipo = 'Operaio' and lavoratore_magazzino.NomeMagazzino = '"
					+ NomeMag + "'";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	public ResultSet getRS_MvgSalario(String NomeMag) {
		try {
			String query = "select avg(Salario) as SalarioMedio from lavoratore_magazzino where lavoratore_magazzino.NomeMagazzino = '"
					+ NomeMag + "'";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	public ResultSet getRS_MaxSalario() {
		try {
			String query = "select max(Salario) from lavoratore_magazzino as StipMaxMagazzino";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	public void MassiveDelete(String NomeMag) {
		try {
			String query;
			query = "delete from lavoratore_magazzino where NomeMagazzino = '" + NomeMag + "'";
			this.st.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void DeleteLavMag(String id) {
		try {
			String query;
			query = "delete from lavoratore_magazzino where CodiceLavoratore = '" + id + "'";
			this.st.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean insertLavMag(LavMag lv) throws SQLException {
		try {
			String query;
			String op = "'Operaio'";
			String mag = "'Magazziniere'";
			String ct = "'Capo Turno'";
			if (lv.getTipo().contentEquals(op) && lv.getCodL().charAt(1) == 'O') {
				query = "insert lavoratore_magazzino values(" + lv.getCodL() + "," + lv.getCf() + "," + lv.getNome()
						+ "," + lv.getCognome() + "," + lv.getDataN() + "," + lv.getTelefono() + "," + lv.getVia() + ","
						+ lv.getNum() + "," + lv.getCitt�() + "," + lv.getSalario() + "," + lv.getTipo() + ","
						+ lv.getMagazzino() + ")";
				this.stVar = this.con.prepareStatement(query);
				this.stVar.executeUpdate();
			} else if (lv.getTipo().contentEquals(mag) && lv.getCodL().charAt(1) == 'M') {
				query = "insert lavoratore_magazzino values(" + lv.getCodL() + "," + lv.getCf() + "," + lv.getNome()
						+ "," + lv.getCognome() + "," + lv.getDataN() + "," + lv.getTelefono() + "," + lv.getVia() + ","
						+ lv.getNum() + "," + lv.getCitt�() + "," + lv.getSalario() + "," + lv.getTipo() + ","
						+ lv.getMagazzino() + ")";
				this.stVar = this.con.prepareStatement(query);
				this.stVar.executeUpdate();
			} else if (lv.getTipo().contentEquals(ct) && lv.getCodL().charAt(1) == 'C') {
				query = "insert lavoratore_magazzino values(" + lv.getCodL() + "," + lv.getCf() + "," + lv.getNome()
						+ "," + lv.getCognome() + "," + lv.getDataN() + "," + lv.getTelefono() + "," + lv.getVia() + ","
						+ lv.getNum() + "," + lv.getCitt�() + "," + lv.getSalario() + "," + lv.getTipo() + ","
						+ lv.getMagazzino() + ")";
				this.stVar = this.con.prepareStatement(query);
				this.stVar.executeUpdate();

			}

			else {
				System.out.print("ERROR");
				return false;
			}
		} catch (Exception e) {
			//System.out.println("ERROR_EXP");
			e.printStackTrace();
			return false;
		}
		return true;
	}

}