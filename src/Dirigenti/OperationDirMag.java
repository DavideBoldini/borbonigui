package Dirigenti;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import LavoratoreMagazzino.LavMag;

public class OperationDirMag {
    private Connection con;
    private Statement st;
    private PreparedStatement stVar;
    private ResultSet rs;

    public OperationDirMag() throws ClassNotFoundException, SQLException {
        super();
        this.gateBDopen();
    }

    private void gateBDopen() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        // this.con =
        // DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root",
        // "Texhnolyze@99");
        this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Boldini99");
        this.st = this.con.createStatement();
    }

    private void gateBDclose() throws SQLException {
        this.con.close();
    }

    public ResultSet getRS_DirMag() {
        try {
            String query = "select * from dirigente_magazzino";
            this.stVar = this.con.prepareStatement(query);
            this.rs = this.stVar.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.rs;
    }

    public ResultSet getRS_MaxSalario() {
        try {
            String query = "select max(Salario) from dirigente_magazzino as StipMaxMagazzino";
            this.stVar = this.con.prepareStatement(query);
            this.rs = this.stVar.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.rs;
    }

    public void DeleteDirMag(String id) {
        try {
            String query;
            query = "delete from dirigente_magazzino where CodiceLavoratore = '" + id + "'";
            this.st.executeUpdate(query);
        } catch (Exception e) {
        	e.printStackTrace();
        }
    }

    public boolean insertDirMag(DirMag dirMag) throws SQLException {
        String query;
        try {
            if (dirMag.getCodL().charAt(1) == 'D' && dirMag.getCodL().charAt(2) == 'M') {

                query = "insert dirigente_magazzino values(" + dirMag.getCodL() + "," + dirMag.getCf() + ","
                        + dirMag.getNome() + "," + dirMag.getCognome() + "," + dirMag.getDataN() + ","
                        + dirMag.getTelefono() + "," + dirMag.getVia() + "," + dirMag.getNum() + "," + dirMag.getCitt�()
                        + "," + dirMag.getSalario() + "," + dirMag.getMagazzino() + ")";
                this.stVar = this.con.prepareStatement(query);
                this.stVar.executeUpdate();
            } else {
                System.out.print("ERROR");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("ERROR_EXP");
            return false;
        }
        return true;
    }
}
