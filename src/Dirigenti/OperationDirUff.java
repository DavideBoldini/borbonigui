package Dirigenti;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class OperationDirUff {

	private Connection con;
	private Statement st;
	private PreparedStatement stVar;
	private ResultSet rs;

	public OperationDirUff() throws ClassNotFoundException, SQLException {
		super();
		this.gateBDopen();
	}

	private void gateBDopen() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		// this.con
		// =DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb",
		// "root","Texhnolyze@99");
		this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Boldini99");
		this.st = this.con.createStatement();
	}

	// CLOSEDB
	private void gateBDclose() throws SQLException {
		this.con.close();
	}

	public ResultSet getRS_DirUff() {
		try {
			String query = "select * from dirigente_ufficio";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	public ResultSet getSingDir(String id) {
		try {
			String query = "select * from dirigente_ufficio where CodiceLavoratore = '" + id + "'";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	public ResultSet getRS_MaxSalario() {
		try {
			String query = "select max(Salario) from dirigente_ufficio as StipMaxMagazzino";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	public void deleteDirUff(String id) throws SQLException {
		try {
			String query;
			query = "delete from dirigente_ufficio where CodiceLavoratore = '" + id + "'";
			this.st.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean insertDirUff(DirUff dirUff) throws SQLException {
		try {
			if (dirUff.getCodL().charAt(1) == 'D' && dirUff.getCodL().charAt(2) == 'U') {
				if (dirUff.getTipoUff() == "Acquisti" && dirUff.getCodUff().charAt(1) == 'A') {
					String query;
					query = "insert dirigente_ufficio values(" + dirUff.getCodL() + "," + dirUff.getCf() + ","
							+ dirUff.getNome() + "," + dirUff.getCognome() + "," + dirUff.getDataN() + ","
							+ dirUff.getTelefono() + "," + dirUff.getVia() + "," + dirUff.getNum() + ","
							+ dirUff.getCitt�() + "," + dirUff.getSalario() + "," + dirUff.getCodUff() + "," + "NULL"
							+ "," + "NULL" + ")";
					this.stVar = this.con.prepareStatement(query);
					this.stVar.executeUpdate();
				} else if (dirUff.getTipoUff() == "Vendite" && dirUff.getCodUff().charAt(1) == 'V') {
					String query;
					query = "insert dirigente_ufficio values(" + dirUff.getCodL() + "," + dirUff.getCf() + ","
							+ dirUff.getNome() + "," + dirUff.getCognome() + "," + dirUff.getDataN() + ","
							+ dirUff.getTelefono() + "," + dirUff.getVia() + "," + dirUff.getNum() + ","
							+ dirUff.getCitt�() + "," + dirUff.getSalario() + "," + "NULL" + "," + dirUff.getCodUff()
							+ "," + "NULL" + ")";
					this.stVar = this.con.prepareStatement(query);
					this.stVar.executeUpdate();
				} else if (dirUff.getTipoUff() == "Contabilit�" && dirUff.getCodUff().charAt(1) == 'C') {
					String query;
					query = "insert dirigente_ufficio values(" + dirUff.getCodL() + "," + dirUff.getCf() + ","
							+ dirUff.getNome() + "," + dirUff.getCognome() + "," + dirUff.getDataN() + ","
							+ dirUff.getTelefono() + "," + dirUff.getVia() + "," + dirUff.getNum() + ","
							+ dirUff.getCitt�() + "," + dirUff.getSalario() + "," + "NULL" + "," + "NULL" + ","
							+ dirUff.getCodUff() + ")";
					this.stVar = this.con.prepareStatement(query);
					this.stVar.executeUpdate();
				} else {
					System.out.print("ERROR");
					return false;
				}
			} else {
				System.out.print("ERROR");
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("ERROR_EXP");
			return false;
		}
		return true;
	}

}
