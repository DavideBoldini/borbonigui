package Dirigenti;

import java.util.List;

import Reformat.Reformat;

public class DirUff {
	private String codL;
	private String cf;
	private String nome;
	private String cognome;
	private String dataN;
	private String telefono;
	private String via;
	private int num;
	private String citt�;
	private int salario;
	private String tipoUff;
	private String CodUff;

	
	public DirUff(List<String> imp) {
		this.codL = imp.get(0);
		this.cf = imp.get(1);
		this.nome = imp.get(2);
		this.cognome = imp.get(3);
		this.dataN = imp.get(4);
		this.telefono = imp.get(5);
		this.via = imp.get(6);
		this.num = Integer.parseInt(imp.get(7));
		this.citt� = imp.get(8);
		this.salario = Integer.parseInt(imp.get(9));
		this.CodUff = imp.get(10);
		this.tipoUff = imp.get(11);	        		
	}

	public String getCodL() {
		return Reformat.reformatString(codL);
	}

	public void setCodL(String codL) {
		this.codL = codL;
	}

	public String getCf() {
		return Reformat.reformatString(cf);
	}

	public void setCf(String cf) {
		this.cf = cf;
	}

	public String getNome() {
		return Reformat.reformatString(this.nome);
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return Reformat.reformatString(this.cognome);
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getDataN() {
		return Reformat.reformatString(this.dataN);
	}

	public void setDataN(String dataN) {
		this.dataN = dataN;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getVia() {
		return Reformat.reformatString(this.via);
	}

	public void setVia(String via) {
		this.via = via;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getCitt�() {
		return Reformat.reformatString(this.citt�);
	}

	public void setCitt�(String citt�) {
		this.citt� = citt�;
	}

	public int getSalario() {
		return salario;
	}

	public void setSalario(int salario) {
		this.salario = salario;
	}

	public String getTipoUff() {
		return tipoUff;
	}

	public void setTipoUffA(String codUff) {
		this.tipoUff = codUff;
	}


	public String getCodUff() {
		return Reformat.reformatString(this.CodUff);
	}

	public void setCodUff(String codUff) {
		CodUff = codUff;
	}
	
}
