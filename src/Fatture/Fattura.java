package Fatture;

import java.util.Calendar;
import java.util.List;

import Reformat.Reformat;

public class Fattura {
	private String tipoOrdine;
	private int numOrdine;
	private int annoOrdine;
	private String data;
	private String codUffCont;
	
	public Fattura(List<String> fatt) {
		super();
		this.numOrdine = Integer.parseInt(fatt.get(0));
		this.data = fatt.get(1);
		this.codUffCont = fatt.get(2);
		this.tipoOrdine = fatt.get(3);		
		this.annoOrdine = Calendar.getInstance().get(Calendar.YEAR);				
	}

	public String getTipoOrdine() {
		return tipoOrdine;
	}

	public void setTipoOrdine(String tipoOrdine) {
		this.tipoOrdine = tipoOrdine;
	}

	public int getNumOrdine() {
		return numOrdine;
	}

	public void setNumOrdine(int numOrdine) {
		this.numOrdine = numOrdine;
	}

	public String getData() {
		return Reformat.reformatString(data);
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getCodUffCont() {
		return Reformat.reformatString(codUffCont);
	}

	public void setCodUffCont(String codUffCont) {
		this.codUffCont = codUffCont;
	}

	public int getAnnoOrdine() {
		return annoOrdine;
	}

	public void setAnnoOrdine(int annoOrdine) {
		this.annoOrdine = annoOrdine;
	}
	
	
	
	
	

}
