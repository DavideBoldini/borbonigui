package Fatture;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Magazzino.Magazzino;
//
public class OperationFattura {
	private Connection con;
	private Statement st;
	private PreparedStatement stVar;
	private ResultSet rs;

	public OperationFattura() throws ClassNotFoundException, SQLException {
		super();
		this.gateBDopen();
	}

	// OPENDB
	private void gateBDopen() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Texhnolyze@99");
		// this.con =
		// DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root",
		// "Boldini99");
		this.st = this.con.createStatement();
	}

	private void gateBDclose() throws SQLException {
		this.con.close();
	}

	public ResultSet getRS_Fatt() {
		try {
			String query = "select * from fattura";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	public String getRS_NewNumFatt() {
		int newFatt = 0;
		try {
			String query = "select Numero from fattura";
			this.stVar = this.con.prepareStatement(query);
			ResultSet or = this.stVar.executeQuery();
			while(or.next()) {
			newFatt = or.getInt(1);
			}	
			newFatt++;		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Integer.toString(newFatt);	
	}
	
	public ResultSet getRS_FattSem(int semestre) {
		try {
			String query;
			if (semestre == 1) {
				query = "select * from fattura F where (F.Data between '2020-01-01' and '2020-06-01')";
				this.stVar = this.con.prepareStatement(query);
				this.rs = this.stVar.executeQuery();
			} else {
				query = "select * from fattura F where (F.Data between '2020-06-02' and '2020-12-31')";
				this.stVar = this.con.prepareStatement(query);
				this.rs = this.stVar.executeQuery();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	public boolean insertFatt(Fattura fa) throws SQLException {
		try {
			String query;
			if (fa.getTipoOrdine().contentEquals("Vendita")) {
				query = "insert fattura values(default, " + fa.getNumOrdine() + "," + fa.getAnnoOrdine()
						+ ", NULL, NULL, " + fa.getData() + "," + fa.getCodUffCont() + ")";
				this.stVar = this.con.prepareStatement(query);
				this.stVar.executeUpdate();
			} else {
				query = "insert fattura values(default, NULL, NULL, " + fa.getNumOrdine() + "," + fa.getAnnoOrdine()
						+ "," + fa.getData() + "," + fa.getCodUffCont() + ")";
				this.stVar = this.con.prepareStatement(query);
				this.stVar.executeUpdate();
			}
		} catch (Exception e) {
			// System.out.println("ERROR_EXP");
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
