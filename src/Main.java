import java.sql.SQLException;

import Dirigenti.DirMag;
import Dirigenti.OperationDirMag;
import GUI.WelcomeMenu;
import GateDB.ConnectDB;
import Impiegato.Impiegato;
import Impiegato.OperationImpiegati;
import LavoratoreMagazzino.LavMag;
import LavoratoreMagazzino.OperationLavMag;
import Magazzino.Magazzino;
import Magazzino.OperationMagazzino;
import Ordine.OperationOrdini;
import Ordine.Ordine;
import Uffici.OperationUff;
import Uffici.Uffici;

public class Main {

	public static void main(String args[]) throws ClassNotFoundException, SQLException {
		WelcomeMenu wMenu = new WelcomeMenu();
		wMenu.run();
		ConnectDB cDB = new ConnectDB();
		cDB.gateBDopen();
		Runtime.getRuntime().addShutdownHook(new Thread() {
		    public void run() {
                        try {
                            cDB.gateBDclose();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
		});
	}
}
