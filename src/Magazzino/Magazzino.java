package Magazzino;

import Reformat.Reformat;

public class Magazzino {
	private String nome;
	private int dimensione;

	public Magazzino(String nome, String dimensione) {
		super();
		this.nome = nome;
		this.dimensione = Integer.parseInt(dimensione);
	}
	

	public String getNome() {
		return Reformat.reformatString(nome);
	}
	public void setNome(String nome) {
		nome = nome;
	}
	public int getDimensione() {
		return dimensione;
	}
	public void setDimensione(int dimensione) {
		dimensione = dimensione;
	}
	
}
