package Magazzino;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class OperationMagazzino {

	private Connection con;
	private Statement st;
	private PreparedStatement stVar;
	private ResultSet rs;

	public OperationMagazzino() throws ClassNotFoundException, SQLException {
		super();
		this.gateBDopen();
	}

	//OPENDB
	private void gateBDopen() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Texhnolyze@99");
		//this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Boldini99");
		this.st = this.con.createStatement();
	}

	private void gateBDclose() throws SQLException {
		this.con.close();
	}

	public ResultSet getRS_Mag() {
		try {
			String query = "select * from magazzino";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	public void DeleteMag(String id) throws SQLException {
		try {
			String query;
			query = "delete from magazzino where Nome = '"+id+"'";
			this.st.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean insertMag(Magazzino mag) throws SQLException {
		try {
			String query;
			query = "insert magazzino values(" + mag.getNome() + "," + mag.getDimensione() + ", default)";
			this.stVar = this.con.prepareStatement(query);
			this.stVar.executeUpdate();
		} catch (Exception e) {
			//System.out.println("ERROR_EXP");
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
