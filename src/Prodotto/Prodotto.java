package Prodotto;

import java.util.List;

import Reformat.Reformat;
//
public class Prodotto {
	private String codProd;
	private String descrizione;
	private String dimensione;
	private float pUnitario;
	private String tipo;
	
	public Prodotto(List<String> prodotto) {
		super();
		this.codProd = prodotto.get(0);
		this.dimensione = prodotto.get(1);
		this.pUnitario = Float.parseFloat(prodotto.get(2));
		this.descrizione = prodotto.get(3);		
		this.tipo = prodotto.get(4);
	}

	public String getCodProd() {
		return Reformat.reformatString(codProd);
	}

	public void setCodProd(String codProd) {
		this.codProd = codProd;
	}

	public String getDescrizione() {
		return Reformat.reformatString(descrizione);
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getDimensione() {
		return dimensione;
	}

	public void setDimensione(String dimensione) {
		this.dimensione = dimensione;
	}

	public float getpUnitario() {
		return pUnitario;
	}

	public void setpUnitario(float pUnitario) {
		this.pUnitario = pUnitario;
	}

	public String getTipo() {
		return Reformat.reformatString(tipo);
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	

}