package Prodotto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Pezzo.Pezzo;

//
public class OperationProdotto {
    private Connection con;
    private Statement st;
    private PreparedStatement stVar;
    private ResultSet rs;

    public OperationProdotto() throws ClassNotFoundException, SQLException {
        super();
        this.gateBDopen();
    }

    private void gateBDopen() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
         this.con =
         DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root",
         "Texhnolyze@99");
        //this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Boldini99");
        this.st = this.con.createStatement();
    }

    // CLOSEDB
    private void gateBDclose() throws SQLException {
        this.con.close();
    }

    public ResultSet getRS_Prodotto() {
        try {
            String query = "select * from prodotto";
            this.stVar = this.con.prepareStatement(query);
            this.rs = this.stVar.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.rs;
    }

    public ResultSet getRS_ProdottoPi�Venduto() {
        try {
            String query = "select distinct count(P.CodiceProdotto) as Venduto, P.CodiceProdotto, P.Descrizione, P.Dimensione, P.Tipo from Prodotto P, prodotto_venduto PV where P.CodiceProdotto = PV.CodiceProdotto group by P.CodiceProdotto order by Venduto desc limit 1";
            this.stVar = this.con.prepareStatement(query);
            this.rs = this.stVar.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.rs;
    }

    public ResultSet getRS_ProdottoSpec(String id) {
        try {
            String query = "select * from prodotto where CodiceProdotto = '" + id + "'";
            this.stVar = this.con.prepareStatement(query);
            this.rs = this.stVar.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.rs;
    }

    public boolean insertProdotto(Prodotto p) throws SQLException {
		try {
		    if((p.getTipo().charAt(1) == 'I' && p.getCodProd().charAt(1) == 'I') ||
		      (p.getTipo().charAt(1) == 'E' && p.getCodProd().charAt(1) == 'E') ) {
			String query;
			query = "insert prodotto values(" + p.getCodProd() + "," +p.getDescrizione()+ "," + p.getDimensione() + "," + p.getpUnitario() + "," + p.getTipo()+")";
			this.stVar = this.con.prepareStatement(query);
			this.stVar.executeUpdate();
		    }
		    else {
		        System.out.println("ERROR");
		        return false;
		    }
		} catch (Exception e) {
			//System.out.println("ERROR_EXP");
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
