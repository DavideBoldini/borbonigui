package Uffici;

import java.util.List;

import Reformat.Reformat;

public class Uffici {
	private String table;
	private String codUff;
	private int dimensione;

	public Uffici(String codUff, String dim ) {
		super();
		this.codUff = codUff;
		this.dimensione = Integer.parseInt(dim);
	}
	
	public String getCodUff() {
		return Reformat.reformatString(codUff);
	}

	public void setCodUff(String codUff) {
		this.codUff = codUff;
	}

	public int getDimensione() {
		return dimensione;
	}

	public void setDimensione(int dimensione) {
		this.dimensione = dimensione;
	}

}
