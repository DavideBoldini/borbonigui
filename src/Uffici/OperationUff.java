package Uffici;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Impiegato.Impiegato;

public class OperationUff {
	private Connection con;
	private Statement st;
	private PreparedStatement stVar;
	private ResultSet rs;

	public OperationUff() throws ClassNotFoundException, SQLException {
		super();
		this.gateBDopen();
	}

	private void gateBDopen() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		//this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Texhnolyze@99");
		this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root","Boldini99");
		this.st = this.con.createStatement();
	}

	// CLOSEDB
	private void gateBDclose() throws SQLException {
		this.con.close();
	}

	public ResultSet getRS_Uff(String table) {
		if(table.charAt(0) == 'A') {
			table = "ufficio_acquisti";
		}
		if(table.charAt(0) == 'V') {
			table = "ufficio_vendite";
		}
		if(table.charAt(0) == 'C') {
			table = "ufficio_contabilitą";
		}
		try {
			String query = "select * from " + table;
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	public void deleteUff(String codUff) throws SQLException {
		try {
			String query;
			if (codUff.charAt(0) == 'A') {
			query = "delete from ufficio_acquisti where CodUfficioAcquisti = '" + codUff + "'";
			this.st.executeUpdate(query);
			}
			else if (codUff.charAt(0) == 'V') {
			query = "delete from ufficio_vendite where CodUfficioVendite = '" + codUff + "'";
			this.st.executeUpdate(query);
			}
			else if (codUff.charAt(0) == 'C') {
			query = "delete from ufficio_contabilitą where CodUfficioContabilitą = '" + codUff + "'";
			this.st.executeUpdate(query);
			}
			else {
				System.out.print("ERROR");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean insertIUff(Uffici uff) throws SQLException {
		try {
			String query;
			if (uff.getCodUff().charAt(1) == 'A') {
				query = "insert ufficio_acquisti values(" + uff.getCodUff() + "," + uff.getDimensione() + ",default"+")";
				this.stVar = this.con.prepareStatement(query);
				this.stVar.executeUpdate();
			} else if (uff.getCodUff().charAt(1) == 'V') {
				query = "insert ufficio_vendite values(" + uff.getCodUff() + "," + uff.getDimensione() +  ",default"+")";
				this.stVar = this.con.prepareStatement(query);
				this.stVar.executeUpdate();
			} else if (uff.getCodUff().charAt(1) == 'C') {
				query = "insert ufficio_contabilitą values(" + uff.getCodUff() + "," + uff.getDimensione() + ",default"+")";
				this.stVar = this.con.prepareStatement(query);
				this.stVar.executeUpdate();
			} else {
				System.out.print("ERROR");
				return false;
			}
		} catch (Exception e) {
			//System.out.println("ERROR_EXP");
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
