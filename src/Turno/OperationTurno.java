package Turno;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Rapporto_Amministrazione.RapAm;

public class OperationTurno {

	private Connection con;
	private Statement st;
	private PreparedStatement stVar;
	private ResultSet rs;

	public OperationTurno() throws ClassNotFoundException, SQLException {
		super();
		this.gateBDopen();
	}

	private void gateBDopen() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Texhnolyze@99");
		//this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Boldini99");
		this.st = this.con.createStatement();
	}

	// CLOSEDB
	private void gateBDclose() throws SQLException {
		this.con.close();
	}

	public ResultSet getRS_Turno() {
		try {
			String query = "select * from turno";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	public ResultSet getRS_turno(String numero) {
		try {
			String query = "select * from turno where turno.Numero = " + numero;
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	public boolean insertTurno(Turno t) throws SQLException {
		try {
			if (t.getCodCt().charAt(1) == 'C') {
				String query;
				query = "insert turno values(default," + t.getGiorno() + "," + t.getFasciaOr() + "," + t.getCodCt()
						+ ")";
				this.stVar = this.con.prepareStatement(query);
				this.stVar.executeUpdate();
				int nRow = 0;
				query = "select count(*) from turno";
				this.stVar = this.con.prepareStatement(query);
				this.rs = this.stVar.executeQuery();
			    while(rs.next()){
			        nRow = rs.getInt("count(*)");
			    }
			    if(nRow == 50) {
			    	System.out.print("TURNI DELLA SETTIMANA FINITI");
			    	query = "truncate turno";
		            this.st.executeUpdate(query);
			    }
			}
			else {
				System.out.print("ERROR");
				return false;
			}
		} catch (Exception e) {
			//System.out.println("ERROR_EXP");
			e.printStackTrace();
			return false;
		}
		return true;

	}

	public void updateTurno(String id, String nCt) throws SQLException {
		String query;
		try {
			query = "update turno set CodiceCapoTurno = '" + nCt + "' where Numero = " + id;
			this.stVar = this.con.prepareStatement(query);
			this.stVar.executeUpdate();
		} catch (Exception e) {
			//System.out.println("ERROE_EXP");
			e.printStackTrace();
		}
	}
}