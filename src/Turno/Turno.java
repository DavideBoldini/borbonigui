package Turno;

import java.util.List;

import Reformat.Reformat;

public class Turno {
	private String giorno;
	private String fasciaOr;
	 private String codCt;
	 
	public Turno(String giorno, String cod, String fascia) {
		super();
		this.giorno = giorno;
		this.fasciaOr = fascia;
		this.codCt =cod;
	}

	public String getFasciaOr() {
		return Reformat.reformatString(fasciaOr);
	}

	public void setFasciaOr(String fasciaOr) {
		this.fasciaOr = fasciaOr;
	}

	public String getGiorno() {
		return Reformat.reformatString(giorno);
	}

	public void setGiorno(String giorno) {
		this.giorno = giorno;
	}

	public String getCodCt() {
		return Reformat.reformatString(codCt);
	}

	public void setCodCt(String codCt) {
		this.codCt = codCt;
	}
	 
	 
}
