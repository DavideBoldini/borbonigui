package Pezzo;
//
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Magazzino.Magazzino;

public class OperationPezzo {
	private Connection con;
	private Statement st;
	private PreparedStatement stVar;
	private ResultSet rs;

	public OperationPezzo() throws ClassNotFoundException, SQLException {
		super();
		this.gateBDopen();
	}

	private void gateBDopen() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Texhnolyze@99");
		//this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Boldini99");
		this.st = this.con.createStatement();
	}

	// CLOSEDB
	private void gateBDclose() throws SQLException {
		this.con.close();
	}

	public ResultSet getRS_Pezzo() {
		try {
			String query = "select * from pezzo";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}
	
	public ResultSet getRS_Inventario() {
		try {
			String query = "select CodicePezzo, Quantit�, Materiale, NomeMagazzino from pezzo";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}
	
	public ResultSet getRS_PezzoPi�Usato() {
		try {
			String query = "select distinct count(P.CodicePezzo) as Usato, P.CodicePezzo, P.Descrizione from Pezzo P, assemblaggio A where P.CodicePezzo = A.CodicePezzo group by A.CodicePezzo order by Usato desc limit 1";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}
	
	public ResultSet getRS_PezzoPi�Acquistato() {
		try {
			String query = "select distinct count(P.CodicePezzo) as Usato, P.CodicePezzo, P.Descrizione from Pezzo P, pezzo_acquistato PA where P.CodicePezzo = PA.CodicePezzo group by PA.CodicePezzo order by Usato desc limit 1";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}
	
	public ResultSet getRS_PezzoSpec(String id) {
		try {
			String query = "select * from pezzo where CodicePezzo = '"+id+"'";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}
	
	public boolean insertPezzo(Pezzo p) throws SQLException {
		try {
		    if(p.getCodPezzo().charAt(1) == 'P') {
			String query;
			query = "insert pezzo values(" + p.getCodPezzo() + ", 0, " +p.getMateriale()+ "," + p.getProvenienza() + "," + p.getDescrizione() + "," + p.getcUnitario() + "," + p.getNomeMagazzino()+")";
			this.stVar = this.con.prepareStatement(query);
			this.stVar.executeUpdate();
		    }
		    else {
		        System.out.println("ERROR");
		        return false;
		    }
		} catch (Exception e) {
			//System.out.println("ERROR_EXP");
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
