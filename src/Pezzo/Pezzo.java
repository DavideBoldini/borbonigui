package Pezzo;
//
import java.util.List;

import Reformat.Reformat;

public class Pezzo {
	private String codPezzo;
	private int quantity;
	private String materiale;
	private String provenienza;
	private String descrizione;
	private float cUnitario;
	private String nomeMagazzino;
	
	public Pezzo(List<String> pezzo) {
		super();
		this.codPezzo = pezzo.get(0);
		this.quantity = 0;
		this.materiale = pezzo.get(1);
		this.provenienza = pezzo.get(2);
		this.cUnitario = Float.parseFloat(pezzo.get(3));
		this.nomeMagazzino = pezzo.get(4);
		this.descrizione = pezzo.get(5);	
	}

	public String getCodPezzo() {
		return Reformat.reformatString(codPezzo);
	}

	public void setCodPezzo(String codPezzo) {
		this.codPezzo = codPezzo;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getMateriale() {
		return Reformat.reformatString(materiale);
	}

	public void setMateriale(String materiale) {
		this.materiale = materiale;
	}

	public String getProvenienza() {
		return Reformat.reformatString(provenienza);
	}

	public void setProvenienza(String provenienza) {
		this.provenienza = provenienza;
	}

	public String getDescrizione() {
		return Reformat.reformatString(descrizione);
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public float getcUnitario() {
		return cUnitario;
	}

	public void setcUnitario(float cUnitario) {
		this.cUnitario = cUnitario;
	}

	public String getNomeMagazzino() {
		return Reformat.reformatString(nomeMagazzino);
	}

	public void setNomeMagazzino(String nomeMagazzino) {
		this.nomeMagazzino = nomeMagazzino;
	}
}
