package Assemblaggio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Magazzino.Magazzino;

public class OperationAssemblaggio {

	Connection con;
	Statement st;
	private PreparedStatement stVar;
	private ResultSet rs;

	public OperationAssemblaggio() throws ClassNotFoundException, SQLException {
		super();
		this.gateBDopen();
	}

	private void gateBDopen() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		//this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Texhnolyze@99");
		this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Boldini99");
		this.st = this.con.createStatement();
	}

	private void gateBDclose() throws SQLException {
		this.con.close();
	}

	public ResultSet getRS_Assemblaggio() {
		try {
			String query = "select * from assemblaggio";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}
	
	public boolean insertAssemblaggio(Assemblaggio a) throws SQLException {
		try {
			String query;
			query = "insert assemblaggio values(" + a.getCodcePezzo() + "," + a.getCodProdotto() + "," + a.getqPezzi()+")";
			this.stVar = this.con.prepareStatement(query);
			this.stVar.executeUpdate();
		} catch (Exception e) {
			//System.out.println("ERROR_EXP");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
}
