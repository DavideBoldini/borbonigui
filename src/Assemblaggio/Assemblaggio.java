package Assemblaggio;

import Reformat.Reformat;

public class Assemblaggio {
	private String codcePezzo;
	private String codProdotto;
	private int qPezzi;
	
	
	public Assemblaggio(String codcePezzo, String codProdotto, String qPezzi) {
		super();
		this.codcePezzo = codcePezzo;
		this.codProdotto = codProdotto;
		this.qPezzi = Integer.parseInt(qPezzi);
	}


	public String getCodcePezzo() {
		return Reformat.reformatString(codcePezzo);
	}


	public void setCodcePezzo(String codcePezzo) {
		this.codcePezzo = codcePezzo;
	}


	public String getCodProdotto() {
		return Reformat.reformatString(codProdotto);
	}


	public void setCodProdotto(String codProdotto) {
		this.codProdotto = codProdotto;
	}


	public int getqPezzi() {
		return qPezzi;
	}


	public void setqPezzi(int qPezzi) {
		this.qPezzi = qPezzi;
	}
	
	
	
	
	
}
