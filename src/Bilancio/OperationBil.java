package Bilancio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;

import Magazzino.Magazzino;

public class OperationBil {

	private Connection con;
	private Statement st;
	private PreparedStatement stVar;
	private ResultSet rs;

	public OperationBil() throws ClassNotFoundException, SQLException {
		super();
		this.gateBDopen();
	}

	// OPENDB
	private void gateBDopen() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		this.con =
		DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root",
		"Texhnolyze@99");
		//this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Boldini99");
		this.st = this.con.createStatement();
	}

	private void gateBDclose() throws SQLException {
		this.con.close();
	}

	public ResultSet getRS_Bil() {
		try {
			String query = "select * from bilancio";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}
	
	public ResultSet getRS_SalarioDip() {
		try {
			String query = "select (select max(LM.Salario) from lavoratore_magazzino LM) as StipMaxMagazzino, (select max(I.Salario) from impiegato I) as StipMaxImpiegato, (select max(DM.Salario) from dirigente_magazzino DM) as StipMaxDirigenteMagazzino, (select max(DU.Salario) from dirigente_ufficio DU) as StipMaxDirigenteUfficio";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	public ResultSet getRS_Util() {
		try {
			int year = Calendar.getInstance().get(Calendar.YEAR);
			String query = "select Attivo - Passivo from bilancio where Anno = " + year;
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	public ResultSet getRS_UtilMax() {
		try {
			String query = "select B.Anno, max(B.Attivo-B.Passivo) as Utile from bilancio B group by B.Anno order by Utile desc limit 1";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	// ATTENZIONEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE

	public boolean insertBil(Bilancio b) throws SQLException {
		try {
			String query;
			int attivo = 0;
			int passivo = 0;
			query = "select sum(importo) from ordine_vendita where ordine_vendita.Anno = " + b.getYear();
			ResultSet rsAtt = this.st.executeQuery(query);
			while (rsAtt.next()) {
				attivo = rsAtt.getInt(1);
			}
			query = "select sum(importo) from ordine_acquisto where ordine_acquisto.Anno = " + b.getYear();
			ResultSet rsPass = this.st.executeQuery(query);
			while (rsPass.next()) {
				passivo = rsAtt.getInt(1);
			}
			query = "insert bilancio values(" + b.getYear() + "," + attivo + "," + passivo + "," + b.getSocio() + ")";
			this.stVar = this.con.prepareStatement(query);
			this.stVar.executeUpdate();
		} catch (Exception e) {
			// System.out.println("ERROR_EXP");
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
