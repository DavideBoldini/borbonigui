package Bilancio;

import java.util.Calendar;

import Reformat.Reformat;

public class Bilancio {
	private int year;
	private String socio;
	
	public Bilancio(String socio) {
		super();
		this.year = Calendar.getInstance().get(Calendar.YEAR);
		this.socio = socio;
	}
	public int getYear() {
		return year;
	}
	public String getSocio() {
		return Reformat.reformatString(socio);
	}
	public void setSocio(String socio) {
		this.socio = socio;
	}

}
