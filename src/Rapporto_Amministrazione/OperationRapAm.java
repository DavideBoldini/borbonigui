package Rapporto_Amministrazione;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Magazzino.Magazzino;

public class OperationRapAm {
	private Connection con;
	private Statement st;
	private PreparedStatement stVar;
	private ResultSet rs;

	public OperationRapAm() throws ClassNotFoundException, SQLException {
		super();
		this.gateBDopen();
	}

	private void gateBDopen() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		//this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Texhnolyze@99");
		this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root","Boldini99");
		this.st = this.con.createStatement();
	}

	// CLOSEDB
	private void gateBDclose() throws SQLException {
		this.con.close();
	}

	public ResultSet getRS_Rap() {
		try {
			String query = "select * from rapporto_amministrazione";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}
	
	public ResultSet getRS_RapSing(String socio) {
		try {
			String query = "select * from rapporto_amministrazione where rapporto_amministrazione.CodiceSocio = '"+socio+"'";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}
	
	public boolean insertRap(RapAm rp) throws SQLException {
		try {
			String query;
			query = "insert rapporto_amministrazione values(" + rp.getDate() + "," + rp.getDesc() + "," + rp.getCodDir() + "," + rp.getCodSocio() +")";
			this.stVar = this.con.prepareStatement(query);
			this.stVar.executeUpdate();
		} catch (Exception e) {
			//System.out.println("ERROR_EXP");
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
