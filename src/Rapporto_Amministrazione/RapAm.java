package Rapporto_Amministrazione;

import java.util.List;

import Reformat.Reformat;

public class RapAm {
		
	private String date;
	private String desc;
	private String codDir;
	private String codSocio;
	
	
	public RapAm(List<String> rp) {
		super();
		this.date = rp.get(0);
		this.desc = rp.get(1);
		this.codDir = rp.get(2);
		this.codSocio = rp.get(3);
	}


	public String getDate() {
		return Reformat.reformatString(date);
	}


	public void setDate(String date) {
		this.date = date;
	}


	public String getDesc() {
		return Reformat.reformatString(desc);
	}


	public void setDesc(String desc) {
		this.desc = desc;
	}


	public String getCodDir() {
		return Reformat.reformatString(codDir);
	}


	public void setCodDir(String codDir) {
		this.codDir = codDir;
	}


	public String getCodSocio() {
		return Reformat.reformatString(codSocio);
	}


	public void setCodSocio(String codSocio) {
		this.codSocio = codSocio;
	}
	
	
	

}
