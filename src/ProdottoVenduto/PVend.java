package ProdottoVenduto;

import java.util.Calendar;

import Reformat.Reformat;

public class PVend {

	private String codProd;
	private String quantity;
	private int year;
	
	
	public PVend(String codProd, String quantity) {
		super();
		this.codProd = codProd;
		this.quantity = quantity;
		this.year = Calendar.getInstance().get(Calendar.YEAR);
	}


	public String getCodProd() {
		return Reformat.reformatString(codProd);
	}


	public void setCodProd(String codProd) {
		this.codProd = codProd;
	}


	public String getQuantity() {
		return quantity;
	}


	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	
	public int getYear() {
		return year;
	}
	
	
	
	
}
