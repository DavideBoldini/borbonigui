package ProdottoVenduto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Ordine.Ordine;
import Reformat.Pair;

public class OperationPVend {
    Connection con;
    Statement st;
    private PreparedStatement stVar;
    private ResultSet rs;
    private List<Integer> quantity = new ArrayList<Integer>();
    int i = 0;

    public OperationPVend() throws ClassNotFoundException, SQLException {
        super();
        this.gateBDopen();
    }

    private void gateBDopen() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
         //this.con =
         //DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root",
         //"Texhnolyze@99");
        this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Boldini99");
        this.st = this.con.createStatement();
    }

    private void gateBDclose() throws SQLException {
        this.con.close();
    }

    public ResultSet getRS_PVend() {
        try {
            String query = "select * from prodotto_venduto";
            this.stVar = this.con.prepareStatement(query);
            this.rs = this.stVar.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.rs;
    }
    //Test
    public ResultSet getRS_PVendInOrd(String nOrd) {
        try {
            String query = "select * from prodotto_venduto where NumOrdine = " + nOrd;
            this.stVar = this.con.prepareStatement(query);
            this.rs = this.stVar.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.rs;
    }

    public void deletePVendFromOrder(String nOrd, String idProd) throws SQLException {
        try {
            String query;
            query = "delete from prodotto_venduto where NumOrdine = " + nOrd + " and CodiceProdotto = '" + idProd + "'";
            this.st.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean insertPVend(String nOrd, PVend pv) throws SQLException {
        try {
            // determino il pTot del prodotto

            int p = 0;
            List<String> pezzi;
            String query = "select (P.PrezzoUnitario * " + pv.getQuantity()
                    + ") from prodotto P where P.CodiceProdotto = " + pv.getCodProd();
            ResultSet pTot = this.st.executeQuery(query);
            while (pTot.next()) {
                p = pTot.getInt(1);
            }
            List<Pair<String, Integer>> pezziQuantit� = new ArrayList<>();
            query = "select CodicePezzo, Quantit�Pezzi from assemblaggio where assemblaggio.CodiceProdotto = "
                    + pv.getCodProd();
            ResultSet assemblaggio = this.st.executeQuery(query);
            while(assemblaggio.next()) {
            	Pair<String,Integer> elem = new Pair<>(assemblaggio.getString(1),assemblaggio.getInt(2)*Integer.parseInt(pv.getQuantity()));
            	pezziQuantit�.add(elem);
            }
            query = "select CodicePezzo, Quantit� from Pezzo";
            ResultSet quantit�Prec = this.st.executeQuery(query);
            while(quantit�Prec.next()) {
            	for(Pair<String,Integer> elem : pezziQuantit�) {
            		if(elem.getFst().equals(quantit�Prec.getString(1)) && elem.getSnd() > quantit�Prec.getInt(2)) {
            			throw new IllegalArgumentException();
            		}
            	}
            }
            query = "insert prodotto_venduto values('" + nOrd + "'," + pv.getYear() + "," + pv.getCodProd() + ","
                    + pv.getQuantity() + "," + p + ")";
            this.stVar = this.con.prepareStatement(query);
            this.stVar.executeUpdate();

            // update del numero dei pezzi
            pezzi = this.PezziToUpdate(pv.getCodProd());
            for (String pezzo : pezzi) {
                this.UpdatePezzo(pezzo, pv);
            }
            this.i = 0;
        } catch (Exception e) {
            // System.out.println("ERROR_EXP");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void UpdatePezzo(String pezzo, PVend pv) {
        int qInOrdine = Integer.parseInt(pv.getQuantity());
        int qDaSot = qInOrdine * this.quantity.get(this.i);
        String query = "update pezzo set Quantit� = Quantit� - " + qDaSot + " where CodicePezzo = '" + pezzo + "'";
        try {
            this.st.executeUpdate(query);
        } catch (SQLException e) {
        	
            e.printStackTrace();
        }
        i++;
    }

    private List<String> PezziToUpdate(String codProd) throws SQLException {
        // trovo i pezzi da modificare e le quantit� contenute nell'assemblaggio
        String query = "select CodicePezzo, Quantit�Pezzi from assemblaggio where assemblaggio.CodiceProdotto = "
                + codProd;
        ResultSet pz = this.st.executeQuery(query);
        List<String> pezzi = new ArrayList<>();
        while (pz.next()) {
            pezzi.add(pz.getString(1));
            this.quantity.add(pz.getInt(2));
        }
        return pezzi;
    }
}
