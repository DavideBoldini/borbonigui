package PezzoAcquistato;

import java.util.Calendar;

import Reformat.Reformat;

public class PAcquistato {
	private String codPezzo;
	private String quantity;
	private int year;
	
	
	public PAcquistato(String codProd, String quantity) {
		super();
		this.codPezzo = codProd;
		this.quantity = quantity;
		this.year = Calendar.getInstance().get(Calendar.YEAR);
	}


	public String getCodPezzo() {
		return Reformat.reformatString(codPezzo);
	}


	public void setCodPezzo(String codProd) {
		this.codPezzo = codProd;
	}


	public String getQuantity() {
		return quantity;
	}


	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	
	public int getYear() {
		return year;
	}

}
