package PezzoAcquistato;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import ProdottoVenduto.PVend;

public class OperationPAcq {
	Connection con;
	Statement st;
	private PreparedStatement stVar;
	private ResultSet rs;

	public OperationPAcq() throws ClassNotFoundException, SQLException {
		super();
		this.gateBDopen();
	}

	private void gateBDopen() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		//this.con =DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Texhnolyze@99");
		this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Boldini99");
		this.st = this.con.createStatement();
	}

	private void gateBDclose() throws SQLException {
		this.con.close();
	}

	public ResultSet getRS_PAcq() {
		try {
			String query = "select * from pezzo_acquistato";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	public ResultSet getRS_PAcqInOrd(String nOrd) {
		try {
			String query = "select * from pezzo_acquistato where NumOrdine = " + nOrd;
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	public void deletePAcqFromOrder(String nOrd, String idProd) throws SQLException {
		try {
			String query;
			query = "delete from pezzo_acquistato where NumOrdine = " + nOrd + " and CodiceProdotto = '" + idProd + "'";
			this.st.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean insertPAcq(String nOrd, PAcquistato pa) throws SQLException {
		try {
			int p = 0;
			String query = "select (P.CostoUnitario * "+pa.getQuantity()+") from pezzo P where P.CodicePezzo = "+pa.getCodPezzo();
			ResultSet pTot = this.st.executeQuery(query);
			while (pTot.next()) {
				p = pTot.getInt(1);
			}
			query = "insert pezzo_acquistato values(" + pa.getCodPezzo()  + ", '" + nOrd + "' , " + pa.getYear() + ", "+ pa.getQuantity() + ", " + p + ")";
			this.stVar = this.con.prepareStatement(query);
			this.stVar.executeUpdate();

		} catch (Exception e) {
			// System.out.println("ERROR_EXP");
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
