package Resi_e_Reclamo;

import Reformat.Reformat;

public class Reclamo {

	private int NumOrdine;
	private String data;
	private String descrizione;
	
	
	public Reclamo(int numOrdine, String data, String descrizione) {
		super();
		this.NumOrdine = numOrdine;
		this.data = data;
		this.descrizione = descrizione;
	}


	public int getNumOrdine() {
		return NumOrdine;
	}

	public void setNumOrdine(int numOrdine) {
		NumOrdine = numOrdine;
	}

	public String getData() {
		return Reformat.reformatString(data);
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getDescrizione() {
		return Reformat.reformatString(descrizione);
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
}
