package Resi_e_Reclamo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;



public class OperationResoReclamo {
	private Connection con;
	private Statement st;
	private PreparedStatement stVar;
	private ResultSet rs;

	public OperationResoReclamo() throws ClassNotFoundException, SQLException {
		super();
		this.gateBDopen();
	}

	//OPENDB
	private void gateBDopen() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		//this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Texhnolyze@99");
		this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Boldini99");
		this.st = this.con.createStatement();
	}

	private void gateBDclose() throws SQLException {
		this.con.close();
	}

	public ResultSet getRS_ResReclamo(String table) {
		String query;
		if(table.contentEquals("Reso")) {
			query = "select * from reso";
		}
		else {
			query = "select * from reclamo";
		}
		try {
			
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}
	
	public void DeleteRes(String num) throws SQLException {
		try {
			String query;
			query = "delete from reso where CodiceReso = "+num;
			this.st.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void DeleteReclamo(String num) throws SQLException {
		try {
			String query;
			query = "delete from reclamo where CodiceReclamo = "+num;
			this.st.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean insertReso(Reso reso) throws SQLException {
		try {
			String query;
			int year = Calendar.getInstance().get(Calendar.YEAR);
			query = "insert reso values( default," + reso.getNumOrdine() + "," + year + "," +reso.getData()+ "," + reso.getDescrizione() + ")";
			this.stVar = this.con.prepareStatement(query);
			this.stVar.executeUpdate();
		} catch (Exception e) {
			//System.out.println("ERROR_EXP");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public String getRS_NewNumReso() {
		int newReso = 0;
		try {
			String query = "select CodiceReso from reso";
			this.stVar = this.con.prepareStatement(query);
			ResultSet or = this.stVar.executeQuery();
			while(or.next()) {
			newReso = or.getInt(1);
			}	
			newReso++;		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Integer.toString(newReso);	
	}
	
	public String getRS_NewNumReclamo() {
		int newReclamo = 0;
		try {
			String query = "select CodiceReclamo from reclamo";
			this.stVar = this.con.prepareStatement(query);
			ResultSet or = this.stVar.executeQuery();
			while(or.next()) {
			newReclamo = or.getInt(1);
			}	
			newReclamo++;		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Integer.toString(newReclamo);	
	}
	
	public boolean insertReclamo(Reclamo reclamo) throws SQLException {
		try {
			String query;
			int year = Calendar.getInstance().get(Calendar.YEAR);
			query = "insert reclamo values( default, " + reclamo.getNumOrdine() + "," + year + "," +reclamo.getData()+ "," + reclamo.getDescrizione() + ")";
			this.stVar = this.con.prepareStatement(query);
			this.stVar.executeUpdate();
		} catch (Exception e) {
			//System.out.println("ERROR_EXP");
			e.printStackTrace();
			return false;
		}
		return true;
	}
}