package Ordine;

import java.util.Calendar;
import java.util.List;

import Reformat.Reformat;

public class Ordine {
	private int quantity;
	private int value;
	private int year;
	private String codOff;


	public Ordine(String codUff) {
		super();
		this.quantity = 0;
		this.value = 0;
		this.year = Calendar.getInstance().get(Calendar.YEAR);
		this.codOff = codUff;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	//GET VALUE
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getYear() {
		return year;
	}

	public String getCodOff() {
		return Reformat.reformatString(this.codOff);
	}

	public void setCodOff(String codOff) {
		this.codOff = codOff;
	}
}
