package Ordine;

import java.sql.*;

public class OperationOrdini {
//Connection � la classe che ci permette di manipolare il database
//Statement ci permette di usare le query
//ResultSet consente di avere il risultato di una query (NON FUNZIONA PER INSERT/DELETE, perch� non ritornano nulla)
//executeUpdate per update, delete o insert
//PreparedStatement per i varchar
	Connection con;
	Statement st;
	private PreparedStatement stVar;
	private ResultSet rs;

	public OperationOrdini() throws ClassNotFoundException, SQLException {
		super();
		this.gateBDopen();
	}

	private void gateBDopen() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Texhnolyze@99");
		//this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Boldini99");
		this.st = this.con.createStatement();
	}

	private void gateBDclose() throws SQLException {
		this.con.close();
	}

	public ResultSet getRS_Ord(String tableName) {
		try {
			String query = "select * from " + tableName;
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}
	
	public ResultSet getRS_OrdSing(String tableName, String id) {
		try {
			String query = "select * from " + tableName + " where NumeroOrdine = "+id;
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	public ResultSet getRS_Storico() {
		try {
			String query = "select * from ordine_acquisto UNION select* from ordine_vendita";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}
	
	public ResultSet getRS_EsterniVendInEstate() {
		try {
			String query = "select count(PV.CodiceProdotto) as NumeroVenditeEstive from prodotto P, prodotto_venduto PV, fattura F where PV.NumOrdine = F.NumeroOrdineVendita and PV.CodiceProdotto = P.CodiceProdotto and (F.Data between '2020-06-01' and '2020-09-01') and P.Tipo = 'Esterno'";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}
	
	public ResultSet getRS_RapportoVendEsterniInterni() {
		try {
			String query = "select P.Tipo, count(PV.NumOrdine) as NumOrdini, concat(round(count(PV.NumOrdine)/(select count(PV1.NumOrdine) from prodotto_venduto PV1)*100,1),'%') as Percentuale from prodotto P, prodotto_venduto PV where PV.CodiceProdotto = P.CodiceProdotto and P.Tipo = 'Esterno' union select P.Tipo, count(PV.NumOrdine) as NumOrdini, concat(round(count(PV.NumOrdine)/(select count(PV1.NumOrdine) from prodotto_venduto PV1)*100,1),'%') as Percentuale from prodotto P, prodotto_venduto PV where PV.CodiceProdotto = P.CodiceProdotto and P.Tipo = 'Interno'";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}
	
	public String getRS_NewOrderVend() {
		int newOrder = 0;
		try {
			String query = "select NumeroOrdine from ordine_vendita";
			this.stVar = this.con.prepareStatement(query);
			ResultSet or = this.stVar.executeQuery();
			while(or.next()) {
			newOrder = or.getInt(1);
			}	
			newOrder++;		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Integer.toString(newOrder);	
	}

	public String getRS_NewOrderAcq() {
		int newOrder = 0;
		try {
			String query = "select NumeroOrdine from ordine_acquisto";
			this.stVar = this.con.prepareStatement(query);
			ResultSet or = this.stVar.executeQuery();
			while(or.next()) {
			newOrder = or.getInt(1);
			}	
			newOrder++;		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Integer.toString(newOrder);	
	}
//DELETE ORDER

	public void deleteOrder(String table, String id) throws SQLException {
		try {
			String query;
			query = "delete from " + table + " where NumeroOrdine =" + id;
			this.st.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//INSERIMENTO

	public boolean insertOrdine(Ordine or) throws SQLException {
		try {
			String query;
			if (or.getCodOff().charAt(1) == 'A') {
				query = "insert ordine_acquisto values(default, " + or.getQuantity() + "," + or.getValue() + ","
						+ or.getYear() + "," + or.getCodOff() + ")";
				this.st.executeUpdate(query);
			} else if (or.getCodOff().charAt(1) == 'V') {
				query = "insert ordine_vendita values(default, " + or.getQuantity() + "," + or.getValue() + ","
						+ or.getYear() + "," + or.getCodOff() + ")";
				this.st.executeUpdate(query);
			} else {
				System.out.print("ERROR");
				return false;
			}
		} catch (Exception e) {
			//System.out.println("ERROR_EXP");
			e.printStackTrace();
			return false;
		}
		return true;
	}
}