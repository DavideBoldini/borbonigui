package Pagamento;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Magazzino.Magazzino;

public class OperationPagamento {


	private Connection con;
	private Statement st;
	private PreparedStatement stVar;
	private ResultSet rs;

	public OperationPagamento() throws ClassNotFoundException, SQLException {
		super();
		this.gateBDopen();
	}

	//OPENDB
	private void gateBDopen() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Texhnolyze@99");
		//this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Boldini99");
		this.st = this.con.createStatement();
	}

	private void gateBDclose() throws SQLException {
		this.con.close();
	}

	public ResultSet getRS_Pagamento() {
		try {
			String query = "select * from pagamento";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}
	
	public String getRS_NewNumPag() {
		int newReso = 0;
		try {
			String query = "select Numero from pagamento";
			this.stVar = this.con.prepareStatement(query);
			ResultSet or = this.stVar.executeQuery();
			while(or.next()) {
			newReso = or.getInt(1);
			}	
			newReso++;		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Integer.toString(newReso);	
	}
	
	
	
	public ResultSet getRS_PagamentoResi500Euro() {
		try {
			String query = "select F.Numero as NumeroFattura, F.NumeroOrdineVendita, F.Data, P.Numero as NumeroPagamento, P.Importo, R.Descrizione, PR.CodiceProdotto, PR.Descrizione from pagamento P, reso R, ordine_vendita OV, fattura F, prodotto_venduto PV, prodotto PR where R.NumOrdine = OV.NumeroOrdine and OV.NumeroOrdine = F.NumeroOrdineVendita and F.Numero = P.FatturaNumero and OV.NumeroOrdine = PV.NumOrdine and PV.CodiceProdotto = PR.CodiceProdotto and OV.Importo > 500";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}
	
	public boolean insertPag(Pagamento pag) throws SQLException {
		try {
			String query;
			String importo = "select case when (select F2.NumeroOrdineVendita from fattura F2 where F2.Numero = "+ pag.getNumFattura()+") is null then (select OA.Importo from ordine_acquisto OA, fattura F1 where OA.NumeroOrdine = F1.NumeroOrdineAcquisto and F1.Numero = "+ pag.getNumFattura()+") else (select OV.Importo from ordine_vendita OV, fattura F1 where OV.NumeroOrdine = F1.NumeroOrdineVendita and F1.Numero = "+ pag.getNumFattura()+") end as importo from fattura F group by importo limit 1)";
			query = "insert pagamento values(default, " + pag.getNumFattura() + "," + pag.getData() + "," + pag.getTipologia() + ", ("+importo+")";
			this.stVar = this.con.prepareStatement(query);
			this.stVar.executeUpdate();
		} catch (Exception e) {
			//System.out.println("ERROR_EXP");
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
