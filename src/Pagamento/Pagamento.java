package Pagamento;

import java.util.List;

import Reformat.Reformat;

public class Pagamento {
	private int numFattura;
	private String data;
	private String Tipologia;
	
	public Pagamento(List<String> pagamento) {
		super();
		this.numFattura = Integer.parseInt(pagamento.get(0));
		this.data = pagamento.get(1);
		Tipologia = pagamento.get(2);
	}

	public int getNumFattura() {
		return numFattura;
	}

	public void setNumFattura(int numFattura) {
		this.numFattura = numFattura;
	}

	public String getData() {
		return Reformat.reformatString(data);
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getTipologia() {
		return Reformat.reformatString(Tipologia);
	}

	public void setTipologia(String tipologia) {
		Tipologia = tipologia;
	}
}
