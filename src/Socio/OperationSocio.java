package Socio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;

import LavoratoreMagazzino.LavMag;

public class OperationSocio {

    private Connection con;
    private Statement st;
    private PreparedStatement stVar;
    private ResultSet rs;
    private int anno;
    private int year;

    public OperationSocio() throws ClassNotFoundException, SQLException {
        super();
        this.gateBDopen();
    }

    private void gateBDopen() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        //this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Texhnolyze@99");
         this.con =
         DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root",
         "Boldini99");
        this.st = this.con.createStatement();
    }

    private void gateBDclose() throws SQLException {
        this.con.close();
    }

    public ResultSet getRS_Socio() {
        try {
            String query = "select * from socio";
            this.stVar = this.con.prepareStatement(query);
            this.rs = this.stVar.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.rs;
    }

    public void DeleteSocio(String id) {
        try {
            String query;
            query = "delete from socio where CodiceLavoratore = '" + id + "'";
            this.st.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ResultSet getRS_UtiliSoci() throws SQLException {
        this.year = Calendar.getInstance().get(Calendar.YEAR);
        String query;
        if (this.anno != year) {
            this.anno = this.year;
            query = "truncate guadagno_socio";
            this.st.executeUpdate(query);
            query = "insert into guadagno_socio values (" + year
                    + ", 'S0000001', (select ((B.attivo - B.passivo)*S.Quota)/100 from bilancio B, socio S  where S.CodiceLavoratore = 'S0000001' and B.Anno = "
                    + year + "))";
            this.st.executeUpdate(query);
            query = "insert into guadagno_socio values (" + year
                    + ", 'S0000002', (select ((B.attivo - B.passivo)*S.Quota)/100 from bilancio B, socio S  where S.CodiceLavoratore = 'S0000002' and B.Anno = "
                    + year + "))";
            this.st.executeUpdate(query);
            query = "insert into guadagno_socio values (" + year
                    + ", 'S0000003', (select ((B.attivo - B.passivo)*S.Quota)/100 from bilancio B, socio S  where S.CodiceLavoratore = 'S0000003' and B.Anno = "
                    + year + "))";
            this.st.executeUpdate(query);
        }
        query = "select * from guadagno_socio where Anno = " + year;

        try {
            this.stVar = this.con.prepareStatement(query);
            this.rs = this.stVar.executeQuery();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return rs;
    }

    public boolean insertSocio(Socio s) throws SQLException {
        try {
            String query;
            String sA = "''Produzione_Acquisto''";
            String sV = "'Vendite'";
            String sC = "'Contabilit�'";
            if (s.getCodL().charAt(1) == 'S') {
                if (s.getTipo().contentEquals(sA) && s.getCodL().charAt(1) == 'P') {
                    query = "insert socio values(" + s.getCodL() + "," + s.getCf() + "," + s.getNome() + ","
                            + s.getCognome() + "," + s.getDataN() + "," + s.getTelefono() + "," + s.getVia() + ","
                            + s.getNum() + "," + s.getCitt�() + "," + s.getSalario() + "," + s.getQuota() + ","
                            + s.getTipo() + ")";
                    this.stVar = this.con.prepareStatement(query);
                    this.stVar.executeUpdate();
                } else if (s.getTipo().contentEquals(sV) && s.getCodL().charAt(1) == 'V') {
                    query = "insert socio values(" + s.getCodL() + "," + s.getCf() + "," + s.getNome() + ","
                            + s.getCognome() + "," + s.getDataN() + "," + s.getTelefono() + "," + s.getVia() + ","
                            + s.getNum() + "," + s.getCitt�() + "," + s.getSalario() + "," + s.getQuota() + ","
                            + s.getTipo() + ")";
                    this.stVar = this.con.prepareStatement(query);
                    this.stVar.executeUpdate();
                } else if (s.getTipo().contentEquals(sC) && s.getCodL().charAt(1) == 'C') {
                    query = "insert socio values(" + s.getCodL() + "," + s.getCf() + "," + s.getNome() + ","
                            + s.getCognome() + "," + s.getDataN() + "," + s.getTelefono() + "," + s.getVia() + ","
                            + s.getNum() + "," + s.getCitt�() + "," + s.getSalario() + "," + s.getQuota() + ","
                            + s.getTipo() + ")";
                    this.stVar = this.con.prepareStatement(query);
                    this.stVar.executeUpdate();
                } else {
                    System.out.print("ERROR");
                    return false;
                }
            } else {
                System.out.print("ERROR");
                return false;
            }
        } catch (Exception e) {
            // System.out.println("ERROR_EXP");
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
