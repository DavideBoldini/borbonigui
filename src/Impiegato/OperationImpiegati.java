package Impiegato;

//
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class OperationImpiegati {

	private Connection con;
	private Statement st;
	private PreparedStatement stVar;
	private ResultSet rs;

	public OperationImpiegati() throws ClassNotFoundException, SQLException {
		super();
		this.gateBDopen();
	}

	private void gateBDopen() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		// this.con =
		// DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root",
		// "Texhnolyze@99");
		this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/borbonidb", "root", "Boldini99");
		this.st = this.con.createStatement();
	}

	// CLOSEDB
	private void gateBDclose() throws SQLException {
		this.con.close();
	}

	public ResultSet getRS_Imp() {
		try {
			String query = "select * from impiegato";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	public ResultSet getSingImp(String id) {
		try {
			String query = "select * from impiegato where CodiceLavoratore = '" + id + "'";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

	public ResultSet getRS_MaxSalario() {
		try {
			String query = "select max(Salario) from impiegato as StipMaxMagazzino";
			this.stVar = this.con.prepareStatement(query);
			this.rs = this.stVar.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.rs;
	}

//DELETE

	public void deleteImpiegato(String id) throws SQLException {
		try {
			String query;
			query = "delete from impiegato where CodiceLavoratore = '" + id + "'";
			this.st.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//INSERIMENTO

	public boolean insertImpiegato(Impiegato imp) throws SQLException {
		try {
			if (imp.getCodL().charAt(1) == 'I') {
				if (imp.getTipoUff() == "Acquisti" && imp.getCodUff().charAt(1) == 'A') {
					String query;
					query = "insert impiegato values(" + imp.getCodL() + "," + imp.getCf() + "," + imp.getNome() + ","
							+ imp.getCognome() + "," + imp.getDataN() + "," + imp.getTelefono() + "," + imp.getVia()
							+ "," + imp.getNum() + "," + imp.getCittà() + "," + imp.getSalario() + "," + imp.getCodUff()
							+ "," + "NULL" + "," + "NULL" + ")";
					this.stVar = this.con.prepareStatement(query);
					this.stVar.executeUpdate();
				} else if (imp.getTipoUff() == "Vendite" && imp.getCodUff().charAt(1) == 'V') {
					String query;
					query = "insert impiegato values(" + imp.getCodL() + "," + imp.getCf() + "," + imp.getNome() + ","
							+ imp.getCognome() + "," + imp.getDataN() + "," + imp.getTelefono() + "," + imp.getVia()
							+ "," + imp.getNum() + "," + imp.getCittà() + "," + imp.getSalario() + "," + "NULL" + ","
							+ imp.getCodUff() + "," + "NULL" + ")";
					this.stVar = this.con.prepareStatement(query);
					this.stVar.executeUpdate();
				} else if (imp.getTipoUff() == "Contabilità" && imp.getCodUff().charAt(1) == 'C') {
					String query;
					query = "insert impiegato values(" + imp.getCodL() + "," + imp.getCf() + "," + imp.getNome() + ","
							+ imp.getCognome() + "," + imp.getDataN() + "," + imp.getTelefono() + "," + imp.getVia()
							+ "," + imp.getNum() + "," + imp.getCittà() + "," + imp.getSalario() + "," + "NULL" + ","
							+ "NULL" + "," + imp.getCodUff() + ")";
					this.stVar = this.con.prepareStatement(query);
					this.stVar.executeUpdate();
				} else {
					System.out.print("ERROR");
					return false;
				}
			} else {
				System.out.print("ERROR");
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("ERROR_EXP");
			return false;
		}
		return true;
	}

//UPDATE

	public void updateUfficioImpiegato(String id, String nUff) throws SQLException {
		try {
			String query;
			query = "update impiegato set CodiceUfficioVendite = NULL, CodiceUfficioAcquisti = NULL, CodiceUfficioContabilità = NULL where CodiceLavoratore = '"
					+ id + "'";
			st.executeUpdate(query);
			// IF PER CAPIRE IL TIPO DEL NUOVO UFFICIO
			if (nUff.charAt(0) == 'A') {
				query = "update impiegato set CodiceUfficioAcquisti = '" + nUff + "'where CodiceLavoratore = '" + id
						+ "'";
			} else if (nUff.charAt(0) == 'V') {
				query = "update impiegato set CodiceUfficioVendite = '" + nUff + "' where CodiceLavoratore = '" + id
						+ "'";
			} else if (nUff.charAt(0) == 'C') {
				query = "update impiegato set CodiceUfficioContabilità = '" + nUff + "' where CodiceLavoratore = '" + id
						+ "'";
			}
			st.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
