package GUI;
//
import javax.swing.JFrame;
import javax.swing.SpringLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import java.awt.Color;
import javax.swing.JScrollPane;

import Impiegato.Impiegato;
import Impiegato.OperationImpiegati;
import Uffici.OperationUff;
import Uffici.Uffici;
import net.proteanit.sql.DbUtils;

import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class AmministrazioneGUI {

    private JFrame frmImpiegati;
    private JTextField textCodLav;
    private JTextField textCF;
    private JTextField textNome;
    private JTextField textCognome;
    private JTextField textD_Nascita;
    private JTextField textTelefono;
    private JTextField textCivico;
    private JTextField textCitt�;
    private JTextField textVia;

    private JTable tableImpiegati;
    private JTable tableUffici;
    private JTextField textUpdate;
    private JTextField textNewUfficio;
    
    private ButtonGroup groupUff_2 = new ButtonGroup();
    
    private Impiegato imp;
    private OperationImpiegati oImp = new OperationImpiegati();
    private OperationUff oUff = new OperationUff();
    
    private List<JTextField> textFieldsList = new ArrayList<JTextField>();
    private List<String> stringFieldList = new ArrayList<String>();
    private JTextField textUff;
    private JTextField textDelUff;
    private JTextField textInserisciUfficio;
    private JTextField textInserisciDimensione;
    private JTextField textSalario;
    private JTextField textDel;

    /**
     * Create the application.
     * 
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public AmministrazioneGUI() throws ClassNotFoundException, SQLException {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    @SuppressWarnings("serial")
    private void initialize() {
        frmImpiegati = new JFrame();
        frmImpiegati.setAlwaysOnTop(true);
        frmImpiegati.setResizable(false);
        frmImpiegati.setTitle("Impiegati");
        frmImpiegati.setBounds(100, 100, 1360, 820);
        frmImpiegati.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        SpringLayout springLayout = new SpringLayout();
        frmImpiegati.getContentPane().setLayout(springLayout);

        JLabel lblInserisciImp = new JLabel("Inserisci Impiegato:");
        springLayout.putConstraint(SpringLayout.NORTH, lblInserisciImp, 71, SpringLayout.NORTH,
                frmImpiegati.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, lblInserisciImp, -1040, SpringLayout.EAST,
                frmImpiegati.getContentPane());
        lblInserisciImp.setFont(new Font("Tahoma", Font.BOLD, 20));
        frmImpiegati.getContentPane().add(lblInserisciImp);

        JLabel lblTitolo = new JLabel("IMPIEGATI");
        springLayout.putConstraint(SpringLayout.NORTH, lblTitolo, 10, SpringLayout.NORTH,
                frmImpiegati.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblTitolo, 504, SpringLayout.WEST, frmImpiegati.getContentPane());
        lblTitolo.setFont(new Font("Tahoma", Font.BOLD, 40));
        frmImpiegati.getContentPane().add(lblTitolo);

        JLabel lblNome = new JLabel("Nome:");
        springLayout.putConstraint(SpringLayout.WEST, lblInserisciImp, 0, SpringLayout.WEST, lblNome);
        springLayout.putConstraint(SpringLayout.WEST, lblNome, 10, SpringLayout.WEST, frmImpiegati.getContentPane());
        lblNome.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmImpiegati.getContentPane().add(lblNome);

        JLabel lblCodLavor = new JLabel("CodiceLavoratore:");
        springLayout.putConstraint(SpringLayout.SOUTH, lblInserisciImp, -10, SpringLayout.NORTH, lblCodLavor);
        springLayout.putConstraint(SpringLayout.NORTH, lblCodLavor, 132, SpringLayout.NORTH,
                frmImpiegati.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblCodLavor, 10, SpringLayout.WEST,
                frmImpiegati.getContentPane());
        lblCodLavor.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmImpiegati.getContentPane().add(lblCodLavor);

        JLabel lblCF = new JLabel("Codice Fiscale:");
        springLayout.putConstraint(SpringLayout.WEST, lblCF, 10, SpringLayout.WEST, frmImpiegati.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, lblNome, 23, SpringLayout.SOUTH, lblCF);
        springLayout.putConstraint(SpringLayout.NORTH, lblCF, 30, SpringLayout.SOUTH, lblCodLavor);
        lblCF.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmImpiegati.getContentPane().add(lblCF);

        JLabel lblD_Nascita = new JLabel("Data Nascita (AAAA-MM-GG):");
        springLayout.putConstraint(SpringLayout.NORTH, lblD_Nascita, 26, SpringLayout.SOUTH, lblNome);
        springLayout.putConstraint(SpringLayout.WEST, lblD_Nascita, 10, SpringLayout.WEST,
                frmImpiegati.getContentPane());
        lblD_Nascita.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmImpiegati.getContentPane().add(lblD_Nascita);

        JLabel lblTelefono = new JLabel("Telefono");
        springLayout.putConstraint(SpringLayout.WEST, lblTelefono, 0, SpringLayout.WEST, lblInserisciImp);
        lblTelefono.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmImpiegati.getContentPane().add(lblTelefono);

        JLabel lblVia = new JLabel("Via");
        springLayout.putConstraint(SpringLayout.WEST, lblVia, 0, SpringLayout.WEST, lblInserisciImp);
        lblVia.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmImpiegati.getContentPane().add(lblVia);

        JLabel lblCivico = new JLabel("Civico");
        springLayout.putConstraint(SpringLayout.NORTH, lblVia, -1, SpringLayout.NORTH, lblCivico);
        springLayout.putConstraint(SpringLayout.WEST, lblCivico, 172, SpringLayout.WEST, frmImpiegati.getContentPane());
        lblCivico.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmImpiegati.getContentPane().add(lblCivico);

        JLabel lblCitta = new JLabel("Citt\u00E0:");
        springLayout.putConstraint(SpringLayout.WEST, lblCitta, 0, SpringLayout.WEST, lblInserisciImp);
        springLayout.putConstraint(SpringLayout.EAST, lblCitta, -53, SpringLayout.EAST, lblCF);
        lblCitta.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmImpiegati.getContentPane().add(lblCitta);

        JLabel lblSalario = new JLabel("Salario:");
        springLayout.putConstraint(SpringLayout.WEST, lblSalario, 0, SpringLayout.WEST, lblInserisciImp);
        lblSalario.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmImpiegati.getContentPane().add(lblSalario);

        JLabel lblUfficio = new JLabel("Ufficio:");
        springLayout.putConstraint(SpringLayout.NORTH, lblUfficio, 30, SpringLayout.SOUTH, lblSalario);
        springLayout.putConstraint(SpringLayout.WEST, lblUfficio, 0, SpringLayout.WEST, lblInserisciImp);
        springLayout.putConstraint(SpringLayout.EAST, lblUfficio, 0, SpringLayout.EAST, lblTelefono);
        lblUfficio.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmImpiegati.getContentPane().add(lblUfficio);

        JRadioButton rdbtnAcquisti = new JRadioButton("Acquisti");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnAcquisti, -6, SpringLayout.NORTH, lblUfficio);
        springLayout.putConstraint(SpringLayout.WEST, rdbtnAcquisti, 6, SpringLayout.EAST, lblUfficio);
        rdbtnAcquisti.setActionCommand("Acquisti");
        frmImpiegati.getContentPane().add(rdbtnAcquisti);

        JRadioButton rdbtnVendite = new JRadioButton("Vendite");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnVendite, -1, SpringLayout.NORTH, lblUfficio);
        rdbtnVendite.setActionCommand("Vendite");
        frmImpiegati.getContentPane().add(rdbtnVendite);

        JRadioButton rdbtnContabilit� = new JRadioButton("Contabilit\u00E0");
        springLayout.putConstraint(SpringLayout.EAST, rdbtnVendite, -6, SpringLayout.WEST, rdbtnContabilit�);
        springLayout.putConstraint(SpringLayout.EAST, rdbtnAcquisti, -79, SpringLayout.WEST, rdbtnContabilit�);
        springLayout.putConstraint(SpringLayout.WEST, rdbtnContabilit�, 232, SpringLayout.WEST, frmImpiegati.getContentPane());
        rdbtnContabilit�.setActionCommand("Contabilit�");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnContabilit�, -1, SpringLayout.NORTH, lblUfficio);
        frmImpiegati.getContentPane().add(rdbtnContabilit�);

        ButtonGroup groupUff = new ButtonGroup();
        groupUff.add(rdbtnAcquisti);
        groupUff.add(rdbtnVendite);
        groupUff.add(rdbtnContabilit�);


        textCodLav = new JTextField();
        textFieldsList.add(textCodLav);
        springLayout.putConstraint(SpringLayout.NORTH, textCodLav, 0, SpringLayout.NORTH, lblCodLavor);
        springLayout.putConstraint(SpringLayout.WEST, textCodLav, 6, SpringLayout.EAST, lblCodLavor);
        springLayout.putConstraint(SpringLayout.EAST, textCodLav, -1014, SpringLayout.EAST,
                frmImpiegati.getContentPane());
        frmImpiegati.getContentPane().add(textCodLav);
        textCodLav.setColumns(20);

        textCF = new JTextField();
        textFieldsList.add(textCF);
        springLayout.putConstraint(SpringLayout.EAST, textCF, 156, SpringLayout.EAST, lblCF);
        springLayout.putConstraint(SpringLayout.NORTH, textCF, 0, SpringLayout.NORTH, lblCF);
        springLayout.putConstraint(SpringLayout.WEST, textCF, 6, SpringLayout.EAST, lblCF);
        textCF.setColumns(20);
        frmImpiegati.getContentPane().add(textCF);

        textNome = new JTextField();
        textFieldsList.add(textNome);
        springLayout.putConstraint(SpringLayout.NORTH, textNome, 0, SpringLayout.NORTH, lblNome);
        springLayout.putConstraint(SpringLayout.WEST, textNome, 6, SpringLayout.EAST, lblNome);
        springLayout.putConstraint(SpringLayout.EAST, textNome, -1082, SpringLayout.EAST,
                frmImpiegati.getContentPane());
        textNome.setColumns(20);
        frmImpiegati.getContentPane().add(textNome);

        textCognome = new JTextField();
        textFieldsList.add(textCognome);
        springLayout.putConstraint(SpringLayout.NORTH, textCognome, 0, SpringLayout.NORTH, lblNome);
        springLayout.putConstraint(SpringLayout.EAST, textCognome, -774, SpringLayout.EAST,
                frmImpiegati.getContentPane());
        textCognome.setColumns(20);
        frmImpiegati.getContentPane().add(textCognome);

        textD_Nascita = new JTextField();
        textFieldsList.add(textD_Nascita);
        springLayout.putConstraint(SpringLayout.NORTH, textD_Nascita, 0, SpringLayout.NORTH, lblD_Nascita);
        springLayout.putConstraint(SpringLayout.WEST, textD_Nascita, 10, SpringLayout.EAST, lblD_Nascita);
        springLayout.putConstraint(SpringLayout.SOUTH, textD_Nascita, 3, SpringLayout.SOUTH, lblD_Nascita);
        springLayout.putConstraint(SpringLayout.EAST, textD_Nascita, 204, SpringLayout.EAST, lblD_Nascita);
        frmImpiegati.getContentPane().add(textD_Nascita);
        textD_Nascita.setColumns(10);

        textTelefono = new JTextField();
        textFieldsList.add(textTelefono);
        springLayout.putConstraint(SpringLayout.WEST, textTelefono, 6, SpringLayout.EAST, lblTelefono);
        springLayout.putConstraint(SpringLayout.EAST, textTelefono, -1040, SpringLayout.EAST,
                frmImpiegati.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, lblTelefono, 0, SpringLayout.NORTH, textTelefono);
        springLayout.putConstraint(SpringLayout.NORTH, lblCivico, 22, SpringLayout.SOUTH, textTelefono);
        springLayout.putConstraint(SpringLayout.NORTH, textTelefono, 307, SpringLayout.NORTH,
                frmImpiegati.getContentPane());
        frmImpiegati.getContentPane().add(textTelefono);
        textTelefono.setColumns(10);
        
        textVia = new JTextField();
        textFieldsList.add(textVia);
        springLayout.putConstraint(SpringLayout.NORTH, lblCitta, 25, SpringLayout.SOUTH, textVia);
        springLayout.putConstraint(SpringLayout.NORTH, lblSalario, 65, SpringLayout.SOUTH, textVia);
        springLayout.putConstraint(SpringLayout.NORTH, textVia, 0, SpringLayout.NORTH, lblVia);
        springLayout.putConstraint(SpringLayout.WEST, textVia, 6, SpringLayout.EAST, lblVia);
        springLayout.putConstraint(SpringLayout.SOUTH, textVia, 3, SpringLayout.SOUTH, lblVia);
        springLayout.putConstraint(SpringLayout.EAST, textVia, -6, SpringLayout.WEST, lblCivico);
        frmImpiegati.getContentPane().add(textVia);
        textVia.setColumns(10);

        textCivico = new JTextField();
        textFieldsList.add(textCivico);
        springLayout.putConstraint(SpringLayout.WEST, textCivico, 3, SpringLayout.EAST, lblCivico);
        springLayout.putConstraint(SpringLayout.NORTH, textCivico, 22, SpringLayout.SOUTH, textTelefono);
        springLayout.putConstraint(SpringLayout.EAST, textCivico, -1070, SpringLayout.EAST, frmImpiegati.getContentPane());
        frmImpiegati.getContentPane().add(textCivico);
        textCivico.setColumns(10);

        textCitt� = new JTextField();
        textFieldsList.add(textCitt�);
        springLayout.putConstraint(SpringLayout.NORTH, textCitt�, 0, SpringLayout.NORTH, lblCitta);
        springLayout.putConstraint(SpringLayout.WEST, textCitt�, 1, SpringLayout.EAST, lblCitta);
        frmImpiegati.getContentPane().add(textCitt�);
        textCitt�.setColumns(10);

        JLabel lblImpiegati = new JLabel("Impiegati:");
        springLayout.putConstraint(SpringLayout.NORTH, lblImpiegati, 18, SpringLayout.NORTH, lblInserisciImp);
        springLayout.putConstraint(SpringLayout.WEST, lblImpiegati, 311, SpringLayout.EAST, lblInserisciImp);
        lblImpiegati.setFont(new Font("Tahoma", Font.BOLD, 15));
        frmImpiegati.getContentPane().add(lblImpiegati);       
        
        textSalario = new JTextField();
        textFieldsList.add(textSalario);
        springLayout.putConstraint(SpringLayout.WEST, textSalario, 6, SpringLayout.EAST, lblSalario);
        springLayout.putConstraint(SpringLayout.SOUTH, textSalario, 0, SpringLayout.SOUTH, lblSalario);
        frmImpiegati.getContentPane().add(textSalario);
        textSalario.setColumns(10);

        JButton btnInserisci = new JButton("INSERISCI");
        springLayout.putConstraint(SpringLayout.NORTH, btnInserisci, 21, SpringLayout.SOUTH, lblUfficio);
        springLayout.putConstraint(SpringLayout.WEST, btnInserisci, 14, SpringLayout.WEST, frmImpiegati.getContentPane());
        btnInserisci.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (JTextField textField : textFieldsList) {
                    if (textField.getText().length() == 0) {
                        stringFieldList.add(null);
                    }else {
                        stringFieldList.add(textField.getText());
                    }                   
                }
                stringFieldList.add(groupUff.getSelection().getActionCommand());
                imp = new Impiegato(stringFieldList);
                try {
                    boolean check = oImp.insertImpiegato(imp);
                    if (!check) {
                        stringFieldList.clear();
                    }else {
                        ResultSet rS = oImp.getRS_Imp();
                        tableImpiegati.setModel(DbUtils.resultSetToTableModel(rS));
                    }
                    
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                stringFieldList.clear();
            }
        });
        btnInserisci.setBackground(UIManager.getColor("Button.background"));
        btnInserisci.setForeground(Color.BLACK);
        frmImpiegati.getContentPane().add(btnInserisci);
        
        
        JButton btnVisualizzaImp = new JButton("VISUALIZZA");
        springLayout.putConstraint(SpringLayout.WEST, btnVisualizzaImp, 0, SpringLayout.WEST, lblImpiegati);
        btnVisualizzaImp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {                 
                    OperationImpiegati O_imp = new OperationImpiegati();
                    ResultSet rS = O_imp.getRS_Imp();
                    tableImpiegati.setModel(DbUtils.resultSetToTableModel(rS));
                } catch (Exception ex1) {
                    ex1.printStackTrace();
                }
            }
        });
        btnVisualizzaImp.setBackground(UIManager.getColor("Button.background"));
        frmImpiegati.getContentPane().add(btnVisualizzaImp);

        JButton btnReset = new JButton("RESET");
        btnReset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (JTextField textField : textFieldsList) {
                    textField.setText("");
                }
                stringFieldList.clear();
            }
        });
        springLayout.putConstraint(SpringLayout.SOUTH, rdbtnAcquisti, -11, SpringLayout.NORTH, btnReset);
        springLayout.putConstraint(SpringLayout.NORTH, btnReset, 0, SpringLayout.NORTH, btnInserisci);
        springLayout.putConstraint(SpringLayout.WEST, btnReset, 0, SpringLayout.WEST, textCF);
        btnReset.setBackground(UIManager.getColor("Button.background"));
        frmImpiegati.getContentPane().add(btnReset);

        JLabel lblCodLavorDel = new JLabel("CodiceLavoratore:");
        springLayout.putConstraint(SpringLayout.WEST, lblCodLavorDel, 10, SpringLayout.WEST, frmImpiegati.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, lblCodLavorDel, -81, SpringLayout.SOUTH, frmImpiegati.getContentPane());
        lblCodLavorDel.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmImpiegati.getContentPane().add(lblCodLavorDel);

        JButton btnElimina = new JButton("ELIMINA");
        springLayout.putConstraint(SpringLayout.WEST, btnElimina, 0, SpringLayout.WEST, lblInserisciImp);
        btnElimina.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    oImp.deleteImpiegato(textDel.getText());
                    ResultSet rS = oImp.getRS_Imp();
                    tableImpiegati.setModel(DbUtils.resultSetToTableModel(rS));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        frmImpiegati.getContentPane().add(btnElimina);

        JLabel lblUffici = new JLabel("Uffici:");
        springLayout.putConstraint(SpringLayout.WEST, lblUffici, 0, SpringLayout.WEST, lblImpiegati);
        lblUffici.setFont(new Font("Tahoma", Font.BOLD, 15));
        frmImpiegati.getContentPane().add(lblUffici);

        JButton btnVisualizzaUff = new JButton("VISUALIZZA");
        btnVisualizzaUff.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    OperationUff oUff = new OperationUff();
                    ResultSet rS = oUff.getRS_Uff(groupUff_2.getSelection().getActionCommand());
                    tableUffici.setModel(DbUtils.resultSetToTableModel(rS));
                } catch (ClassNotFoundException | SQLException e1) {
                    e1.printStackTrace();
                }                
            }
        });
        springLayout.putConstraint(SpringLayout.WEST, btnVisualizzaUff, 0, SpringLayout.WEST, lblImpiegati);
        btnVisualizzaUff.setBackground(UIManager.getColor("Button.background"));
        frmImpiegati.getContentPane().add(btnVisualizzaUff);
        
        tableImpiegati = new JTable() {
            public boolean isCellEditable(int row,int column){  
                return false; 
              }
        };
        springLayout.putConstraint(SpringLayout.NORTH, tableImpiegati, 6, SpringLayout.SOUTH, lblImpiegati);
        springLayout.putConstraint(SpringLayout.WEST, tableImpiegati, 203, SpringLayout.EAST, textD_Nascita);
        springLayout.putConstraint(SpringLayout.SOUTH, tableImpiegati, -6, SpringLayout.NORTH, btnVisualizzaImp);
        springLayout.putConstraint(SpringLayout.EAST, tableImpiegati, -72, SpringLayout.EAST, frmImpiegati.getContentPane());        
        JScrollPane scrollPaneImpiegati = new JScrollPane(tableImpiegati);
        springLayout.putConstraint(SpringLayout.NORTH, btnVisualizzaImp, 6, SpringLayout.SOUTH, scrollPaneImpiegati);
        springLayout.putConstraint(SpringLayout.WEST, scrollPaneImpiegati, 0, SpringLayout.WEST, lblImpiegati);
        springLayout.putConstraint(SpringLayout.SOUTH, scrollPaneImpiegati, -461, SpringLayout.SOUTH, frmImpiegati.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, scrollPaneImpiegati, -25, SpringLayout.EAST, frmImpiegati.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, scrollPaneImpiegati, 47, SpringLayout.NORTH, lblInserisciImp);
        frmImpiegati.getContentPane().add(scrollPaneImpiegati);
        
        tableUffici = new JTable() {
            public boolean isCellEditable(int row,int column){  
                return false; 
              }
        };
        springLayout.putConstraint(SpringLayout.NORTH, tableUffici, 6, SpringLayout.SOUTH, lblUffici);
        springLayout.putConstraint(SpringLayout.WEST, tableUffici, 340, SpringLayout.EAST, rdbtnContabilit�);
        springLayout.putConstraint(SpringLayout.SOUTH, tableUffici, -6, SpringLayout.NORTH, btnVisualizzaUff);
        springLayout.putConstraint(SpringLayout.EAST, tableUffici, -37, SpringLayout.EAST, frmImpiegati.getContentPane());
        JScrollPane scrollPaneUffici = new JScrollPane(tableUffici);
        springLayout.putConstraint(SpringLayout.SOUTH, scrollPaneUffici, -164, SpringLayout.SOUTH, frmImpiegati.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, btnVisualizzaUff, 6, SpringLayout.SOUTH, scrollPaneUffici);
        springLayout.putConstraint(SpringLayout.SOUTH, lblUffici, -6, SpringLayout.NORTH, scrollPaneUffici);
        springLayout.putConstraint(SpringLayout.EAST, textCitt�, -350, SpringLayout.WEST, scrollPaneUffici);
        springLayout.putConstraint(SpringLayout.WEST, scrollPaneUffici, 0, SpringLayout.WEST, lblImpiegati);
        springLayout.putConstraint(SpringLayout.EAST, scrollPaneUffici, -25, SpringLayout.EAST, frmImpiegati.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, scrollPaneUffici, 397, SpringLayout.NORTH, frmImpiegati.getContentPane());
        frmImpiegati.getContentPane().add(scrollPaneUffici);
        
        JLabel lblCodLavorUP = new JLabel("CodiceLavoratore:");
        springLayout.putConstraint(SpringLayout.SOUTH, lblCodLavorUP, -175, SpringLayout.SOUTH, frmImpiegati.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, btnElimina, 6, SpringLayout.SOUTH, lblCodLavorUP);
        springLayout.putConstraint(SpringLayout.WEST, lblCodLavorUP, 0, SpringLayout.WEST, lblInserisciImp);
        springLayout.putConstraint(SpringLayout.EAST, lblCodLavorUP, -16, SpringLayout.EAST, rdbtnAcquisti);
        lblCodLavorUP.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmImpiegati.getContentPane().add(lblCodLavorUP);
        
        textUpdate = new JTextField();
        springLayout.putConstraint(SpringLayout.WEST, textUpdate, 143, SpringLayout.WEST, frmImpiegati.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, textUpdate, -81, SpringLayout.SOUTH, frmImpiegati.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, textUpdate, -1100, SpringLayout.EAST, frmImpiegati.getContentPane());
        textUpdate.setColumns(20);
        frmImpiegati.getContentPane().add(textUpdate);
        
        JButton btnAggiorna = new JButton("AGGIORNA");
        springLayout.putConstraint(SpringLayout.NORTH, btnAggiorna, 6, SpringLayout.SOUTH, lblCodLavorDel);
        springLayout.putConstraint(SpringLayout.WEST, btnAggiorna, 0, SpringLayout.WEST, lblInserisciImp);
        btnAggiorna.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    oImp.updateUfficioImpiegato(textUpdate.getText(), textNewUfficio.getText());
                    ResultSet rS = oImp.getRS_Imp();
                    tableImpiegati.setModel(DbUtils.resultSetToTableModel(rS));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        frmImpiegati.getContentPane().add(btnAggiorna);
        
        JLabel lblCognome = new JLabel("Cognome:");
        springLayout.putConstraint(SpringLayout.WEST, textCognome, 6, SpringLayout.EAST, lblCognome);
        lblCognome.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.WEST, lblCognome, 22, SpringLayout.EAST, textNome);
        springLayout.putConstraint(SpringLayout.SOUTH, lblCognome, 0, SpringLayout.SOUTH, lblNome);
        frmImpiegati.getContentPane().add(lblCognome);
        
        JLabel lblCancellaImpiegato = new JLabel("Cancella Impiegato:");
        springLayout.putConstraint(SpringLayout.SOUTH, lblCancellaImpiegato, -198, SpringLayout.SOUTH, frmImpiegati.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblCancellaImpiegato, 0, SpringLayout.WEST, lblInserisciImp);
        lblCancellaImpiegato.setFont(new Font("Tahoma", Font.BOLD, 20));
        frmImpiegati.getContentPane().add(lblCancellaImpiegato);
        
        JLabel lblAggiornaImpiegato = new JLabel("Aggiorna Impiegato:");
        springLayout.putConstraint(SpringLayout.SOUTH, lblAggiornaImpiegato, -104, SpringLayout.SOUTH, frmImpiegati.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, lblCodLavorDel, 6, SpringLayout.SOUTH, lblAggiornaImpiegato);
        springLayout.putConstraint(SpringLayout.NORTH, textUpdate, 6, SpringLayout.SOUTH, lblAggiornaImpiegato);
        springLayout.putConstraint(SpringLayout.WEST, lblAggiornaImpiegato, 0, SpringLayout.WEST, lblInserisciImp);
        lblAggiornaImpiegato.setFont(new Font("Tahoma", Font.BOLD, 20));
        frmImpiegati.getContentPane().add(lblAggiornaImpiegato);
        
        JLabel lblnewOffice = new JLabel("Nuovo Ufficio:");
        springLayout.putConstraint(SpringLayout.WEST, lblnewOffice, 26, SpringLayout.EAST, textUpdate);
        springLayout.putConstraint(SpringLayout.SOUTH, lblnewOffice, 0, SpringLayout.SOUTH, lblCodLavorDel);
        lblnewOffice.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmImpiegati.getContentPane().add(lblnewOffice);
        
        textNewUfficio = new JTextField();
        springLayout.putConstraint(SpringLayout.WEST, textNewUfficio, 8, SpringLayout.EAST, lblnewOffice);
        springLayout.putConstraint(SpringLayout.SOUTH, textNewUfficio, 0, SpringLayout.SOUTH, lblCodLavorDel);
        textNewUfficio.setColumns(20);
        frmImpiegati.getContentPane().add(textNewUfficio);
        
        textUff = new JTextField();
        textFieldsList.add(textUff);
        springLayout.putConstraint(SpringLayout.NORTH, textUff, 0, SpringLayout.NORTH, lblUfficio);
        frmImpiegati.getContentPane().add(textUff);
        textUff.setColumns(10);
        
        JLabel lblCodUff = new JLabel("Codice Ufficio:");
        springLayout.putConstraint(SpringLayout.WEST, textUff, 6, SpringLayout.EAST, lblCodUff);
        springLayout.putConstraint(SpringLayout.NORTH, lblCodUff, 0, SpringLayout.NORTH, lblUfficio);
        springLayout.putConstraint(SpringLayout.WEST, lblCodUff, 6, SpringLayout.EAST, rdbtnContabilit�);
        lblCodUff.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmImpiegati.getContentPane().add(lblCodUff);
        
        JRadioButton rdbtnVisualizzaAcq = new JRadioButton("Acquisti");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnVisualizzaAcq, 6, SpringLayout.SOUTH, scrollPaneUffici);
        rdbtnVisualizzaAcq.setActionCommand("Acquisti");
        springLayout.putConstraint(SpringLayout.WEST, rdbtnVisualizzaAcq, 21, SpringLayout.EAST, btnVisualizzaUff);
        frmImpiegati.getContentPane().add(rdbtnVisualizzaAcq);
        
        JRadioButton rdbtnVisualizzaVend = new JRadioButton("Vendite");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnVisualizzaVend, 6, SpringLayout.SOUTH, scrollPaneUffici);
        rdbtnVisualizzaVend.setActionCommand("Vendite");
        springLayout.putConstraint(SpringLayout.WEST, rdbtnVisualizzaVend, 13, SpringLayout.EAST, rdbtnVisualizzaAcq);
        frmImpiegati.getContentPane().add(rdbtnVisualizzaVend);
        
        JRadioButton rdbtnVisualizzaCont = new JRadioButton("Contabilit\u00E0");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnVisualizzaCont, 6, SpringLayout.SOUTH, scrollPaneUffici);
        rdbtnVisualizzaCont.setActionCommand("Contabilit�");
        springLayout.putConstraint(SpringLayout.WEST, rdbtnVisualizzaCont, 16, SpringLayout.EAST, rdbtnVisualizzaVend);
        frmImpiegati.getContentPane().add(rdbtnVisualizzaCont);
        
        groupUff_2.add(rdbtnVisualizzaAcq);
        groupUff_2.add(rdbtnVisualizzaVend);
        groupUff_2.add(rdbtnVisualizzaCont);
        
   
        textDelUff = new JTextField();
        springLayout.putConstraint(SpringLayout.WEST, textDelUff, 0, SpringLayout.WEST, rdbtnVisualizzaAcq);
        frmImpiegati.getContentPane().add(textDelUff);
        textDelUff.setColumns(10);
        
        JLabel lblDelUfficio = new JLabel("Cancella Ufficio:");
        springLayout.putConstraint(SpringLayout.NORTH, lblDelUfficio, 64, SpringLayout.SOUTH, btnVisualizzaUff);
        springLayout.putConstraint(SpringLayout.NORTH, textDelUff, 0, SpringLayout.NORTH, lblDelUfficio);
        lblDelUfficio.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.WEST, lblDelUfficio, 0, SpringLayout.WEST, lblImpiegati);
        frmImpiegati.getContentPane().add(lblDelUfficio);
        
        JLabel lblInserisciUfficio = new JLabel("Inserisci Ufficio:");
        springLayout.putConstraint(SpringLayout.NORTH, lblInserisciUfficio, 0, SpringLayout.NORTH, lblCodLavorDel);
        springLayout.putConstraint(SpringLayout.EAST, lblInserisciUfficio, -332, SpringLayout.EAST, frmImpiegati.getContentPane());
        lblInserisciUfficio.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmImpiegati.getContentPane().add(lblInserisciUfficio);
        
        textInserisciUfficio = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textInserisciUfficio, 7, SpringLayout.SOUTH, lblInserisciUfficio);
        springLayout.putConstraint(SpringLayout.WEST, textInserisciUfficio, 995, SpringLayout.WEST, frmImpiegati.getContentPane());
        frmImpiegati.getContentPane().add(textInserisciUfficio);
        textInserisciUfficio.setColumns(10);
        
        JLabel lblInsertCodUff = new JLabel("Codice Ufficio:");
        springLayout.putConstraint(SpringLayout.NORTH, lblInsertCodUff, 10, SpringLayout.SOUTH, lblInserisciUfficio);
        springLayout.putConstraint(SpringLayout.EAST, lblInsertCodUff, -6, SpringLayout.WEST, textInserisciUfficio);
        frmImpiegati.getContentPane().add(lblInsertCodUff);
        
        JLabel lblInsertDimensione = new JLabel("Dimensione:");
        springLayout.putConstraint(SpringLayout.EAST, lblInsertDimensione, 0, SpringLayout.EAST, lblInsertCodUff);
        frmImpiegati.getContentPane().add(lblInsertDimensione);
        
        textInserisciDimensione = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, lblInsertDimensione, 3, SpringLayout.NORTH, textInserisciDimensione);
        springLayout.putConstraint(SpringLayout.NORTH, textInserisciDimensione, 10, SpringLayout.SOUTH, textInserisciUfficio);
        springLayout.putConstraint(SpringLayout.EAST, textInserisciDimensione, 0, SpringLayout.EAST, textInserisciUfficio);
        frmImpiegati.getContentPane().add(textInserisciDimensione);
        textInserisciDimensione.setColumns(10);
        
        JButton btnCancellaUfficio = new JButton("CANCELLA");
        btnCancellaUfficio.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    oUff.deleteUff(textDelUff.getText());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnCancellaUfficio, 6, SpringLayout.SOUTH, textDelUff);
        springLayout.putConstraint(SpringLayout.WEST, btnCancellaUfficio, 0, SpringLayout.WEST, rdbtnVisualizzaAcq);
        frmImpiegati.getContentPane().add(btnCancellaUfficio);
        
        JButton btnInserisciUfficio = new JButton("INSERISCI");
        btnInserisciUfficio.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Uffici uff = new Uffici(textInserisciUfficio.getText(), textInserisciDimensione.getText());
                try {
                    oUff.insertIUff(uff);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        springLayout.putConstraint(SpringLayout.WEST, btnInserisciUfficio, 24, SpringLayout.EAST, textInserisciUfficio);
        springLayout.putConstraint(SpringLayout.SOUTH, btnInserisciUfficio, -38, SpringLayout.SOUTH, frmImpiegati.getContentPane());
        frmImpiegati.getContentPane().add(btnInserisciUfficio);
        
        JButton btnDirigenteUff = new JButton("DIRIGENTE UFFICIO >>");
        btnDirigenteUff.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DirigenteUfficioGUI dGUI;
                try {
                    dGUI = new DirigenteUfficioGUI();
                    dGUI.run();
                } catch (ClassNotFoundException | SQLException e1) {
                    e1.printStackTrace();
                }                
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnDirigenteUff, 25, SpringLayout.NORTH, lblTitolo);
        springLayout.putConstraint(SpringLayout.WEST, btnDirigenteUff, -226, SpringLayout.EAST, frmImpiegati.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, btnDirigenteUff, 58, SpringLayout.NORTH, frmImpiegati.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, btnDirigenteUff, 0, SpringLayout.EAST, scrollPaneImpiegati);
        btnDirigenteUff.setBackground(Color.ORANGE);
        frmImpiegati.getContentPane().add(btnDirigenteUff);
        
        
        
        textDel = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textDel, 6, SpringLayout.SOUTH, lblCancellaImpiegato);
        springLayout.putConstraint(SpringLayout.WEST, textDel, 16, SpringLayout.EAST, lblCodLavorUP);
        springLayout.putConstraint(SpringLayout.EAST, textDel, 151, SpringLayout.WEST, textCodLav);
        frmImpiegati.getContentPane().add(textDel);
        textDel.setColumns(10);
                
    }

    public void run() {
        try {
            AmministrazioneGUI window = new AmministrazioneGUI();
            window.frmImpiegati.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
