package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SpringLayout;

import Assemblaggio.Assemblaggio;
import Assemblaggio.OperationAssemblaggio;
import Pezzo.OperationPezzo;
import Pezzo.Pezzo;
import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import javax.swing.JScrollPane;

import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class PezzoGUI {

    private JFrame frmPezziGui;
    private JTextField textCodPezzo;
    private JTextField textQuantit�;
    private JTextField textMateriale;
    private JTextField textProvenienza;
    private JTextField textcostoUnitario;
    private JTextField textMagazzino;
    private JTextField textDescrizione;
    private JTable tablePezzi;
    private JTable tableAss;
    private JTextField textProdAss;
    private JTextField textPezzoAss;
    private JTextField textQuantit�Ass;

    private List<JTextField> textFieldList = new ArrayList<JTextField>();
    private List<String> stringFieldList = new ArrayList<String>();
    private OperationPezzo oPezzo = new OperationPezzo();
    private OperationAssemblaggio oAss = new OperationAssemblaggio();

    /**
     * Create the application.
     */
    public PezzoGUI() throws SQLException, ClassNotFoundException {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmPezziGui = new JFrame();
        frmPezziGui.setTitle("Pezzi GUI");
        frmPezziGui.setResizable(false);
        frmPezziGui.setBounds(100, 100, 1240, 710);
        frmPezziGui.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        SpringLayout springLayout = new SpringLayout();
        frmPezziGui.getContentPane().setLayout(springLayout);

        JLabel lblTitle = new JLabel("PEZZI");
        springLayout.putConstraint(SpringLayout.NORTH, lblTitle, 10, SpringLayout.NORTH, frmPezziGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblTitle, 529, SpringLayout.WEST, frmPezziGui.getContentPane());
        lblTitle.setFont(new Font("Tahoma", Font.BOLD, 40));
        frmPezziGui.getContentPane().add(lblTitle);

        JLabel lblSubTitle = new JLabel("Inserisci Pezzo:");
        lblSubTitle.setFont(new Font("Tahoma", Font.BOLD, 20));
        springLayout.putConstraint(SpringLayout.NORTH, lblSubTitle, 80, SpringLayout.NORTH, frmPezziGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblSubTitle, 10, SpringLayout.WEST, frmPezziGui.getContentPane());
        frmPezziGui.getContentPane().add(lblSubTitle);

        JLabel lblCodPezzo = new JLabel("Codice Pezzo:");
        lblCodPezzo.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblCodPezzo, 22, SpringLayout.SOUTH, lblSubTitle);
        springLayout.putConstraint(SpringLayout.WEST, lblCodPezzo, 0, SpringLayout.WEST, lblSubTitle);
        frmPezziGui.getContentPane().add(lblCodPezzo);

        textCodPezzo = new JTextField();
        textFieldList.add(textCodPezzo);
        springLayout.putConstraint(SpringLayout.NORTH, textCodPezzo, 0, SpringLayout.NORTH, lblCodPezzo);
        springLayout.putConstraint(SpringLayout.WEST, textCodPezzo, 6, SpringLayout.EAST, lblCodPezzo);
        frmPezziGui.getContentPane().add(textCodPezzo);
        textCodPezzo.setColumns(10);

        JLabel lblQuantit� = new JLabel("Quantit\u00E0:");
        lblQuantit�.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblQuantit�, 22, SpringLayout.SOUTH, lblCodPezzo);
        springLayout.putConstraint(SpringLayout.WEST, lblQuantit�, 0, SpringLayout.WEST, lblSubTitle);
        frmPezziGui.getContentPane().add(lblQuantit�);

        textQuantit� = new JTextField();
        textQuantit�.setEnabled(false);
        textQuantit�.setText("0");
        springLayout.putConstraint(SpringLayout.WEST, textQuantit�, 6, SpringLayout.EAST, lblQuantit�);
        springLayout.putConstraint(SpringLayout.SOUTH, textQuantit�, 0, SpringLayout.SOUTH, lblQuantit�);
        frmPezziGui.getContentPane().add(textQuantit�);
        textQuantit�.setColumns(10);

        JLabel lblMateriale = new JLabel("Materiale:");
        lblMateriale.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblMateriale, 19, SpringLayout.SOUTH, lblQuantit�);
        springLayout.putConstraint(SpringLayout.WEST, lblMateriale, 10, SpringLayout.WEST, frmPezziGui.getContentPane());
        frmPezziGui.getContentPane().add(lblMateriale);

        textMateriale = new JTextField();
        textFieldList.add(textMateriale);
        springLayout.putConstraint(SpringLayout.NORTH, textMateriale, 13, SpringLayout.SOUTH, textQuantit�);
        springLayout.putConstraint(SpringLayout.WEST, textMateriale, 4, SpringLayout.EAST, lblMateriale);
        frmPezziGui.getContentPane().add(textMateriale);
        textMateriale.setColumns(10);

        JLabel lblProvenienza = new JLabel("Provenienza:");
        lblProvenienza.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblProvenienza, 21, SpringLayout.SOUTH, lblMateriale);
        springLayout.putConstraint(SpringLayout.WEST, lblProvenienza, 0, SpringLayout.WEST, lblSubTitle);
        frmPezziGui.getContentPane().add(lblProvenienza);

        textProvenienza = new JTextField();
        textFieldList.add(textProvenienza);
        springLayout.putConstraint(SpringLayout.NORTH, textProvenienza, 0, SpringLayout.NORTH, lblProvenienza);
        springLayout.putConstraint(SpringLayout.WEST, textProvenienza, 6, SpringLayout.EAST, lblProvenienza);
        springLayout.putConstraint(SpringLayout.EAST, textProvenienza, 126, SpringLayout.EAST, lblProvenienza);
        frmPezziGui.getContentPane().add(textProvenienza);
        textProvenienza.setColumns(10);

        JLabel lblCostoUnitario = new JLabel("Costo Unitario:");
        lblCostoUnitario.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblCostoUnitario, 22, SpringLayout.SOUTH, lblProvenienza);
        springLayout.putConstraint(SpringLayout.WEST, lblCostoUnitario, 0, SpringLayout.WEST, lblSubTitle);
        frmPezziGui.getContentPane().add(lblCostoUnitario);

        textcostoUnitario = new JTextField();
        textFieldList.add(textcostoUnitario);
        springLayout.putConstraint(SpringLayout.WEST, textcostoUnitario, 9, SpringLayout.EAST, lblCostoUnitario);
        springLayout.putConstraint(SpringLayout.SOUTH, textcostoUnitario, 0, SpringLayout.SOUTH, lblCostoUnitario);
        frmPezziGui.getContentPane().add(textcostoUnitario);
        textcostoUnitario.setColumns(10);

        JLabel lblMagazzino = new JLabel("Magazzino:");
        lblMagazzino.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblMagazzino, 25, SpringLayout.SOUTH, lblCostoUnitario);
        springLayout.putConstraint(SpringLayout.WEST, lblMagazzino, 0, SpringLayout.WEST, lblSubTitle);
        frmPezziGui.getContentPane().add(lblMagazzino);

        textMagazzino = new JTextField();
        textFieldList.add(textMagazzino);
        springLayout.putConstraint(SpringLayout.NORTH, textMagazzino, 0, SpringLayout.NORTH, lblMagazzino);
        springLayout.putConstraint(SpringLayout.WEST, textMagazzino, 6, SpringLayout.EAST, lblMagazzino);
        frmPezziGui.getContentPane().add(textMagazzino);
        textMagazzino.setColumns(10);

        JLabel lblDescrizione = new JLabel("Descrizione:");
        lblDescrizione.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblDescrizione, 24, SpringLayout.SOUTH, lblMagazzino);
        springLayout.putConstraint(SpringLayout.WEST, lblDescrizione, 0, SpringLayout.WEST, lblSubTitle);
        frmPezziGui.getContentPane().add(lblDescrizione);

        textDescrizione = new JTextField();
        textFieldList.add(textDescrizione);
        springLayout.putConstraint(SpringLayout.NORTH, textDescrizione, 6, SpringLayout.SOUTH, lblDescrizione);
        springLayout.putConstraint(SpringLayout.WEST, textDescrizione, 10, SpringLayout.WEST, frmPezziGui.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, textDescrizione, 76, SpringLayout.SOUTH, lblDescrizione);
        frmPezziGui.getContentPane().add(textDescrizione);
        textDescrizione.setColumns(10);

        JButton btnInserisci = new JButton("INSERISCI");
        btnInserisci.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (JTextField textField : textFieldList) {
                    stringFieldList.add(textField.getText());
                }
                Pezzo p = new Pezzo(stringFieldList);
                try {
                    boolean check = oPezzo.insertPezzo(p);
                    if(!check) {
                        stringFieldList.clear();
                    }else {
                        ResultSet rS = oPezzo.getRS_Inventario();
                        tablePezzi.setModel(DbUtils.resultSetToTableModel(rS));
                    }                   
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                stringFieldList.clear();
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnInserisci, 16, SpringLayout.SOUTH, textDescrizione);
        springLayout.putConstraint(SpringLayout.WEST, btnInserisci, 0, SpringLayout.WEST, lblSubTitle);
        frmPezziGui.getContentPane().add(btnInserisci);

        JButton btnReset = new JButton("RESET");
        btnReset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (JTextField textField : textFieldList) {
                    textField.setText("");
                }
                stringFieldList.clear();
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnReset, 0, SpringLayout.NORTH, btnInserisci);
        springLayout.putConstraint(SpringLayout.EAST, btnReset, 0, SpringLayout.EAST, textProvenienza);
        frmPezziGui.getContentPane().add(btnReset);

        JLabel lblPezzi = new JLabel("Pezzi:");
        lblPezzi.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblPezzi, 11, SpringLayout.SOUTH, lblTitle);
        springLayout.putConstraint(SpringLayout.WEST, lblPezzi, 0, SpringLayout.WEST, lblTitle);
        frmPezziGui.getContentPane().add(lblPezzi);

        tablePezzi = new JTable() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        JScrollPane scrollPezzi = new JScrollPane(tablePezzi);
        springLayout.putConstraint(SpringLayout.NORTH, scrollPezzi, 11, SpringLayout.SOUTH, lblPezzi);
        springLayout.putConstraint(SpringLayout.WEST, scrollPezzi, 0, SpringLayout.WEST, lblTitle);
        springLayout.putConstraint(SpringLayout.SOUTH, scrollPezzi, 0, SpringLayout.SOUTH, lblMateriale);
        springLayout.putConstraint(SpringLayout.EAST, textMateriale, -320, SpringLayout.WEST, tablePezzi);
        springLayout.putConstraint(SpringLayout.NORTH, tablePezzi, 2, SpringLayout.SOUTH, lblPezzi);
        springLayout.putConstraint(SpringLayout.WEST, tablePezzi, 0, SpringLayout.WEST, lblTitle);
        springLayout.putConstraint(SpringLayout.SOUTH, tablePezzi, 0, SpringLayout.SOUTH, lblMateriale);
        springLayout.putConstraint(SpringLayout.EAST, tablePezzi, -86, SpringLayout.EAST, frmPezziGui.getContentPane());
        frmPezziGui.getContentPane().add(scrollPezzi);

        JButton btnVisPezzi = new JButton("VISUALIZZA");
        btnVisPezzi.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = oPezzo.getRS_Pezzo();
                tablePezzi.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.WEST, btnVisPezzi, 0, SpringLayout.WEST, lblTitle);
        springLayout.putConstraint(SpringLayout.SOUTH, btnVisPezzi, 0, SpringLayout.SOUTH, lblProvenienza);
        frmPezziGui.getContentPane().add(btnVisPezzi);

        JLabel lblAssemblaggio = new JLabel("Assemblaggio:");
        lblAssemblaggio.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblAssemblaggio, 0, SpringLayout.NORTH, lblCostoUnitario);
        springLayout.putConstraint(SpringLayout.WEST, lblAssemblaggio, 0, SpringLayout.WEST, lblTitle);
        frmPezziGui.getContentPane().add(lblAssemblaggio);

        tableAss = new JTable() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        JScrollPane scrollAss = new JScrollPane(tableAss);
        springLayout.putConstraint(SpringLayout.EAST, scrollPezzi, 0, SpringLayout.EAST, scrollAss);
        springLayout.putConstraint(SpringLayout.NORTH, scrollAss, 6, SpringLayout.SOUTH, lblAssemblaggio);
        springLayout.putConstraint(SpringLayout.WEST, scrollAss, 0, SpringLayout.WEST, lblTitle);
        springLayout.putConstraint(SpringLayout.SOUTH, scrollAss, 354, SpringLayout.NORTH, lblPezzi);
        springLayout.putConstraint(SpringLayout.EAST, scrollAss, -76, SpringLayout.EAST, frmPezziGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, textDescrizione, -304, SpringLayout.WEST, tableAss);
        springLayout.putConstraint(SpringLayout.NORTH, tableAss, 6, SpringLayout.SOUTH, lblAssemblaggio);
        springLayout.putConstraint(SpringLayout.WEST, tableAss, 0, SpringLayout.WEST, lblTitle);
        springLayout.putConstraint(SpringLayout.SOUTH, tableAss, 53, SpringLayout.SOUTH, lblDescrizione);
        springLayout.putConstraint(SpringLayout.EAST, tableAss, -86, SpringLayout.EAST, frmPezziGui.getContentPane());
        frmPezziGui.getContentPane().add(scrollAss);

        JButton btnVisAssemblaggio = new JButton("VISUALIZZA");
        btnVisAssemblaggio.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = oAss.getRS_Assemblaggio();
                tableAss.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnVisAssemblaggio, 6, SpringLayout.SOUTH, tableAss);
        springLayout.putConstraint(SpringLayout.WEST, btnVisAssemblaggio, 0, SpringLayout.WEST, lblTitle);
        frmPezziGui.getContentPane().add(btnVisAssemblaggio);

        JLabel lblInsertAssemblaggio = new JLabel("Inserisci Assemblaggio:");
        lblInsertAssemblaggio.setFont(new Font("Tahoma", Font.BOLD, 20));
        springLayout.putConstraint(SpringLayout.NORTH, lblInsertAssemblaggio, 23, SpringLayout.SOUTH,
                btnVisAssemblaggio);
        springLayout.putConstraint(SpringLayout.WEST, lblInsertAssemblaggio, 0, SpringLayout.WEST, lblTitle);
        frmPezziGui.getContentPane().add(lblInsertAssemblaggio);

        JLabel lblProdAss = new JLabel("Codice Prodotto:");
        lblProdAss.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblProdAss, 6, SpringLayout.SOUTH, lblInsertAssemblaggio);
        springLayout.putConstraint(SpringLayout.WEST, lblProdAss, 0, SpringLayout.WEST, lblTitle);
        frmPezziGui.getContentPane().add(lblProdAss);

        textProdAss = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textProdAss, 6, SpringLayout.SOUTH, lblInsertAssemblaggio);
        springLayout.putConstraint(SpringLayout.WEST, textProdAss, 6, SpringLayout.EAST, lblProdAss);
        springLayout.putConstraint(SpringLayout.EAST, textProdAss, 0, SpringLayout.EAST, lblInsertAssemblaggio);
        frmPezziGui.getContentPane().add(textProdAss);
        textProdAss.setColumns(10);

        JLabel lblPezzoAss = new JLabel("Codice Pezzo:");
        lblPezzoAss.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblPezzoAss, 20, SpringLayout.SOUTH, lblProdAss);
        springLayout.putConstraint(SpringLayout.WEST, lblPezzoAss, 0, SpringLayout.WEST, lblTitle);
        frmPezziGui.getContentPane().add(lblPezzoAss);

        textPezzoAss = new JTextField();
        springLayout.putConstraint(SpringLayout.WEST, textPezzoAss, 6, SpringLayout.EAST, lblPezzoAss);
        springLayout.putConstraint(SpringLayout.EAST, textPezzoAss, 134, SpringLayout.EAST, lblPezzoAss);
        frmPezziGui.getContentPane().add(textPezzoAss);
        textPezzoAss.setColumns(10);

        JLabel lblQuantit�Ass = new JLabel("Quantit\u00E0 Pezzo:");
        lblQuantit�Ass.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblQuantit�Ass, 18, SpringLayout.SOUTH, lblPezzoAss);
        springLayout.putConstraint(SpringLayout.WEST, lblQuantit�Ass, 0, SpringLayout.WEST, lblTitle);
        frmPezziGui.getContentPane().add(lblQuantit�Ass);

        textQuantit�Ass = new JTextField();
        springLayout.putConstraint(SpringLayout.WEST, textQuantit�Ass, 15, SpringLayout.EAST, lblQuantit�Ass);
        springLayout.putConstraint(SpringLayout.SOUTH, textPezzoAss, -12, SpringLayout.NORTH, textQuantit�Ass);
        springLayout.putConstraint(SpringLayout.SOUTH, textQuantit�Ass, 0, SpringLayout.SOUTH, lblQuantit�Ass);
        frmPezziGui.getContentPane().add(textQuantit�Ass);
        textQuantit�Ass.setColumns(10);

        JButton btnInsertAss = new JButton("INSERISCI");
        btnInsertAss.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Assemblaggio ass = new Assemblaggio(textPezzoAss.getText(), textProdAss.getText(),
                        textQuantit�Ass.getText());
                try {
                    oAss.insertAssemblaggio(ass);
                    ResultSet rS = oAss.getRS_Assemblaggio();
                    tableAss.setModel(DbUtils.resultSetToTableModel(rS));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnInsertAss, 14, SpringLayout.SOUTH, lblQuantit�Ass);
        springLayout.putConstraint(SpringLayout.WEST, btnInsertAss, 0, SpringLayout.WEST, lblTitle);
        frmPezziGui.getContentPane().add(btnInsertAss);

        JButton btnPezzoMaxAcq = new JButton("Pezzo pi\u00F9 Acquistato");
        btnPezzoMaxAcq.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = oPezzo.getRS_PezzoPi�Acquistato();
                tablePezzi.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.WEST, btnPezzoMaxAcq, 0, SpringLayout.WEST, lblSubTitle);
        frmPezziGui.getContentPane().add(btnPezzoMaxAcq);

        JLabel lblAltro = new JLabel("Altro:");
        springLayout.putConstraint(SpringLayout.NORTH, btnPezzoMaxAcq, 6, SpringLayout.SOUTH, lblAltro);
        lblAltro.setFont(new Font("Tahoma", Font.BOLD, 20));
        springLayout.putConstraint(SpringLayout.WEST, lblAltro, 0, SpringLayout.WEST, lblSubTitle);
        springLayout.putConstraint(SpringLayout.SOUTH, lblAltro, 0, SpringLayout.SOUTH, lblProdAss);
        frmPezziGui.getContentPane().add(lblAltro);

        JButton btnPezzoMaxUsato = new JButton("Pezzo pi\u00F9 Usato");
        btnPezzoMaxUsato.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = oPezzo.getRS_PezzoPi�Usato();
                tablePezzi.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnPezzoMaxUsato, 20, SpringLayout.SOUTH, btnPezzoMaxAcq);
        springLayout.putConstraint(SpringLayout.WEST, btnPezzoMaxUsato, 0, SpringLayout.WEST, lblSubTitle);
        springLayout.putConstraint(SpringLayout.EAST, btnPezzoMaxUsato, 0, SpringLayout.EAST, btnPezzoMaxAcq);
        frmPezziGui.getContentPane().add(btnPezzoMaxUsato);
    }

    public void run() {
        try {
            PezzoGUI window = new PezzoGUI();
            window.frmPezziGui.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
