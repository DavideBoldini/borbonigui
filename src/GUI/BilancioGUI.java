package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SpringLayout;

import Bilancio.OperationBil;
import Ordine.OperationOrdini;
import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import javax.swing.JScrollPane;

import java.awt.Font;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class BilancioGUI {

    private JFrame frmBilancioGui;
    private JTable tableBilancio;
    private JTable tableStorico;
    private OperationBil oBil = new OperationBil();
    private OperationOrdini oOrd = new OperationOrdini();

    
    /**
     * Create the application.
     */
    public BilancioGUI() throws SQLException, ClassNotFoundException{
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmBilancioGui = new JFrame();
        frmBilancioGui.setTitle("Bilancio GUI");
        frmBilancioGui.setAlwaysOnTop(true);
        frmBilancioGui.setResizable(false);
        frmBilancioGui.setBounds(100, 100, 975, 590);
        frmBilancioGui.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        SpringLayout springLayout = new SpringLayout();
        frmBilancioGui.getContentPane().setLayout(springLayout);
        
        JLabel lblBilancio = new JLabel("Bilancio:");
        springLayout.putConstraint(SpringLayout.NORTH, lblBilancio, 46, SpringLayout.NORTH, frmBilancioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblBilancio, 48, SpringLayout.WEST, frmBilancioGui.getContentPane());
        lblBilancio.setFont(new Font("Tahoma", Font.BOLD, 40));
        frmBilancioGui.getContentPane().add(lblBilancio);
        
        tableBilancio = new JTable() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        JScrollPane scrollBil = new JScrollPane(tableBilancio);
        springLayout.putConstraint(SpringLayout.NORTH, scrollBil, 46, SpringLayout.NORTH, frmBilancioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, scrollBil, 61, SpringLayout.EAST, lblBilancio);
        springLayout.putConstraint(SpringLayout.EAST, scrollBil, -141, SpringLayout.EAST, frmBilancioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, tableBilancio, 46, SpringLayout.NORTH, frmBilancioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, tableBilancio, 43, SpringLayout.EAST, lblBilancio);
        springLayout.putConstraint(SpringLayout.SOUTH, tableBilancio, 227, SpringLayout.NORTH, frmBilancioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, tableBilancio, -174, SpringLayout.EAST, frmBilancioGui.getContentPane());
        frmBilancioGui.getContentPane().add(scrollBil);
        
        JButton btnVisBilancio = new JButton("VISUALIZZA");
        springLayout.putConstraint(SpringLayout.NORTH, btnVisBilancio, 17, SpringLayout.SOUTH, lblBilancio);
        springLayout.putConstraint(SpringLayout.WEST, btnVisBilancio, 0, SpringLayout.WEST, lblBilancio);
        btnVisBilancio.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = oBil.getRS_Bil();
                tableBilancio.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        btnVisBilancio.setFont(new Font("Tahoma", Font.PLAIN, 17));
        frmBilancioGui.getContentPane().add(btnVisBilancio);
        
        JLabel lblStorico = new JLabel("Storico:");
        lblStorico.setFont(new Font("Tahoma", Font.BOLD, 40));
        springLayout.putConstraint(SpringLayout.WEST, lblStorico, 0, SpringLayout.WEST, lblBilancio);
        frmBilancioGui.getContentPane().add(lblStorico);
        
        JButton btnVisStorico = new JButton("VISUALIZZA");
        btnVisStorico.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = oOrd.getRS_Storico();
                tableStorico.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnVisStorico, 16, SpringLayout.SOUTH, lblStorico);
        springLayout.putConstraint(SpringLayout.WEST, btnVisStorico, 48, SpringLayout.WEST, frmBilancioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, btnVisStorico, 65, SpringLayout.SOUTH, lblStorico);
        springLayout.putConstraint(SpringLayout.EAST, btnVisStorico, 205, SpringLayout.WEST, frmBilancioGui.getContentPane());
        btnVisStorico.setFont(new Font("Tahoma", Font.PLAIN, 17));
        frmBilancioGui.getContentPane().add(btnVisStorico);
        
        tableStorico = new JTable() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        JScrollPane scrollStorico = new JScrollPane(tableStorico);
        springLayout.putConstraint(SpringLayout.NORTH, scrollStorico, 327, SpringLayout.NORTH, frmBilancioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, scrollBil, -88, SpringLayout.NORTH, scrollStorico);
        springLayout.putConstraint(SpringLayout.WEST, scrollStorico, 0, SpringLayout.WEST, scrollBil);
        springLayout.putConstraint(SpringLayout.SOUTH, scrollStorico, -41, SpringLayout.SOUTH, frmBilancioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, scrollStorico, 0, SpringLayout.EAST, scrollBil);
        springLayout.putConstraint(SpringLayout.NORTH, tableStorico, 83, SpringLayout.SOUTH, tableBilancio);
        springLayout.putConstraint(SpringLayout.WEST, tableStorico, 60, SpringLayout.EAST, lblStorico);
        springLayout.putConstraint(SpringLayout.SOUTH, tableStorico, -46, SpringLayout.SOUTH, frmBilancioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, tableStorico, 0, SpringLayout.EAST, tableBilancio);
        frmBilancioGui.getContentPane().add(scrollStorico);
        
        JButton btnSocio = new JButton("SOCI >>");
        btnSocio.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                SocioGUI sGUI;
                try {
                    sGUI = new SocioGUI();
                    sGUI.run();
                } catch (ClassNotFoundException | SQLException e1) {
                    e1.printStackTrace();
                }
                
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnSocio, 267, SpringLayout.NORTH, frmBilancioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, btnSocio, -141, SpringLayout.EAST, frmBilancioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, btnSocio, -255, SpringLayout.SOUTH, frmBilancioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, btnSocio, -30, SpringLayout.EAST, frmBilancioGui.getContentPane());
        frmBilancioGui.getContentPane().add(btnSocio);
        
        JButton btnUtile = new JButton("Utile");
        btnUtile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = oBil.getRS_Util();
                tableBilancio.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnUtile, 177, SpringLayout.NORTH, frmBilancioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, lblStorico, 110, SpringLayout.SOUTH, btnUtile);
        springLayout.putConstraint(SpringLayout.SOUTH, btnVisBilancio, -16, SpringLayout.NORTH, btnUtile);
        springLayout.putConstraint(SpringLayout.WEST, btnUtile, 0, SpringLayout.WEST, lblBilancio);
        frmBilancioGui.getContentPane().add(btnUtile);
        
        JButton btnUtileMax = new JButton("Utile Max Registrato");
        btnUtileMax.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = oBil.getRS_UtilMax();
                tableBilancio.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.EAST, btnVisBilancio, 0, SpringLayout.EAST, btnUtileMax);
        springLayout.putConstraint(SpringLayout.NORTH, btnUtileMax, 0, SpringLayout.NORTH, btnUtile);
        springLayout.putConstraint(SpringLayout.WEST, btnUtileMax, 6, SpringLayout.EAST, btnUtile);
        frmBilancioGui.getContentPane().add(btnUtileMax);
        
        JButton btnStipendioMax = new JButton("Stipendio massimo dipendenti");
        btnStipendioMax.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = oBil.getRS_SalarioDip();
                tableBilancio.setModel(DbUtils.resultSetToTableModel(rS));
                
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnStipendioMax, 6, SpringLayout.SOUTH, btnUtile);
        springLayout.putConstraint(SpringLayout.WEST, btnStipendioMax, 0, SpringLayout.WEST, lblBilancio);
        springLayout.putConstraint(SpringLayout.EAST, btnStipendioMax, 0, SpringLayout.EAST, btnVisBilancio);
        frmBilancioGui.getContentPane().add(btnStipendioMax);
    }
    
    public void run() {
        try {
            BilancioGUI window = new BilancioGUI();
            window.frmBilancioGui.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
