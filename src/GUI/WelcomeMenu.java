package GUI;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.SystemColor;
//
public class WelcomeMenu {

    private JFrame frmBenvenutoInBordonidb;
    /**
     * Create the application.
     */
    public WelcomeMenu() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmBenvenutoInBordonidb = new JFrame();
        frmBenvenutoInBordonidb.setResizable(false);
        frmBenvenutoInBordonidb.setTitle("Benvenuto in BordoniDB");
        frmBenvenutoInBordonidb.setBackground(Color.LIGHT_GRAY);
        frmBenvenutoInBordonidb.setBounds(100, 100, 450, 300);
        frmBenvenutoInBordonidb.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmBenvenutoInBordonidb.getContentPane().setLayout(null);
        
        JLabel lblTitolo = new JLabel("Bordoni Luce");
        lblTitolo.setForeground(SystemColor.textHighlight);
        lblTitolo.setHorizontalAlignment(SwingConstants.CENTER);
        lblTitolo.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 45));
        lblTitolo.setBounds(51, 34, 331, 80);
        frmBenvenutoInBordonidb.getContentPane().add(lblTitolo);
        
        JButton btnEntra = new JButton("ENTRA");
        btnEntra.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MainMenu m_M = new MainMenu();
                m_M.run();
                frmBenvenutoInBordonidb.dispose();
            }
        });
        btnEntra.setFont(new Font("Calibri", Font.BOLD, 25));
        btnEntra.setBounds(148, 136, 142, 56);
        frmBenvenutoInBordonidb.getContentPane().add(btnEntra);
    }
    
    public void run() {
        try {
            WelcomeMenu window = new WelcomeMenu();
            window.frmBenvenutoInBordonidb.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
