package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SpringLayout;

import Prodotto.OperationProdotto;
import Prodotto.Prodotto;
import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class ProdottoGUI {

    private JFrame frmProdottoGui;
    private JTextField textCodProd;
    private JTextField textDimensione;
    private JTextField textPrezzoUnitario;
    private JTextField textDescrizione;
    private JTable tableProdotto;
    
    private List<JTextField> textFieldsList = new ArrayList<JTextField>();
    private List<String> stringFieldsList = new ArrayList<String>();
    
    private OperationProdotto oProd = new OperationProdotto();

    /**
     * Create the application.
     */
    public ProdottoGUI() throws SQLException, ClassNotFoundException{
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmProdottoGui = new JFrame();
        frmProdottoGui.setTitle("Prodotto GUI");
        frmProdottoGui.setResizable(false);
        frmProdottoGui.setBounds(100, 100, 1200, 545);
        frmProdottoGui.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        SpringLayout springLayout = new SpringLayout();
        frmProdottoGui.getContentPane().setLayout(springLayout);
        
        JLabel lblTitle = new JLabel("PRODOTTO");
        springLayout.putConstraint(SpringLayout.NORTH, lblTitle, 10, SpringLayout.NORTH, frmProdottoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblTitle, 492, SpringLayout.WEST, frmProdottoGui.getContentPane());
        lblTitle.setFont(new Font("Tahoma", Font.BOLD, 40));
        frmProdottoGui.getContentPane().add(lblTitle);
        
        JLabel lblSubTitle = new JLabel("Inserisci Prodotto:");
        lblSubTitle.setFont(new Font("Tahoma", Font.BOLD, 20));
        springLayout.putConstraint(SpringLayout.NORTH, lblSubTitle, 71, SpringLayout.NORTH, frmProdottoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblSubTitle, 10, SpringLayout.WEST, frmProdottoGui.getContentPane());
        frmProdottoGui.getContentPane().add(lblSubTitle);
        
        JLabel lblCodProd = new JLabel("Codice Prodotto:");
        lblCodProd.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblCodProd, 19, SpringLayout.SOUTH, lblSubTitle);
        springLayout.putConstraint(SpringLayout.WEST, lblCodProd, 0, SpringLayout.WEST, lblSubTitle);
        frmProdottoGui.getContentPane().add(lblCodProd);
        
        textCodProd = new JTextField();
        textFieldsList.add(textCodProd);
        springLayout.putConstraint(SpringLayout.NORTH, textCodProd, 0, SpringLayout.NORTH, lblCodProd);
        springLayout.putConstraint(SpringLayout.WEST, textCodProd, 6, SpringLayout.EAST, lblCodProd);
        frmProdottoGui.getContentPane().add(textCodProd);
        textCodProd.setColumns(10);
        
        JLabel lblDimensione = new JLabel("Dimensione:");
        lblDimensione.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblDimensione, 22, SpringLayout.SOUTH, lblCodProd);
        springLayout.putConstraint(SpringLayout.WEST, lblDimensione, 0, SpringLayout.WEST, lblSubTitle);
        frmProdottoGui.getContentPane().add(lblDimensione);
        
        textDimensione = new JTextField();
        textFieldsList.add(textDimensione);
        springLayout.putConstraint(SpringLayout.WEST, textDimensione, 6, SpringLayout.EAST, lblDimensione);
        springLayout.putConstraint(SpringLayout.SOUTH, textDimensione, 0, SpringLayout.SOUTH, lblDimensione);
        frmProdottoGui.getContentPane().add(textDimensione);
        textDimensione.setColumns(10);
        
        JLabel lblPrezzoUnitario = new JLabel("Prezzo Unitario:");
        lblPrezzoUnitario.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblPrezzoUnitario, 22, SpringLayout.SOUTH, lblDimensione);
        springLayout.putConstraint(SpringLayout.WEST, lblPrezzoUnitario, 0, SpringLayout.WEST, lblSubTitle);
        frmProdottoGui.getContentPane().add(lblPrezzoUnitario);
        
        textPrezzoUnitario = new JTextField();
        textFieldsList.add(textPrezzoUnitario);
        springLayout.putConstraint(SpringLayout.NORTH, textPrezzoUnitario, 0, SpringLayout.NORTH, lblPrezzoUnitario);
        springLayout.putConstraint(SpringLayout.WEST, textPrezzoUnitario, 11, SpringLayout.EAST, lblPrezzoUnitario);
        frmProdottoGui.getContentPane().add(textPrezzoUnitario);
        textPrezzoUnitario.setColumns(10);
        
        JLabel lblTipo = new JLabel("Tipo:");
        lblTipo.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblTipo, 24, SpringLayout.SOUTH, lblPrezzoUnitario);
        springLayout.putConstraint(SpringLayout.WEST, lblTipo, 10, SpringLayout.WEST, frmProdottoGui.getContentPane());
        frmProdottoGui.getContentPane().add(lblTipo);
        
        JRadioButton rdbtnInterno = new JRadioButton("Interno");
        rdbtnInterno.setActionCommand("Interno");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnInterno, -1, SpringLayout.NORTH, lblTipo);
        springLayout.putConstraint(SpringLayout.WEST, rdbtnInterno, 6, SpringLayout.EAST, lblTipo);
        frmProdottoGui.getContentPane().add(rdbtnInterno);
        
        JRadioButton rdbtnEsterno = new JRadioButton("Esterno");
        rdbtnEsterno.setActionCommand("Esterno");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnEsterno, -1, SpringLayout.NORTH, lblTipo);
        springLayout.putConstraint(SpringLayout.WEST, rdbtnEsterno, 0, SpringLayout.WEST, textPrezzoUnitario);
        frmProdottoGui.getContentPane().add(rdbtnEsterno);
        
        ButtonGroup group = new ButtonGroup();
        group.add(rdbtnInterno);
        group.add(rdbtnEsterno);
        
        JLabel lblDescrizione = new JLabel("Descrizione:");
        lblDescrizione.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblDescrizione, 10, SpringLayout.SOUTH, rdbtnInterno);
        springLayout.putConstraint(SpringLayout.WEST, lblDescrizione, 0, SpringLayout.WEST, lblSubTitle);
        frmProdottoGui.getContentPane().add(lblDescrizione);
        
        textDescrizione = new JTextField();
        textFieldsList.add(textDescrizione);
        springLayout.putConstraint(SpringLayout.NORTH, textDescrizione, 6, SpringLayout.SOUTH, lblDescrizione);
        springLayout.putConstraint(SpringLayout.WEST, textDescrizione, 10, SpringLayout.WEST, frmProdottoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, textDescrizione, 84, SpringLayout.SOUTH, lblDescrizione);
        springLayout.putConstraint(SpringLayout.EAST, textDescrizione, 197, SpringLayout.WEST, frmProdottoGui.getContentPane());
        frmProdottoGui.getContentPane().add(textDescrizione);
        textDescrizione.setColumns(10);
        
        JButton btnInserisci = new JButton("INSERISCI");
        btnInserisci.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (JTextField textField : textFieldsList) {
                    stringFieldsList.add(textField.getText());
                }
                stringFieldsList.add(group.getSelection().getActionCommand());
                Prodotto prod = new Prodotto(stringFieldsList);
                try {
                    boolean check = oProd.insertProdotto(prod);
                    if(!check) {
                        stringFieldsList.clear();
                    }else {
                        ResultSet rS = oProd.getRS_Prodotto();
                        tableProdotto.setModel(DbUtils.resultSetToTableModel(rS));
                    }                    
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                stringFieldsList.clear();
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnInserisci, 21, SpringLayout.SOUTH, textDescrizione);
        springLayout.putConstraint(SpringLayout.WEST, btnInserisci, 0, SpringLayout.WEST, lblSubTitle);
        frmProdottoGui.getContentPane().add(btnInserisci);
        
        JButton btnReset = new JButton("RESET");
        btnReset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (JTextField textField : textFieldsList) {
                    textField.setText("");
                }
                stringFieldsList.clear();
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnReset, 0, SpringLayout.NORTH, btnInserisci);
        springLayout.putConstraint(SpringLayout.EAST, btnReset, 0, SpringLayout.EAST, lblSubTitle);
        frmProdottoGui.getContentPane().add(btnReset);
        
        JLabel lblProdotto = new JLabel("Prodotto:");
        lblProdotto.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblProdotto, 6, SpringLayout.SOUTH, lblTitle);
        springLayout.putConstraint(SpringLayout.WEST, lblProdotto, 0, SpringLayout.WEST, lblTitle);
        frmProdottoGui.getContentPane().add(lblProdotto);
        
        tableProdotto = new JTable() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        JScrollPane scrollProd = new JScrollPane(tableProdotto);
        springLayout.putConstraint(SpringLayout.NORTH, scrollProd, 6, SpringLayout.SOUTH, lblProdotto);
        springLayout.putConstraint(SpringLayout.WEST, scrollProd, 0, SpringLayout.WEST, lblTitle);
        springLayout.putConstraint(SpringLayout.SOUTH, scrollProd, 0, SpringLayout.SOUTH, lblTipo);
        springLayout.putConstraint(SpringLayout.EAST, scrollProd, -74, SpringLayout.EAST, frmProdottoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, tableProdotto, 7, SpringLayout.SOUTH, lblProdotto);
        springLayout.putConstraint(SpringLayout.WEST, tableProdotto, 0, SpringLayout.WEST, lblTitle);
        springLayout.putConstraint(SpringLayout.SOUTH, tableProdotto, 0, SpringLayout.SOUTH, rdbtnInterno);
        springLayout.putConstraint(SpringLayout.EAST, tableProdotto, -131, SpringLayout.EAST, frmProdottoGui.getContentPane());
        frmProdottoGui.getContentPane().add(scrollProd);
        
        JButton btnVisProd = new JButton("VISUALIZZA");
        btnVisProd.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = oProd.getRS_Prodotto();
                tableProdotto.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnVisProd, 0, SpringLayout.NORTH, lblDescrizione);
        springLayout.putConstraint(SpringLayout.WEST, btnVisProd, 0, SpringLayout.WEST, lblTitle);
        frmProdottoGui.getContentPane().add(btnVisProd);
        
        JLabel lblAltro = new JLabel("Altro:");
        lblAltro.setFont(new Font("Tahoma", Font.BOLD, 20));
        springLayout.putConstraint(SpringLayout.NORTH, lblAltro, 28, SpringLayout.SOUTH, btnVisProd);
        springLayout.putConstraint(SpringLayout.WEST, lblAltro, 0, SpringLayout.WEST, lblTitle);
        frmProdottoGui.getContentPane().add(lblAltro);
        
        JButton btnProdMaxVend = new JButton("Prodotto pi\u00F9 Venduto");
        btnProdMaxVend.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = oProd.getRS_ProdottoPi�Venduto();
                tableProdotto.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnProdMaxVend, 13, SpringLayout.SOUTH, lblAltro);
        springLayout.putConstraint(SpringLayout.WEST, btnProdMaxVend, 0, SpringLayout.WEST, lblTitle);
        frmProdottoGui.getContentPane().add(btnProdMaxVend);
    }
    
    public void run() {
        try {
            ProdottoGUI window = new ProdottoGUI();
            window.frmProdottoGui.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
