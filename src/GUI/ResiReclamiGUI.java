package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SpringLayout;

import Resi_e_Reclamo.OperationResoReclamo;
import Resi_e_Reclamo.Reclamo;
import Resi_e_Reclamo.Reso;
import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class ResiReclamiGUI {

    private JFrame frmResiEReclami;
    private JTable tableResi;
    private JTable tableReclami;
    private JTextField textCodice;
    private JTextField textNumOrdine;
    private JTextField textAnno;
    private JTextField textData;
    private JTextField textDescrizione;
    
    private OperationResoReclamo oReRec = new OperationResoReclamo();
    private ButtonGroup resoReclamo;

    /**
     * Create the application.
     */
    public ResiReclamiGUI() throws SQLException, ClassNotFoundException{
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmResiEReclami = new JFrame();
        frmResiEReclami.setTitle("Resi e Reclami GUI");
        frmResiEReclami.setResizable(false);
        frmResiEReclami.setAlwaysOnTop(true);
        frmResiEReclami.setBounds(100, 100, 1240, 710);
        frmResiEReclami.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        SpringLayout springLayout = new SpringLayout();
        frmResiEReclami.getContentPane().setLayout(springLayout);
        
        JLabel lblTitle = new JLabel("Resi e Reclami");
        springLayout.putConstraint(SpringLayout.NORTH, lblTitle, 10, SpringLayout.NORTH, frmResiEReclami.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, lblTitle, -467, SpringLayout.EAST, frmResiEReclami.getContentPane());
        lblTitle.setFont(new Font("Tahoma", Font.BOLD, 40));
        frmResiEReclami.getContentPane().add(lblTitle);
        
        JLabel lblVisResi = new JLabel("Resi:");
        springLayout.putConstraint(SpringLayout.WEST, lblVisResi, 561, SpringLayout.WEST, frmResiEReclami.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, lblVisResi, -580, SpringLayout.SOUTH, frmResiEReclami.getContentPane());
        lblVisResi.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmResiEReclami.getContentPane().add(lblVisResi);
        
        tableResi = new JTable() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        JScrollPane scrollResi = new JScrollPane(tableResi);
        springLayout.putConstraint(SpringLayout.NORTH, scrollResi, 13, SpringLayout.SOUTH, lblVisResi);
        springLayout.putConstraint(SpringLayout.WEST, scrollResi, 0, SpringLayout.WEST, lblVisResi);
        springLayout.putConstraint(SpringLayout.EAST, scrollResi, -186, SpringLayout.EAST, frmResiEReclami.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, tableResi, 6, SpringLayout.SOUTH, lblVisResi);
        springLayout.putConstraint(SpringLayout.WEST, tableResi, 0, SpringLayout.WEST, lblVisResi);
        springLayout.putConstraint(SpringLayout.SOUTH, tableResi, -408, SpringLayout.SOUTH, frmResiEReclami.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, tableResi, -47, SpringLayout.EAST, frmResiEReclami.getContentPane());
        frmResiEReclami.getContentPane().add(scrollResi);
        
        JLabel lblVisReclami = new JLabel("Reclami:");
        springLayout.putConstraint(SpringLayout.NORTH, lblVisReclami, 73, SpringLayout.SOUTH, tableResi);
        springLayout.putConstraint(SpringLayout.WEST, lblVisReclami, 0, SpringLayout.WEST, lblVisResi);
        lblVisReclami.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmResiEReclami.getContentPane().add(lblVisReclami);
        
        tableReclami = new JTable() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        JScrollPane scrollReclami = new JScrollPane(tableReclami); 
        springLayout.putConstraint(SpringLayout.NORTH, scrollReclami, 10, SpringLayout.SOUTH, lblVisReclami);
        springLayout.putConstraint(SpringLayout.WEST, scrollReclami, 0, SpringLayout.WEST, lblVisResi);
        springLayout.putConstraint(SpringLayout.EAST, scrollReclami, 430, SpringLayout.EAST, lblVisReclami);
        springLayout.putConstraint(SpringLayout.NORTH, tableReclami, 6, SpringLayout.SOUTH, lblVisReclami);
        springLayout.putConstraint(SpringLayout.WEST, tableReclami, 0, SpringLayout.WEST, lblVisResi);
        springLayout.putConstraint(SpringLayout.SOUTH, tableReclami, 172, SpringLayout.SOUTH, lblVisReclami);
        springLayout.putConstraint(SpringLayout.EAST, tableReclami, 0, SpringLayout.EAST, tableResi);
        frmResiEReclami.getContentPane().add(scrollReclami);
        
        JButton btnVisResi = new JButton("VISUALIZZA");
        btnVisResi.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = oReRec.getRS_ResReclamo("Reso");
                tableResi.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnVisResi, 6, SpringLayout.SOUTH, scrollResi);
        springLayout.putConstraint(SpringLayout.WEST, btnVisResi, 0, SpringLayout.WEST, lblVisResi);
        frmResiEReclami.getContentPane().add(btnVisResi);
        
        JButton btnVisReclami = new JButton("VISUALIZZA");
        btnVisReclami.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = oReRec.getRS_ResReclamo("Reclamo");
                tableReclami.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnVisReclami, 6, SpringLayout.SOUTH, scrollReclami);
        springLayout.putConstraint(SpringLayout.WEST, btnVisReclami, 0, SpringLayout.WEST, lblVisResi);
        frmResiEReclami.getContentPane().add(btnVisReclami);
        
        JLabel lblNewLabel = new JLabel("Inserisci Reso o Reclamo:");
        springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel, -6, SpringLayout.NORTH, lblVisResi);
        springLayout.putConstraint(SpringLayout.WEST, lblNewLabel, 10, SpringLayout.WEST, frmResiEReclami.getContentPane());
        lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
        frmResiEReclami.getContentPane().add(lblNewLabel);
        
        JLabel lblCodice = new JLabel("Codice:");
        lblCodice.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblCodice, 22, SpringLayout.SOUTH, lblNewLabel);
        springLayout.putConstraint(SpringLayout.WEST, lblCodice, 0, SpringLayout.WEST, lblNewLabel);
        frmResiEReclami.getContentPane().add(lblCodice);
        
        textCodice = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textCodice, 0, SpringLayout.NORTH, lblCodice);
        springLayout.putConstraint(SpringLayout.WEST, textCodice, 6, SpringLayout.EAST, lblCodice);
        textCodice.setEnabled(false);
        frmResiEReclami.getContentPane().add(textCodice);
        textCodice.setColumns(10);
        
        JLabel lblNumOrdine = new JLabel("Numero Ordine:");
        lblNumOrdine.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblNumOrdine, 190, SpringLayout.NORTH, frmResiEReclami.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblNumOrdine, 0, SpringLayout.WEST, lblNewLabel);
        frmResiEReclami.getContentPane().add(lblNumOrdine);
        
        JLabel lblTipo = new JLabel("Tipo:");
        springLayout.putConstraint(SpringLayout.WEST, lblTipo, 0, SpringLayout.WEST, lblNewLabel);
        springLayout.putConstraint(SpringLayout.SOUTH, lblTipo, -15, SpringLayout.NORTH, lblNumOrdine);
        lblTipo.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmResiEReclami.getContentPane().add(lblTipo);
        
        JRadioButton rdbtnReso = new JRadioButton("Reso");
        rdbtnReso.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textCodice.setText(oReRec.getRS_NewNumReso());
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnReso, -1, SpringLayout.NORTH, lblTipo);
        rdbtnReso.setActionCommand("Reso");
        if(rdbtnReso.isSelected())
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnReso, -1, SpringLayout.NORTH, lblTipo);
        rdbtnReso.setFont(new Font("Tahoma", Font.PLAIN, 11));
        frmResiEReclami.getContentPane().add(rdbtnReso);
        
        JRadioButton rdbtnReclamo = new JRadioButton("Reclamo");
        rdbtnReclamo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textCodice.setText(oReRec.getRS_NewNumReclamo());
            }
        });
        rdbtnReclamo.setActionCommand("Reclamo");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnReclamo, -1, SpringLayout.NORTH, lblTipo);
        springLayout.putConstraint(SpringLayout.WEST, rdbtnReclamo, 6, SpringLayout.EAST, lblTipo);
        frmResiEReclami.getContentPane().add(rdbtnReclamo);
        
        resoReclamo = new ButtonGroup();
        resoReclamo.add(rdbtnReclamo);
        resoReclamo.add(rdbtnReso);
        
        textNumOrdine = new JTextField();
        springLayout.putConstraint(SpringLayout.WEST, rdbtnReso, 0, SpringLayout.WEST, textNumOrdine);
        springLayout.putConstraint(SpringLayout.NORTH, textNumOrdine, 0, SpringLayout.NORTH, lblNumOrdine);
        springLayout.putConstraint(SpringLayout.WEST, textNumOrdine, 6, SpringLayout.EAST, lblNumOrdine);
        frmResiEReclami.getContentPane().add(textNumOrdine);
        textNumOrdine.setColumns(10);
        
        JLabel lblAnno = new JLabel("Anno:");
        springLayout.putConstraint(SpringLayout.SOUTH, scrollResi, 19, SpringLayout.SOUTH, lblAnno);
        springLayout.putConstraint(SpringLayout.NORTH, lblAnno, 14, SpringLayout.SOUTH, lblNumOrdine);
        springLayout.putConstraint(SpringLayout.WEST, lblAnno, 0, SpringLayout.WEST, lblNewLabel);
        lblAnno.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmResiEReclami.getContentPane().add(lblAnno);
        
        textAnno = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textAnno, 0, SpringLayout.NORTH, lblAnno);
        springLayout.putConstraint(SpringLayout.WEST, textAnno, 0, SpringLayout.WEST, textCodice);
        textAnno.setEnabled(false);
        textAnno.setText("2020");
        frmResiEReclami.getContentPane().add(textAnno);
        textAnno.setColumns(10);
        
        JLabel lblData = new JLabel("Data (AAAA-MM-GG):");
        springLayout.putConstraint(SpringLayout.NORTH, lblData, 16, SpringLayout.SOUTH, textAnno);
        springLayout.putConstraint(SpringLayout.WEST, lblData, 0, SpringLayout.WEST, lblNewLabel);
        lblData.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmResiEReclami.getContentPane().add(lblData);
        
        textData = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textData, 47, SpringLayout.SOUTH, textNumOrdine);
        springLayout.putConstraint(SpringLayout.WEST, textData, 7, SpringLayout.EAST, lblData);
        springLayout.putConstraint(SpringLayout.EAST, textData, 139, SpringLayout.EAST, lblData);
        frmResiEReclami.getContentPane().add(textData);
        textData.setColumns(10);
        
        JLabel lblDescrizione = new JLabel("Descrizione:");
        springLayout.putConstraint(SpringLayout.NORTH, lblDescrizione, 18, SpringLayout.SOUTH, lblData);
        springLayout.putConstraint(SpringLayout.WEST, lblDescrizione, 0, SpringLayout.WEST, lblNewLabel);
        lblDescrizione.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmResiEReclami.getContentPane().add(lblDescrizione);
        
        textDescrizione = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textDescrizione, -23, SpringLayout.NORTH, lblVisReclami);
        springLayout.putConstraint(SpringLayout.WEST, textDescrizione, 0, SpringLayout.WEST, lblNewLabel);
        springLayout.putConstraint(SpringLayout.SOUTH, textDescrizione, -291, SpringLayout.SOUTH, frmResiEReclami.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, textDescrizione, 0, SpringLayout.EAST, textData);
        frmResiEReclami.getContentPane().add(textDescrizione);
        textDescrizione.setColumns(10);
        
       
        
        JButton btnInserisci = new JButton("INSERISCI");
        btnInserisci.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String selected = resoReclamo.getSelection().getActionCommand();
                if(selected.equals("Reso")) {
                    Reso res = new Reso(Integer.parseInt(textNumOrdine.getText()), textData.getText(), textDescrizione.getText());
                    try {
                        oReRec.insertReso(res);
                        ResultSet rS = oReRec.getRS_ResReclamo("Reso");
                        tableResi.setModel(DbUtils.resultSetToTableModel(rS));
                        textCodice.setText(oReRec.getRS_NewNumReso());
                        
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                    
                }else {
                    Reclamo rec = new Reclamo(Integer.parseInt(textNumOrdine.getText()), textData.getText(), textDescrizione.getText());
                    try {
                        oReRec.insertReclamo(rec);
                        ResultSet rS = oReRec.getRS_ResReclamo("Reclamo");
                        tableReclami.setModel(DbUtils.resultSetToTableModel(rS));
                        textCodice.setText(oReRec.getRS_NewNumReclamo());
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
        springLayout.putConstraint(SpringLayout.SOUTH, scrollReclami, 109, SpringLayout.NORTH, btnInserisci);
        springLayout.putConstraint(SpringLayout.NORTH, btnInserisci, 17, SpringLayout.SOUTH, textDescrizione);
        springLayout.putConstraint(SpringLayout.WEST, btnInserisci, 0, SpringLayout.WEST, lblNewLabel);
        frmResiEReclami.getContentPane().add(btnInserisci);
        
        JButton btnReset = new JButton("RESET");
        btnReset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textData.setText("");
                textNumOrdine.setText("");
                textDescrizione.setText("");
            }
        });
        springLayout.putConstraint(SpringLayout.WEST, btnReset, 11, SpringLayout.EAST, btnInserisci);
        springLayout.putConstraint(SpringLayout.SOUTH, btnReset, 0, SpringLayout.SOUTH, btnInserisci);
        frmResiEReclami.getContentPane().add(btnReset);
        
        
    }
    
    public void run() {
        try {
            ResiReclamiGUI window = new ResiReclamiGUI();
            window.frmResiEReclami.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
