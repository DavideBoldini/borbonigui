package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SpringLayout;

import Dirigenti.DirUff;
import Dirigenti.OperationDirUff;
import Rapporto_Amministrazione.OperationRapAm;
import Rapporto_Amministrazione.RapAm;
import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Scrollbar;
import java.util.ArrayList;
import java.util.List;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class DirigenteUfficioGUI {

    private JFrame frmDirigenteUfficioGui;
    private JTextField textCodLav;
    private JTextField textCF;
    private JTextField textNome;
    private JTextField textCognome;
    private JTextField textD_Nascita;
    private JTextField textTelefono;
    private JTextField textVia;
    private JTextField textCivico;
    private JTextField textCitt�;
    private JTextField textSalario;
    private JTextField textCodUff;
    private JTable TableDirUff;
    private JTable TableRappAmm;
    private JTextField textDelCodLav;
    private JTextField textVisCodLav;
    
    private List<JTextField> fieldsList = new ArrayList<JTextField>();
    private List<String> stringsfieldList = new ArrayList<String>();
    
    private List<JTextField> fieldsList_2 = new ArrayList<JTextField>();
    private List<String> stringsfieldList_2 = new ArrayList<String>();
    
    private OperationDirUff opDirUff = new OperationDirUff();
    private OperationRapAm opRapAm = new OperationRapAm();
    private DirUff dUff;
    private JTextField textDataAmm;
    private JTextField textDescrAmm;
    private JTextField textCodDirAmm;
    private JTextField textCodSocioAmm;


    
    /**
     * Create the application.
     */
    public DirigenteUfficioGUI() throws SQLException, ClassNotFoundException{
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmDirigenteUfficioGui = new JFrame();
        frmDirigenteUfficioGui.setResizable(false);
        frmDirigenteUfficioGui.setTitle("Dirigente Ufficio GUI");
        frmDirigenteUfficioGui.setAlwaysOnTop(true);
        frmDirigenteUfficioGui.setBounds(100, 100, 1240, 710);
        frmDirigenteUfficioGui.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        SpringLayout springLayout = new SpringLayout();
        frmDirigenteUfficioGui.getContentPane().setLayout(springLayout);
        
        JLabel lblTitle = new JLabel("DIRIGENTE UFFICIO");
        lblTitle.setBackground(Color.DARK_GRAY);
        springLayout.putConstraint(SpringLayout.NORTH, lblTitle, 10, SpringLayout.NORTH, frmDirigenteUfficioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblTitle, 402, SpringLayout.WEST, frmDirigenteUfficioGui.getContentPane());
        lblTitle.setFont(new Font("Tahoma", Font.BOLD, 40));
        frmDirigenteUfficioGui.getContentPane().add(lblTitle);
        
        JLabel lblSubTitle = new JLabel("Inserisci Dirigente Ufficio:");
        springLayout.putConstraint(SpringLayout.NORTH, lblSubTitle, 72, SpringLayout.NORTH, frmDirigenteUfficioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblSubTitle, 10, SpringLayout.WEST, frmDirigenteUfficioGui.getContentPane());
        lblSubTitle.setFont(new Font("Tahoma", Font.BOLD, 20));
        frmDirigenteUfficioGui.getContentPane().add(lblSubTitle);
        
        JLabel lblCodLav = new JLabel("Codice Lavoratore:");
        lblCodLav.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblCodLav, 20, SpringLayout.SOUTH, lblSubTitle);
        springLayout.putConstraint(SpringLayout.WEST, lblCodLav, 10, SpringLayout.WEST, frmDirigenteUfficioGui.getContentPane());
        frmDirigenteUfficioGui.getContentPane().add(lblCodLav);
        
        textCodLav = new JTextField();
        fieldsList.add(textCodLav);
        springLayout.putConstraint(SpringLayout.WEST, textCodLav, 6, SpringLayout.EAST, lblCodLav);
        springLayout.putConstraint(SpringLayout.EAST, textCodLav, 137, SpringLayout.EAST, lblCodLav);
        frmDirigenteUfficioGui.getContentPane().add(textCodLav);
        textCodLav.setColumns(10);
        
        JLabel lblCF = new JLabel("Codice Fiscale:");
        lblCF.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblCF, 18, SpringLayout.SOUTH, lblCodLav);
        springLayout.putConstraint(SpringLayout.WEST, lblCF, 0, SpringLayout.WEST, lblSubTitle);
        frmDirigenteUfficioGui.getContentPane().add(lblCF);
        
        textCF = new JTextField();
        fieldsList.add(textCF);
        springLayout.putConstraint(SpringLayout.SOUTH, textCodLav, -18, SpringLayout.NORTH, textCF);
        springLayout.putConstraint(SpringLayout.NORTH, textCF, 0, SpringLayout.NORTH, lblCF);
        springLayout.putConstraint(SpringLayout.WEST, textCF, 6, SpringLayout.EAST, lblCF);
        springLayout.putConstraint(SpringLayout.EAST, textCF, 161, SpringLayout.EAST, lblCF);
        frmDirigenteUfficioGui.getContentPane().add(textCF);
        textCF.setColumns(10);
        
        JLabel lblNome = new JLabel("Nome:");
        lblNome.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblNome, 17, SpringLayout.SOUTH, lblCF);
        springLayout.putConstraint(SpringLayout.WEST, lblNome, 0, SpringLayout.WEST, lblSubTitle);
        frmDirigenteUfficioGui.getContentPane().add(lblNome);
        
        textNome = new JTextField();
        fieldsList.add(textNome);
        springLayout.putConstraint(SpringLayout.NORTH, textNome, 14, SpringLayout.SOUTH, textCF);
        springLayout.putConstraint(SpringLayout.WEST, textNome, 6, SpringLayout.EAST, lblNome);
        frmDirigenteUfficioGui.getContentPane().add(textNome);
        textNome.setColumns(10);
        
        JLabel lblCognome = new JLabel("Cognome:");
        springLayout.putConstraint(SpringLayout.WEST, lblCognome, 250, SpringLayout.WEST, frmDirigenteUfficioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, textNome, -24, SpringLayout.WEST, lblCognome);
        springLayout.putConstraint(SpringLayout.NORTH, lblCognome, 0, SpringLayout.NORTH, lblNome);
        lblCognome.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmDirigenteUfficioGui.getContentPane().add(lblCognome);
        
        textCognome = new JTextField();
        fieldsList.add(textCognome);
        springLayout.putConstraint(SpringLayout.NORTH, textCognome, 0, SpringLayout.NORTH, lblNome);
        springLayout.putConstraint(SpringLayout.WEST, textCognome, 6, SpringLayout.EAST, lblCognome);
        frmDirigenteUfficioGui.getContentPane().add(textCognome);
        textCognome.setColumns(10);
        
        JLabel lblD_Nascita = new JLabel("Data Nascita (AAAA-MM-GG):");
        springLayout.putConstraint(SpringLayout.NORTH, lblD_Nascita, 18, SpringLayout.SOUTH, textNome);
        springLayout.putConstraint(SpringLayout.WEST, lblD_Nascita, 0, SpringLayout.WEST, lblSubTitle);
        lblD_Nascita.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmDirigenteUfficioGui.getContentPane().add(lblD_Nascita);
        
        textD_Nascita = new JTextField();
        fieldsList.add(textD_Nascita);
        springLayout.putConstraint(SpringLayout.NORTH, textD_Nascita, 0, SpringLayout.NORTH, lblD_Nascita);
        springLayout.putConstraint(SpringLayout.WEST, textD_Nascita, 6, SpringLayout.EAST, lblD_Nascita);
        springLayout.putConstraint(SpringLayout.EAST, textD_Nascita, -869, SpringLayout.EAST, frmDirigenteUfficioGui.getContentPane());
        frmDirigenteUfficioGui.getContentPane().add(textD_Nascita);
        textD_Nascita.setColumns(10);
        
        JLabel lblTelefono = new JLabel("Telefono:");
        springLayout.putConstraint(SpringLayout.NORTH, lblTelefono, 16, SpringLayout.SOUTH, lblD_Nascita);
        springLayout.putConstraint(SpringLayout.WEST, lblTelefono, 10, SpringLayout.WEST, frmDirigenteUfficioGui.getContentPane());
        lblTelefono.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmDirigenteUfficioGui.getContentPane().add(lblTelefono);
        
        textTelefono = new JTextField();
        fieldsList.add(textTelefono);
        springLayout.putConstraint(SpringLayout.NORTH, textTelefono, 0, SpringLayout.NORTH, lblTelefono);
        springLayout.putConstraint(SpringLayout.WEST, textTelefono, 82, SpringLayout.WEST, frmDirigenteUfficioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, textTelefono, 0, SpringLayout.EAST, lblD_Nascita);
        frmDirigenteUfficioGui.getContentPane().add(textTelefono);
        textTelefono.setColumns(10);
        
        JLabel lblVia = new JLabel("Via:");
        springLayout.putConstraint(SpringLayout.NORTH, lblVia, 17, SpringLayout.SOUTH, lblTelefono);
        springLayout.putConstraint(SpringLayout.WEST, lblVia, 0, SpringLayout.WEST, lblSubTitle);
        lblVia.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmDirigenteUfficioGui.getContentPane().add(lblVia);
        
        textVia = new JTextField();
        fieldsList.add(textVia);
        springLayout.putConstraint(SpringLayout.NORTH, textVia, 14, SpringLayout.SOUTH, textTelefono);
        springLayout.putConstraint(SpringLayout.WEST, textVia, 6, SpringLayout.EAST, lblVia);
        frmDirigenteUfficioGui.getContentPane().add(textVia);
        textVia.setColumns(10);
        
        JLabel lblCivico = new JLabel("Civico:");
        springLayout.putConstraint(SpringLayout.EAST, textVia, -27, SpringLayout.WEST, lblCivico);
        springLayout.putConstraint(SpringLayout.NORTH, lblCivico, 0, SpringLayout.NORTH, lblVia);
        springLayout.putConstraint(SpringLayout.EAST, lblCivico, 0, SpringLayout.EAST, textCodLav);
        lblCivico.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmDirigenteUfficioGui.getContentPane().add(lblCivico);
        
        textCivico = new JTextField();
        fieldsList.add(textCivico);
        springLayout.putConstraint(SpringLayout.NORTH, textCivico, 0, SpringLayout.NORTH, lblVia);
        springLayout.putConstraint(SpringLayout.WEST, textCivico, 6, SpringLayout.EAST, lblCivico);
        frmDirigenteUfficioGui.getContentPane().add(textCivico);
        textCivico.setColumns(10);
        
        JLabel lblCitt� = new JLabel("Citt\u00E0:");
        springLayout.putConstraint(SpringLayout.NORTH, lblCitt�, 18, SpringLayout.SOUTH, textVia);
        springLayout.putConstraint(SpringLayout.WEST, lblCitt�, 0, SpringLayout.WEST, lblSubTitle);
        lblCitt�.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmDirigenteUfficioGui.getContentPane().add(lblCitt�);
        
        textCitt� = new JTextField();
        fieldsList.add(textCitt�);
        springLayout.putConstraint(SpringLayout.NORTH, textCitt�, 18, SpringLayout.SOUTH, textVia);
        springLayout.putConstraint(SpringLayout.WEST, textCitt�, 7, SpringLayout.EAST, lblCitt�);
        springLayout.putConstraint(SpringLayout.EAST, textCitt�, 0, SpringLayout.EAST, textVia);
        frmDirigenteUfficioGui.getContentPane().add(textCitt�);
        textCitt�.setColumns(10);
        
        JLabel lblSalario = new JLabel("Salario:");
        lblSalario.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblSalario, 19, SpringLayout.SOUTH, lblCitt�);
        springLayout.putConstraint(SpringLayout.WEST, lblSalario, 0, SpringLayout.WEST, lblSubTitle);
        frmDirigenteUfficioGui.getContentPane().add(lblSalario);
        
        textSalario = new JTextField();
        fieldsList.add(textSalario);
        springLayout.putConstraint(SpringLayout.NORTH, textSalario, 0, SpringLayout.NORTH, lblSalario);
        springLayout.putConstraint(SpringLayout.WEST, textSalario, 0, SpringLayout.WEST, textNome);
        springLayout.putConstraint(SpringLayout.EAST, textSalario, 172, SpringLayout.WEST, frmDirigenteUfficioGui.getContentPane());
        frmDirigenteUfficioGui.getContentPane().add(textSalario);
        textSalario.setColumns(10);
        
        JLabel lblUfficio = new JLabel("Ufficio:");
        springLayout.putConstraint(SpringLayout.NORTH, lblUfficio, 20, SpringLayout.SOUTH, lblSalario);
        springLayout.putConstraint(SpringLayout.WEST, lblUfficio, 0, SpringLayout.WEST, lblSubTitle);
        lblUfficio.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmDirigenteUfficioGui.getContentPane().add(lblUfficio);
        
        JRadioButton rdbtnUffAcquisti = new JRadioButton("Acquisti");
        rdbtnUffAcquisti.setActionCommand("Acquisti");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnUffAcquisti, -1, SpringLayout.NORTH, lblUfficio);
        springLayout.putConstraint(SpringLayout.WEST, rdbtnUffAcquisti, 0, SpringLayout.WEST, textNome);
        frmDirigenteUfficioGui.getContentPane().add(rdbtnUffAcquisti);
        
        JRadioButton rdbtnUffVendite = new JRadioButton("Vendite");
        rdbtnUffVendite.setActionCommand("Vendite");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnUffVendite, -1, SpringLayout.NORTH, lblUfficio);
        springLayout.putConstraint(SpringLayout.WEST, rdbtnUffVendite, 6, SpringLayout.EAST, rdbtnUffAcquisti);
        frmDirigenteUfficioGui.getContentPane().add(rdbtnUffVendite);
        
        JRadioButton rdbtnUffContabilit� = new JRadioButton("Contabilit\u00E0");
        rdbtnUffContabilit�.setActionCommand("Contabilit�");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnUffContabilit�, -1, SpringLayout.NORTH, lblUfficio);
        springLayout.putConstraint(SpringLayout.WEST, rdbtnUffContabilit�, 6, SpringLayout.EAST, rdbtnUffVendite);
        frmDirigenteUfficioGui.getContentPane().add(rdbtnUffContabilit�);
        
        ButtonGroup group = new ButtonGroup();
        group.add(rdbtnUffAcquisti);
        group.add(rdbtnUffVendite);
        group.add(rdbtnUffContabilit�);
        
        JLabel lblCodUff = new JLabel("Codice Ufficio:");
        springLayout.putConstraint(SpringLayout.NORTH, lblCodUff, 0, SpringLayout.NORTH, lblUfficio);
        springLayout.putConstraint(SpringLayout.WEST, lblCodUff, 25, SpringLayout.EAST, rdbtnUffContabilit�);
        lblCodUff.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmDirigenteUfficioGui.getContentPane().add(lblCodUff);
        
        textCodUff = new JTextField();
        fieldsList.add(textCodUff);
        springLayout.putConstraint(SpringLayout.NORTH, textCodUff, 0, SpringLayout.NORTH, lblUfficio);
        springLayout.putConstraint(SpringLayout.WEST, textCodUff, 16, SpringLayout.EAST, lblCodUff);
        frmDirigenteUfficioGui.getContentPane().add(textCodUff);
        textCodUff.setColumns(10);
        
        JButton btnInserisci = new JButton("INSERISCI");
        btnInserisci.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (JTextField jTextField : fieldsList) {
                    if(jTextField.getText().length() == 0) {
                        stringsfieldList.add(null);
                    }else {
                        stringsfieldList.add(jTextField.getText());
                    }
                }
                stringsfieldList.add(group.getSelection().getActionCommand());
                dUff = new DirUff(stringsfieldList);
                try {
                    boolean check = opDirUff.insertDirUff(dUff);
                    if(!check) {
                        stringsfieldList.clear();
                    }else {
                        ResultSet rS = opDirUff.getRS_DirUff();
                        TableDirUff.setModel(DbUtils.resultSetToTableModel(rS));
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                stringsfieldList.clear();
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnInserisci, 23, SpringLayout.SOUTH, rdbtnUffAcquisti);
        springLayout.putConstraint(SpringLayout.WEST, btnInserisci, 0, SpringLayout.WEST, lblSubTitle);
        frmDirigenteUfficioGui.getContentPane().add(btnInserisci);
        
        JButton btnReset = new JButton("RESET");
        btnReset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (JTextField jTextField : fieldsList) {
                    jTextField.setText("");
                }
                stringsfieldList.clear();
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnReset, 0, SpringLayout.NORTH, btnInserisci);
        springLayout.putConstraint(SpringLayout.WEST, btnReset, 0, SpringLayout.WEST, textCF);
        frmDirigenteUfficioGui.getContentPane().add(btnReset);
        
        JLabel lblDirigenteUfficio = new JLabel("Dirigente Ufficio:");
        springLayout.putConstraint(SpringLayout.NORTH, lblDirigenteUfficio, 6, SpringLayout.NORTH, lblSubTitle);
        springLayout.putConstraint(SpringLayout.WEST, lblDirigenteUfficio, 299, SpringLayout.EAST, lblSubTitle);
        lblDirigenteUfficio.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmDirigenteUfficioGui.getContentPane().add(lblDirigenteUfficio);
        
        TableDirUff = new JTable() {
            public boolean isCellEditable(int row,int column){  
                return false; 
              }
        };
        JScrollPane scrollDirUff = new JScrollPane(TableDirUff);
        springLayout.putConstraint(SpringLayout.NORTH, scrollDirUff, 6, SpringLayout.SOUTH, lblDirigenteUfficio);
        springLayout.putConstraint(SpringLayout.WEST, scrollDirUff, 0, SpringLayout.WEST, lblDirigenteUfficio);
        springLayout.putConstraint(SpringLayout.SOUTH, scrollDirUff, -444, SpringLayout.SOUTH, frmDirigenteUfficioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, scrollDirUff, -146, SpringLayout.EAST, frmDirigenteUfficioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, TableDirUff, 6, SpringLayout.SOUTH, lblDirigenteUfficio);
        springLayout.putConstraint(SpringLayout.WEST, TableDirUff, 0, SpringLayout.WEST, lblDirigenteUfficio);
        springLayout.putConstraint(SpringLayout.SOUTH, TableDirUff, 210, SpringLayout.SOUTH, lblDirigenteUfficio);
        springLayout.putConstraint(SpringLayout.EAST, TableDirUff, -145, SpringLayout.EAST, frmDirigenteUfficioGui.getContentPane());
        frmDirigenteUfficioGui.getContentPane().add(scrollDirUff);
        
        JButton btnVisualizzaDirUff = new JButton("VISUALIZZA");
        springLayout.putConstraint(SpringLayout.NORTH, btnVisualizzaDirUff, 6, SpringLayout.SOUTH, scrollDirUff);
        springLayout.putConstraint(SpringLayout.WEST, btnVisualizzaDirUff, 0, SpringLayout.WEST, lblDirigenteUfficio);
        btnVisualizzaDirUff.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = opDirUff.getRS_DirUff();
                TableDirUff.setModel(DbUtils.resultSetToTableModel(rS));
                
            }
        });
        frmDirigenteUfficioGui.getContentPane().add(btnVisualizzaDirUff);
        
        JLabel lblRapportoAmministrazione = new JLabel("Rapporto Amministrazione:");
        springLayout.putConstraint(SpringLayout.NORTH, lblRapportoAmministrazione, 0, SpringLayout.NORTH, lblVia);
        springLayout.putConstraint(SpringLayout.WEST, lblRapportoAmministrazione, 0, SpringLayout.WEST, lblDirigenteUfficio);
        lblRapportoAmministrazione.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmDirigenteUfficioGui.getContentPane().add(lblRapportoAmministrazione);
        
        TableRappAmm = new JTable() {
            public boolean isCellEditable(int row,int column){  
                return false; 
              }
        };
        JScrollPane scrollRappAmm = new JScrollPane(TableRappAmm);
        springLayout.putConstraint(SpringLayout.NORTH, scrollRappAmm, 6, SpringLayout.SOUTH, lblRapportoAmministrazione);
        springLayout.putConstraint(SpringLayout.WEST, scrollRappAmm, 0, SpringLayout.WEST, lblDirigenteUfficio);
        springLayout.putConstraint(SpringLayout.SOUTH, scrollRappAmm, -220, SpringLayout.SOUTH, frmDirigenteUfficioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, scrollRappAmm, -146, SpringLayout.EAST, frmDirigenteUfficioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, TableRappAmm, 6, SpringLayout.SOUTH, lblRapportoAmministrazione);
        springLayout.putConstraint(SpringLayout.WEST, TableRappAmm, 76, SpringLayout.EAST, textCodUff);
        springLayout.putConstraint(SpringLayout.SOUTH, TableRappAmm, 183, SpringLayout.SOUTH, lblRapportoAmministrazione);
        springLayout.putConstraint(SpringLayout.EAST, TableRappAmm, 265, SpringLayout.EAST, textCodUff);
        frmDirigenteUfficioGui.getContentPane().add(scrollRappAmm);
        
        JButton btnVisRapAmm = new JButton("VISUALIZZA");
        springLayout.putConstraint(SpringLayout.NORTH, btnVisRapAmm, 6, SpringLayout.SOUTH, scrollRappAmm);
        springLayout.putConstraint(SpringLayout.WEST, btnVisRapAmm, 0, SpringLayout.WEST, lblDirigenteUfficio);
        btnVisRapAmm.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                OperationRapAm opRapAmm;                
                try {
                    opRapAmm = new OperationRapAm();
                    ResultSet rS = opRapAmm.getRS_Rap();
                    TableRappAmm.setModel(DbUtils.resultSetToTableModel(rS));
                } catch (ClassNotFoundException | SQLException e1) {
                    e1.printStackTrace();
                }
                
            }
        });
        frmDirigenteUfficioGui.getContentPane().add(btnVisRapAmm);
        
        JLabel lblDelDirUff = new JLabel("Cancella Dirigente Ufficio:");
        lblDelDirUff.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblDelDirUff, 22, SpringLayout.SOUTH, btnInserisci);
        springLayout.putConstraint(SpringLayout.WEST, lblDelDirUff, 0, SpringLayout.WEST, lblSubTitle);
        frmDirigenteUfficioGui.getContentPane().add(lblDelDirUff);
        
        JLabel lblDelCodLav = new JLabel("Codice Lavoratore:");
        springLayout.putConstraint(SpringLayout.NORTH, lblDelCodLav, 9, SpringLayout.SOUTH, lblDelDirUff);
        springLayout.putConstraint(SpringLayout.WEST, lblDelCodLav, 0, SpringLayout.WEST, lblSubTitle);
        frmDirigenteUfficioGui.getContentPane().add(lblDelCodLav);
        
        textDelCodLav = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textDelCodLav, 6, SpringLayout.SOUTH, lblDelDirUff);
        springLayout.putConstraint(SpringLayout.WEST, textDelCodLav, 0, SpringLayout.WEST, textCF);
        springLayout.putConstraint(SpringLayout.EAST, textDelCodLav, 0, SpringLayout.EAST, textNome);
        frmDirigenteUfficioGui.getContentPane().add(textDelCodLav);
        textDelCodLav.setColumns(10);
        
        JButton btnDelDirUff = new JButton("ELIMINA");
        btnDelDirUff.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    opDirUff.deleteDirUff(textDelCodLav.getText());
                    ResultSet rS = opDirUff.getRS_DirUff();
                    TableDirUff.setModel(DbUtils.resultSetToTableModel(rS));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnDelDirUff, 0, SpringLayout.NORTH, textDelCodLav);
        springLayout.putConstraint(SpringLayout.EAST, btnDelDirUff, 0, SpringLayout.EAST, lblCognome);
        frmDirigenteUfficioGui.getContentPane().add(btnDelDirUff);
        
        JLabel lblVisDirUff = new JLabel("Visualizza Direttore Ufficio:");
        lblVisDirUff.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.WEST, lblVisDirUff, 0, SpringLayout.WEST, lblSubTitle);
        frmDirigenteUfficioGui.getContentPane().add(lblVisDirUff);
        
        JLabel lblVisCodLav = new JLabel("Codice Lavoratore:");
        springLayout.putConstraint(SpringLayout.NORTH, lblVisCodLav, 9, SpringLayout.SOUTH, lblVisDirUff);
        springLayout.putConstraint(SpringLayout.WEST, lblVisCodLav, 0, SpringLayout.WEST, lblSubTitle);
        frmDirigenteUfficioGui.getContentPane().add(lblVisCodLav);
        
        textVisCodLav = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textVisCodLav, 587, SpringLayout.NORTH, frmDirigenteUfficioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, lblVisDirUff, -6, SpringLayout.NORTH, textVisCodLav);
        springLayout.putConstraint(SpringLayout.WEST, textVisCodLav, 0, SpringLayout.WEST, textCF);
        springLayout.putConstraint(SpringLayout.EAST, textVisCodLav, 0, SpringLayout.EAST, textNome);
        frmDirigenteUfficioGui.getContentPane().add(textVisCodLav);
        textVisCodLav.setColumns(10);
        
        JButton btnVisCodLav = new JButton("VISUALIZZA");
        btnVisCodLav.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = opDirUff.getSingDir(textVisCodLav.getText());
                TableDirUff.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.WEST, btnVisCodLav, 0, SpringLayout.WEST, lblCivico);
        springLayout.putConstraint(SpringLayout.SOUTH, btnVisCodLav, 0, SpringLayout.SOUTH, textVisCodLav);
        frmDirigenteUfficioGui.getContentPane().add(btnVisCodLav);
        
        JLabel lblNewLabel = new JLabel("Inserisci Rapporto Amministrazione");
        springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel, 23, SpringLayout.SOUTH, btnVisRapAmm);
        springLayout.putConstraint(SpringLayout.WEST, lblNewLabel, 0, SpringLayout.WEST, lblDirigenteUfficio);
        lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmDirigenteUfficioGui.getContentPane().add(lblNewLabel);
        
        JLabel lblDataAmm = new JLabel("Data:");
        springLayout.putConstraint(SpringLayout.WEST, lblDataAmm, 0, SpringLayout.WEST, lblDirigenteUfficio);
        lblDataAmm.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmDirigenteUfficioGui.getContentPane().add(lblDataAmm);
        
        JLabel lblDescrAmm = new JLabel("Descrizione:");
        springLayout.putConstraint(SpringLayout.NORTH, lblDescrAmm, 1, SpringLayout.NORTH, btnVisCodLav);
        springLayout.putConstraint(SpringLayout.WEST, lblDescrAmm, 0, SpringLayout.WEST, lblDirigenteUfficio);
        lblDescrAmm.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmDirigenteUfficioGui.getContentPane().add(lblDescrAmm);
        
        JLabel lblCodDirAmm = new JLabel("Codice Dirigente:");
        springLayout.putConstraint(SpringLayout.NORTH, lblCodDirAmm, 88, SpringLayout.SOUTH, scrollRappAmm);
        springLayout.putConstraint(SpringLayout.EAST, lblCodDirAmm, -283, SpringLayout.EAST, frmDirigenteUfficioGui.getContentPane());
        lblCodDirAmm.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmDirigenteUfficioGui.getContentPane().add(lblCodDirAmm);
        
        JLabel lblCodSocioAmm = new JLabel("Codice Socio:");
        springLayout.putConstraint(SpringLayout.NORTH, lblCodSocioAmm, 1, SpringLayout.NORTH, btnVisCodLav);
        springLayout.putConstraint(SpringLayout.EAST, lblCodSocioAmm, 0, SpringLayout.EAST, lblCodDirAmm);
        lblCodSocioAmm.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmDirigenteUfficioGui.getContentPane().add(lblCodSocioAmm);
        
        textDataAmm = new JTextField();
        fieldsList_2.add(textDataAmm);
        springLayout.putConstraint(SpringLayout.NORTH, textDataAmm, 0, SpringLayout.NORTH, lblDataAmm);
        springLayout.putConstraint(SpringLayout.WEST, textDataAmm, 6, SpringLayout.EAST, lblDataAmm);
        springLayout.putConstraint(SpringLayout.EAST, textDataAmm, -75, SpringLayout.WEST, lblCodDirAmm);
        frmDirigenteUfficioGui.getContentPane().add(textDataAmm);
        textDataAmm.setColumns(10);
        
        textDescrAmm = new JTextField();
        fieldsList_2.add(textDescrAmm);
        springLayout.putConstraint(SpringLayout.WEST, textDescrAmm, 6, SpringLayout.EAST, lblDescrAmm);
        springLayout.putConstraint(SpringLayout.SOUTH, textDescrAmm, 0, SpringLayout.SOUTH, lblVisCodLav);
        springLayout.putConstraint(SpringLayout.EAST, textDescrAmm, -405, SpringLayout.EAST, frmDirigenteUfficioGui.getContentPane());
        frmDirigenteUfficioGui.getContentPane().add(textDescrAmm);
        textDescrAmm.setColumns(10);
        
        textCodDirAmm = new JTextField();
        fieldsList_2.add(textCodDirAmm);
        springLayout.putConstraint(SpringLayout.NORTH, textCodDirAmm, 88, SpringLayout.SOUTH, scrollRappAmm);
        springLayout.putConstraint(SpringLayout.SOUTH, lblDataAmm, 0, SpringLayout.SOUTH, textCodDirAmm);
        springLayout.putConstraint(SpringLayout.WEST, textCodDirAmm, 6, SpringLayout.EAST, lblCodDirAmm);
        springLayout.putConstraint(SpringLayout.EAST, textCodDirAmm, -156, SpringLayout.EAST, frmDirigenteUfficioGui.getContentPane());
        frmDirigenteUfficioGui.getContentPane().add(textCodDirAmm);
        textCodDirAmm.setColumns(10);
        
        textCodSocioAmm = new JTextField();
        springLayout.putConstraint(SpringLayout.WEST, textCodSocioAmm, 6, SpringLayout.EAST, lblCodSocioAmm);
        springLayout.putConstraint(SpringLayout.EAST, textCodSocioAmm, -146, SpringLayout.EAST, frmDirigenteUfficioGui.getContentPane());
        fieldsList_2.add(textCodSocioAmm);
        springLayout.putConstraint(SpringLayout.NORTH, textCodSocioAmm, 0, SpringLayout.NORTH, btnVisCodLav);
        frmDirigenteUfficioGui.getContentPane().add(textCodSocioAmm);
        textCodSocioAmm.setColumns(10);
        
        JButton btnInsertRappAmm = new JButton("INSERISCI");
        btnInsertRappAmm.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (JTextField jTextField : fieldsList_2) {
                    stringsfieldList_2.add(jTextField.getText());
                }
                RapAm rA = new RapAm(stringsfieldList_2);
                try {
                    boolean check = opRapAm.insertRap(rA);
                    if(!check) {
                        stringsfieldList_2.clear();
                    }else {
                        ResultSet rS = opRapAm.getRS_Rap();
                        TableRappAmm.setModel(DbUtils.resultSetToTableModel(rS));
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                stringsfieldList_2.clear();
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnInsertRappAmm, 12, SpringLayout.SOUTH, lblDescrAmm);
        springLayout.putConstraint(SpringLayout.WEST, btnInsertRappAmm, 0, SpringLayout.WEST, lblDirigenteUfficio);
        frmDirigenteUfficioGui.getContentPane().add(btnInsertRappAmm);
    }
    
    public void run() {
        try {
            DirigenteUfficioGUI window = new DirigenteUfficioGUI();
            window.frmDirigenteUfficioGui.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
