package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SpringLayout;


import Pagamento.OperationPagamento;
import Pagamento.Pagamento;
import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import javax.swing.JScrollPane;

import java.awt.Font;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PagamentoGUI {

    private JFrame frmPagamentiGui;
    private JTextField textNumPagamento;
    private JTextField textNumfattura;
    private JTextField textDate;
    private JTable tablePagamento;
    
    private OperationPagamento oPag = new OperationPagamento();
    private List<JTextField> textFieldsList = new ArrayList<JTextField>();
    private List<String> stringsFieldsList = new ArrayList<String>();
    
    

   
    /**
     * Create the application.
     */
    public PagamentoGUI() throws SQLException, ClassNotFoundException{
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    @SuppressWarnings("unchecked")
    private void initialize() {
        frmPagamentiGui = new JFrame();
        frmPagamentiGui.setTitle("Pagamenti GUI");
        frmPagamentiGui.setResizable(false);
        frmPagamentiGui.setBounds(100, 100, 1075, 425);
        frmPagamentiGui.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        SpringLayout springLayout = new SpringLayout();
        frmPagamentiGui.getContentPane().setLayout(springLayout);
        
        JLabel lblTitle = new JLabel("PAGAMENTI");
        springLayout.putConstraint(SpringLayout.NORTH, lblTitle, 10, SpringLayout.NORTH, frmPagamentiGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblTitle, 401, SpringLayout.WEST, frmPagamentiGui.getContentPane());
        lblTitle.setFont(new Font("Tahoma", Font.BOLD, 40));
        frmPagamentiGui.getContentPane().add(lblTitle);
        
        JLabel lblSubTitle = new JLabel("Inserisci Pagamento:");
        lblSubTitle.setFont(new Font("Tahoma", Font.BOLD, 20));
        springLayout.putConstraint(SpringLayout.NORTH, lblSubTitle, 78, SpringLayout.NORTH, frmPagamentiGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblSubTitle, 10, SpringLayout.WEST, frmPagamentiGui.getContentPane());
        frmPagamentiGui.getContentPane().add(lblSubTitle);
        
        JLabel lblNumPagamento = new JLabel("Numero:");
        lblNumPagamento.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblNumPagamento, 16, SpringLayout.SOUTH, lblSubTitle);
        springLayout.putConstraint(SpringLayout.WEST, lblNumPagamento, 10, SpringLayout.WEST, frmPagamentiGui.getContentPane());
        frmPagamentiGui.getContentPane().add(lblNumPagamento);
        
        //
        textNumPagamento = new JTextField();
        textNumPagamento.setEnabled(false);
        textNumPagamento.setText(oPag.getRS_NewNumPag());
        springLayout.putConstraint(SpringLayout.NORTH, textNumPagamento, 0, SpringLayout.NORTH, lblNumPagamento);
        springLayout.putConstraint(SpringLayout.WEST, textNumPagamento, 20, SpringLayout.EAST, lblNumPagamento);
        frmPagamentiGui.getContentPane().add(textNumPagamento);
        textNumPagamento.setColumns(10);
        
        JLabel lblNumFattura = new JLabel("Numero Fattura:");
        lblNumFattura.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblNumFattura, 24, SpringLayout.SOUTH, lblNumPagamento);
        springLayout.putConstraint(SpringLayout.WEST, lblNumFattura, 0, SpringLayout.WEST, lblSubTitle);
        frmPagamentiGui.getContentPane().add(lblNumFattura);
        
        textNumfattura = new JTextField();
        textFieldsList.add(textNumfattura);
        springLayout.putConstraint(SpringLayout.WEST, textNumfattura, 17, SpringLayout.EAST, lblNumFattura);
        springLayout.putConstraint(SpringLayout.SOUTH, textNumfattura, 0, SpringLayout.SOUTH, lblNumFattura);
        frmPagamentiGui.getContentPane().add(textNumfattura);
        textNumfattura.setColumns(10);
        
        JLabel lblData = new JLabel("Data (AAAA-MM-GG):");
        lblData.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblData, 24, SpringLayout.SOUTH, lblNumFattura);
        springLayout.putConstraint(SpringLayout.WEST, lblData, 0, SpringLayout.WEST, lblSubTitle);
        frmPagamentiGui.getContentPane().add(lblData);
        
        textDate = new JTextField();
        textFieldsList.add(textDate);
        springLayout.putConstraint(SpringLayout.WEST, textDate, 12, SpringLayout.EAST, lblData);
        springLayout.putConstraint(SpringLayout.EAST, textDate, 153, SpringLayout.EAST, lblData);
        frmPagamentiGui.getContentPane().add(textDate);
        textDate.setColumns(10);
        
        JLabel lblTipologia = new JLabel("Tipologia:");
        lblTipologia.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblTipologia, 25, SpringLayout.SOUTH, lblData);
        springLayout.putConstraint(SpringLayout.WEST, lblTipologia, 0, SpringLayout.WEST, lblSubTitle);
        frmPagamentiGui.getContentPane().add(lblTipologia);
        
        JComboBox comboBox = new JComboBox();
        comboBox.addItem("Bonifico Bancario");
        comboBox.addItem("Assegno");
        comboBox.addItem("PayPal");
        comboBox.addItem("Contrassegno");
        comboBox.addItem("Contanti");
        comboBox.addItem("Carta di Credito");
        
        springLayout.putConstraint(SpringLayout.SOUTH, textDate, -21, SpringLayout.NORTH, comboBox);
        springLayout.putConstraint(SpringLayout.NORTH, comboBox, -1, SpringLayout.NORTH, lblTipologia);
        springLayout.putConstraint(SpringLayout.WEST, comboBox, 6, SpringLayout.EAST, lblTipologia);
        springLayout.putConstraint(SpringLayout.EAST, comboBox, 6, SpringLayout.EAST, lblSubTitle);
        frmPagamentiGui.getContentPane().add(comboBox);
        
        JButton btnInserisci = new JButton("INSERISCI");
        btnInserisci.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (JTextField textField : textFieldsList) {
                    stringsFieldsList.add(textField.getText());
                }
                stringsFieldsList.add(comboBox.getSelectedItem().toString());
                Pagamento pag = new Pagamento(stringsFieldsList);
                try {
                    boolean check = oPag.insertPag(pag);
                    if(!check) {
                        stringsFieldsList.clear();
                    }else {
                        ResultSet rS = oPag.getRS_Pagamento();
                        tablePagamento.setModel(DbUtils.resultSetToTableModel(rS));
                        textNumPagamento.setText(oPag.getRS_NewNumPag());
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                stringsFieldsList.clear();
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnInserisci, 15, SpringLayout.SOUTH, comboBox);
        springLayout.putConstraint(SpringLayout.WEST, btnInserisci, 0, SpringLayout.WEST, lblSubTitle);
        frmPagamentiGui.getContentPane().add(btnInserisci);
        
        JButton btnReset = new JButton("RESET");
        btnReset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (JTextField textField : textFieldsList) {
                    textField.setText("");
                }
                stringsFieldsList.clear();
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnReset, 0, SpringLayout.NORTH, btnInserisci);
        springLayout.putConstraint(SpringLayout.WEST, btnReset, 38, SpringLayout.EAST, btnInserisci);
        frmPagamentiGui.getContentPane().add(btnReset);
        
        JLabel lblPagamento = new JLabel("Pagamento:");
        springLayout.putConstraint(SpringLayout.WEST, lblPagamento, 251, SpringLayout.EAST, lblSubTitle);
        lblPagamento.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblPagamento, 9, SpringLayout.NORTH, lblSubTitle);
        frmPagamentiGui.getContentPane().add(lblPagamento);
        
        tablePagamento = new JTable() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        JScrollPane scrollPag = new JScrollPane(tablePagamento);
        springLayout.putConstraint(SpringLayout.NORTH, scrollPag, 6, SpringLayout.SOUTH, lblPagamento);
        springLayout.putConstraint(SpringLayout.WEST, scrollPag, 163, SpringLayout.EAST, textDate);
        springLayout.putConstraint(SpringLayout.EAST, scrollPag, -153, SpringLayout.EAST, frmPagamentiGui.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, tablePagamento, 15, SpringLayout.SOUTH, lblPagamento);
        springLayout.putConstraint(SpringLayout.WEST, tablePagamento, 245, SpringLayout.EAST, comboBox);
        springLayout.putConstraint(SpringLayout.SOUTH, tablePagamento, 201, SpringLayout.SOUTH, lblPagamento);
        springLayout.putConstraint(SpringLayout.EAST, tablePagamento, -131, SpringLayout.EAST, frmPagamentiGui.getContentPane());
        frmPagamentiGui.getContentPane().add(scrollPag);
        
        JButton btnVisPagamento = new JButton("VISUALIZZA");
        springLayout.putConstraint(SpringLayout.WEST, btnVisPagamento, 475, SpringLayout.WEST, frmPagamentiGui.getContentPane());
        btnVisPagamento.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = oPag.getRS_Pagamento();
                tablePagamento.setModel(DbUtils.resultSetToTableModel(rS));
                
            }
        });
        springLayout.putConstraint(SpringLayout.SOUTH, scrollPag, -6, SpringLayout.NORTH, btnVisPagamento);
        springLayout.putConstraint(SpringLayout.NORTH, btnVisPagamento, 18, SpringLayout.SOUTH, tablePagamento);
        frmPagamentiGui.getContentPane().add(btnVisPagamento);
    }
    
    public void run() {
        try {
            PagamentoGUI window = new PagamentoGUI();
            window.frmPagamentiGui.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
