package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SpringLayout;

import Fatture.Fattura;
import Fatture.OperationFattura;
import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;

public class FattureGUI {

    private JFrame frmFattureGui;
    private JTextField textNumOrdine;
    private JTextField textData;
    private JTextField textCodUffContabilità;
    private JTable tableFatture;
    
    private OperationFattura opFatt = new OperationFattura();
    private List<JTextField> textFieldsList = new ArrayList<JTextField>();
    private List<String> stringFieldList = new ArrayList<String>();


    /**
     * Create the application.
     */
    public FattureGUI() throws SQLException, ClassNotFoundException{
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmFattureGui = new JFrame();
        frmFattureGui.setTitle("Fatture GUI");
        frmFattureGui.setResizable(false);
        frmFattureGui.setBounds(100, 100, 1235, 565);
        frmFattureGui.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        SpringLayout springLayout = new SpringLayout();
        frmFattureGui.getContentPane().setLayout(springLayout);
        
        JLabel lblTitle = new JLabel("FATTURE");
        springLayout.putConstraint(SpringLayout.NORTH, lblTitle, 10, SpringLayout.NORTH, frmFattureGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblTitle, 508, SpringLayout.WEST, frmFattureGui.getContentPane());
        lblTitle.setFont(new Font("Tahoma", Font.BOLD, 40));
        frmFattureGui.getContentPane().add(lblTitle);
        
        JLabel lblSubTitle = new JLabel("Inserisci Fattura:");
        lblSubTitle.setFont(new Font("Tahoma", Font.BOLD, 20));
        springLayout.putConstraint(SpringLayout.NORTH, lblSubTitle, 73, SpringLayout.NORTH, frmFattureGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblSubTitle, 10, SpringLayout.WEST, frmFattureGui.getContentPane());
        frmFattureGui.getContentPane().add(lblSubTitle);
        
        JLabel lblTipo = new JLabel("Tipo:");
        springLayout.putConstraint(SpringLayout.NORTH, lblTipo, 9, SpringLayout.SOUTH, lblSubTitle);
        springLayout.putConstraint(SpringLayout.WEST, lblTipo, 0, SpringLayout.WEST, lblSubTitle);
        lblTipo.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmFattureGui.getContentPane().add(lblTipo);
        
        JRadioButton rdbtnAcquisto = new JRadioButton("Acquisto");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnAcquisto, 6, SpringLayout.SOUTH, lblSubTitle);
        springLayout.putConstraint(SpringLayout.WEST, rdbtnAcquisto, 6, SpringLayout.EAST, lblTipo);
        rdbtnAcquisto.setActionCommand("Acquisto");
        frmFattureGui.getContentPane().add(rdbtnAcquisto);
        
        JRadioButton rdbtnVendita = new JRadioButton("Vendita");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnVendita, 6, SpringLayout.SOUTH, lblSubTitle);
        rdbtnVendita.setActionCommand("Vendita");
        frmFattureGui.getContentPane().add(rdbtnVendita);
        
        ButtonGroup group = new ButtonGroup();
        group.add(rdbtnAcquisto);
        group.add(rdbtnVendita);
        
        JLabel lblNumOrdine = new JLabel("Numero Ordine:");
        springLayout.putConstraint(SpringLayout.NORTH, lblNumOrdine, 16, SpringLayout.SOUTH, rdbtnAcquisto);
        springLayout.putConstraint(SpringLayout.WEST, lblNumOrdine, 0, SpringLayout.WEST, lblSubTitle);
        lblNumOrdine.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmFattureGui.getContentPane().add(lblNumOrdine);
        
        textNumOrdine = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textNumOrdine, 0, SpringLayout.NORTH, lblNumOrdine);
        springLayout.putConstraint(SpringLayout.WEST, textNumOrdine, 10, SpringLayout.EAST, lblNumOrdine);
        textFieldsList.add(textNumOrdine);
        frmFattureGui.getContentPane().add(textNumOrdine);
        textNumOrdine.setColumns(10);
        
        JLabel lblData = new JLabel("Data (AAAA-MM-GG):");
        springLayout.putConstraint(SpringLayout.NORTH, lblData, 16, SpringLayout.SOUTH, textNumOrdine);
        springLayout.putConstraint(SpringLayout.WEST, lblData, 0, SpringLayout.WEST, lblSubTitle);
        lblData.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmFattureGui.getContentPane().add(lblData);
        
        textData = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textData, 0, SpringLayout.NORTH, lblData);
        springLayout.putConstraint(SpringLayout.WEST, textData, 6, SpringLayout.EAST, lblData);
        textFieldsList.add(textData);
        frmFattureGui.getContentPane().add(textData);
        textData.setColumns(10);
        
        JLabel lblCodUffCont = new JLabel("Codice Ufficio Contabilit\u00E0:");
        springLayout.putConstraint(SpringLayout.NORTH, lblCodUffCont, 20, SpringLayout.SOUTH, textData);
        springLayout.putConstraint(SpringLayout.WEST, lblCodUffCont, 0, SpringLayout.WEST, lblSubTitle);
        lblCodUffCont.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmFattureGui.getContentPane().add(lblCodUffCont);
        
        textCodUffContabilità = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textCodUffContabilità, 0, SpringLayout.NORTH, lblCodUffCont);
        springLayout.putConstraint(SpringLayout.WEST, textCodUffContabilità, 5, SpringLayout.EAST, lblCodUffCont);
        textFieldsList.add(textCodUffContabilità);
        frmFattureGui.getContentPane().add(textCodUffContabilità);
        textCodUffContabilità.setColumns(10);
        
        JButton btnInserisci = new JButton("INSERISCI");
        springLayout.putConstraint(SpringLayout.NORTH, btnInserisci, 15, SpringLayout.SOUTH, lblCodUffCont);
        springLayout.putConstraint(SpringLayout.WEST, btnInserisci, 0, SpringLayout.WEST, lblSubTitle);
        btnInserisci.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (JTextField textField : textFieldsList) {
                    stringFieldList.add(textField.getText());
                }
                stringFieldList.add(group.getSelection().getActionCommand());
                Fattura fatt = new Fattura(stringFieldList);
                try {
                    boolean check = opFatt.insertFatt(fatt);
                    if(!check) {
                        stringFieldList.clear();
                    }else {
                        ResultSet rS = opFatt.getRS_Fatt();
                        tableFatture.setModel(DbUtils.resultSetToTableModel(rS));
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                stringFieldList.clear();
            }
        });
        frmFattureGui.getContentPane().add(btnInserisci);
        
        JButton btnReset = new JButton("RESET");
        springLayout.putConstraint(SpringLayout.EAST, rdbtnVendita, 0, SpringLayout.EAST, btnReset);
        springLayout.putConstraint(SpringLayout.NORTH, btnReset, 0, SpringLayout.NORTH, btnInserisci);
        springLayout.putConstraint(SpringLayout.WEST, btnReset, 0, SpringLayout.WEST, textNumOrdine);
        btnReset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (JTextField textField : textFieldsList) {
                    textField.setText("");
                }
                stringFieldList.clear();
            }
        });
        frmFattureGui.getContentPane().add(btnReset);
        
        JLabel lblFattSemestre = new JLabel("Visualizza Fatture semestrali:");
        springLayout.putConstraint(SpringLayout.NORTH, lblFattSemestre, 27, SpringLayout.SOUTH, btnInserisci);
        springLayout.putConstraint(SpringLayout.WEST, lblFattSemestre, 0, SpringLayout.WEST, lblSubTitle);
        lblFattSemestre.setFont(new Font("Tahoma", Font.BOLD, 20));
        frmFattureGui.getContentPane().add(lblFattSemestre);
        
        JLabel lblSemestre = new JLabel("Semestre:");
        springLayout.putConstraint(SpringLayout.NORTH, lblSemestre, 6, SpringLayout.SOUTH, lblFattSemestre);
        springLayout.putConstraint(SpringLayout.WEST, lblSemestre, 0, SpringLayout.WEST, lblSubTitle);
        lblSemestre.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmFattureGui.getContentPane().add(lblSemestre);
        
        JRadioButton rdbtnPrimoSemestre = new JRadioButton("Primo Semestre");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnPrimoSemestre, 6, SpringLayout.SOUTH, lblFattSemestre);
        springLayout.putConstraint(SpringLayout.WEST, rdbtnPrimoSemestre, 6, SpringLayout.EAST, lblSemestre);
        rdbtnPrimoSemestre.setActionCommand("1");
        frmFattureGui.getContentPane().add(rdbtnPrimoSemestre);
        
        JRadioButton rdbtnSecondoSemestre = new JRadioButton("Secondo Semestre");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnSecondoSemestre, 6, SpringLayout.SOUTH, lblFattSemestre);
        springLayout.putConstraint(SpringLayout.WEST, rdbtnSecondoSemestre, 23, SpringLayout.EAST, rdbtnPrimoSemestre);
        rdbtnSecondoSemestre.setActionCommand("2");
        frmFattureGui.getContentPane().add(rdbtnSecondoSemestre);
        
        ButtonGroup group_2 = new ButtonGroup();
        group_2.add(rdbtnPrimoSemestre);
        group_2.add(rdbtnSecondoSemestre);
        
        JButton btnVisFattSemestre = new JButton("VISUALIZZA");
        springLayout.putConstraint(SpringLayout.NORTH, btnVisFattSemestre, 7, SpringLayout.SOUTH, rdbtnPrimoSemestre);
        springLayout.putConstraint(SpringLayout.WEST, btnVisFattSemestre, 0, SpringLayout.WEST, lblSubTitle);
        btnVisFattSemestre.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = opFatt.getRS_FattSem(Integer.parseInt(group_2.getSelection().getActionCommand()));
                tableFatture.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        frmFattureGui.getContentPane().add(btnVisFattSemestre);
        
        JLabel lblVisFatture = new JLabel("Fatture:");
        lblVisFatture.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.WEST, lblVisFatture, 0, SpringLayout.WEST, lblTitle);
        springLayout.putConstraint(SpringLayout.SOUTH, lblVisFatture, 0, SpringLayout.SOUTH, lblSubTitle);
        frmFattureGui.getContentPane().add(lblVisFatture);
        
        tableFatture = new JTable() {
            public boolean isCellEditable(int row, int column) {
                return false;
                
            }
        };
        JScrollPane scrollFatture = new JScrollPane(tableFatture);
        springLayout.putConstraint(SpringLayout.WEST, scrollFatture, 0, SpringLayout.WEST, lblTitle);
        springLayout.putConstraint(SpringLayout.EAST, scrollFatture, -64, SpringLayout.EAST, frmFattureGui.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, scrollFatture, 11, SpringLayout.SOUTH, lblVisFatture);
        springLayout.putConstraint(SpringLayout.SOUTH, scrollFatture, -191, SpringLayout.SOUTH, frmFattureGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, tableFatture, 0, SpringLayout.WEST, lblTitle);
        springLayout.putConstraint(SpringLayout.SOUTH, tableFatture, 225, SpringLayout.SOUTH, lblVisFatture);
        springLayout.putConstraint(SpringLayout.EAST, tableFatture, -90, SpringLayout.EAST, frmFattureGui.getContentPane());
        frmFattureGui.getContentPane().add(scrollFatture);
        
        JButton btnVisFatture = new JButton("VISUALIZZA");
        springLayout.putConstraint(SpringLayout.NORTH, btnVisFatture, 6, SpringLayout.SOUTH, scrollFatture);
        springLayout.putConstraint(SpringLayout.WEST, btnVisFatture, 0, SpringLayout.WEST, lblTitle);
        btnVisFatture.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = opFatt.getRS_Fatt();
                tableFatture.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        frmFattureGui.getContentPane().add(btnVisFatture);
        
        JButton btnPagamento = new JButton("PAGAMENTI >>");
        springLayout.putConstraint(SpringLayout.NORTH, btnPagamento, 33, SpringLayout.SOUTH, btnVisFatture);
        springLayout.putConstraint(SpringLayout.WEST, btnPagamento, 0, SpringLayout.WEST, lblTitle);
        springLayout.putConstraint(SpringLayout.SOUTH, btnPagamento, -88, SpringLayout.SOUTH, frmFattureGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, btnPagamento, -580, SpringLayout.EAST, frmFattureGui.getContentPane());
        btnPagamento.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                PagamentoGUI pGUI;
                try {
                    pGUI = new PagamentoGUI();
                    pGUI.run();
                } catch (ClassNotFoundException | SQLException e1) {
                    e1.printStackTrace();
                }                
            }
        });
        frmFattureGui.getContentPane().add(btnPagamento);
    }
    
    public void run() {
        try {
            FattureGUI window = new FattureGUI();
            window.frmFattureGui.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
