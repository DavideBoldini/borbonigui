package GUI;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

//
public class MainMenu {
//ciao
    private JFrame frmSezioni;

    /**
     * Create the application.
     */
    public MainMenu() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmSezioni = new JFrame();
        frmSezioni.setResizable(false);
        frmSezioni.setTitle("Sezioni");
        frmSezioni.setBounds(100, 100, 450, 300);
        frmSezioni.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel lblSezioni = new JLabel("SEZIONI");
        lblSezioni.setForeground(SystemColor.textHighlight);
        lblSezioni.setFont(new Font("Tahoma", Font.BOLD, 30));
        lblSezioni.setHorizontalAlignment(SwingConstants.CENTER);

        JButton btnAmministrazione = new JButton("AMMINISTRAZIONE");
        btnAmministrazione.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AmministrazioneGUI ammGUI;
                try {
                    ammGUI = new AmministrazioneGUI();
                    ammGUI.run();
                } catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });

        JButton btnOrdini = new JButton("ORDINI");
        btnOrdini.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                OrdiniVenditaGUI OrdVGUI;
                try {
                    OrdVGUI = new OrdiniVenditaGUI();
                    OrdVGUI.run();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }

            }
        });

        JButton btnBilancio = new JButton("BILANCIO");
        btnBilancio.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                BilancioGUI bGUI;
                try {
                    bGUI = new BilancioGUI();
                    bGUI.run();
                } catch (ClassNotFoundException | SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });

        // BTN MAGAZZINO
        JButton btnMagazzino = new JButton("MAGAZZINO");
        btnMagazzino.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LavMagGUI lvGUI;
                try {
                    lvGUI = new LavMagGUI();
                    lvGUI.run();
                    frmSezioni.setVisible(true);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        });

        JButton btnPezzi = new JButton("PEZZI");
        btnPezzi.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                PezzoGUI pGUI;
                try {
                    pGUI = new PezzoGUI();
                    pGUI.run();
                } catch (ClassNotFoundException | SQLException e1) {
                    e1.printStackTrace();
                }

            }
        });

        JButton btnProdotti = new JButton("PRODOTTI");
        btnProdotti.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ProdottoGUI pGUI;
                try {
                    pGUI = new ProdottoGUI();
                    pGUI.run();
                } catch (ClassNotFoundException | SQLException e1) {
                    e1.printStackTrace();
                }               
            }
        });

        JLabel lblAmm = new JLabel("Amministrazione");
        lblAmm.setFont(new Font("Tahoma", Font.BOLD, 15));

        JLabel lblStoccaggio = new JLabel("Magazzino");
        lblStoccaggio.setFont(new Font("Tahoma", Font.BOLD, 15));
        
        JButton btnFatture = new JButton("FATTURE");
        btnFatture.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                FattureGUI fGUI;
                try {
                    fGUI = new FattureGUI();
                    fGUI.run();
                } catch (ClassNotFoundException | SQLException e1) {
                    e1.printStackTrace();
                }               
            }
        });
        GroupLayout groupLayout = new GroupLayout(frmSezioni.getContentPane());
        groupLayout.setHorizontalGroup(
            groupLayout.createParallelGroup(Alignment.TRAILING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addGap(36)
                    .addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
                        .addGroup(groupLayout.createSequentialGroup()
                            .addComponent(lblAmm)
                            .addPreferredGap(ComponentPlacement.RELATED, 166, Short.MAX_VALUE)
                            .addComponent(lblStoccaggio))
                        .addGroup(groupLayout.createSequentialGroup()
                            .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                                .addComponent(btnAmministrazione)
                                .addComponent(btnFatture)
                                .addComponent(btnOrdini))
                            .addPreferredGap(ComponentPlacement.RELATED, 150, Short.MAX_VALUE)
                            .addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
                                .addComponent(btnProdotti)
                                .addComponent(btnMagazzino)
                                .addComponent(btnPezzi))))
                    .addGap(38))
                .addGroup(groupLayout.createSequentialGroup()
                    .addContainerGap(148, Short.MAX_VALUE)
                    .addComponent(lblSezioni, GroupLayout.PREFERRED_SIZE, 161, GroupLayout.PREFERRED_SIZE)
                    .addGap(135))
                .addGroup(groupLayout.createSequentialGroup()
                    .addContainerGap(188, Short.MAX_VALUE)
                    .addComponent(btnBilancio)
                    .addGap(175))
        );
        groupLayout.setVerticalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addGap(16)
                    .addComponent(lblSezioni)
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(lblAmm)
                        .addComponent(lblStoccaggio))
                    .addPreferredGap(ComponentPlacement.UNRELATED)
                    .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(btnMagazzino)
                        .addComponent(btnFatture))
                    .addPreferredGap(ComponentPlacement.UNRELATED)
                    .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(btnProdotti)
                        .addComponent(btnAmministrazione))
                    .addPreferredGap(ComponentPlacement.UNRELATED)
                    .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(btnOrdini)
                        .addComponent(btnPezzi))
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addComponent(btnBilancio)
                    .addContainerGap(62, Short.MAX_VALUE))
        );
        frmSezioni.getContentPane().setLayout(groupLayout);
    }

    public void run() {
        try {
            MainMenu window = new MainMenu();
            window.frmSezioni.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
