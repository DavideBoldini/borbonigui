package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SpringLayout;

import Rapporto_Amministrazione.OperationRapAm;
import Socio.OperationSocio;
import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import javax.swing.JScrollPane;

import java.awt.Font;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

public class SocioGUI {

    private JFrame frmSocioGui;
    private JTable tableSocio;
    private JTable tableDivUtile;
    private OperationSocio opSocio = new OperationSocio();
    private OperationRapAm opAmm = new OperationRapAm();
    private JTextField textRappAmmSocio;

    
    /**
     * Create the application.
     */
    public SocioGUI() throws SQLException, ClassNotFoundException{
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmSocioGui = new JFrame();
        frmSocioGui.setTitle("Socio GUI");
        frmSocioGui.setAlwaysOnTop(true);
        frmSocioGui.setResizable(false);
        frmSocioGui.setBounds(100, 100, 975, 590);
        frmSocioGui.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        SpringLayout springLayout = new SpringLayout();
        frmSocioGui.getContentPane().setLayout(springLayout);
        
        JLabel lblSocio = new JLabel("Socio:");
        springLayout.putConstraint(SpringLayout.WEST, lblSocio, 33, SpringLayout.WEST, frmSocioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, lblSocio, -464, SpringLayout.SOUTH, frmSocioGui.getContentPane());
        lblSocio.setFont(new Font("Tahoma", Font.BOLD, 40));
        frmSocioGui.getContentPane().add(lblSocio);
        
        tableSocio = new JTable() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        JScrollPane scrollSocio = new JScrollPane(tableSocio);
        springLayout.putConstraint(SpringLayout.NORTH, scrollSocio, 0, SpringLayout.NORTH, lblSocio);
        springLayout.putConstraint(SpringLayout.WEST, scrollSocio, 55, SpringLayout.EAST, lblSocio);
        springLayout.putConstraint(SpringLayout.SOUTH, scrollSocio, -353, SpringLayout.SOUTH, frmSocioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, scrollSocio, -95, SpringLayout.EAST, frmSocioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, tableSocio, 48, SpringLayout.NORTH, frmSocioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, tableSocio, 27, SpringLayout.EAST, lblSocio);
        springLayout.putConstraint(SpringLayout.SOUTH, tableSocio, -326, SpringLayout.SOUTH, frmSocioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, tableSocio, -109, SpringLayout.EAST, frmSocioGui.getContentPane());
        frmSocioGui.getContentPane().add(scrollSocio);
        
        JButton btnVisSocio = new JButton("VISUALIZZA");
        btnVisSocio.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = opSocio.getRS_Socio();
                tableSocio.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnVisSocio, 6, SpringLayout.SOUTH, lblSocio);
        springLayout.putConstraint(SpringLayout.WEST, btnVisSocio, 0, SpringLayout.WEST, lblSocio);
        springLayout.putConstraint(SpringLayout.SOUTH, btnVisSocio, -428, SpringLayout.SOUTH, frmSocioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, btnVisSocio, -813, SpringLayout.EAST, frmSocioGui.getContentPane());
        frmSocioGui.getContentPane().add(btnVisSocio);
        
        JLabel lblDivisioneUtili = new JLabel("Divisione Utili:");
        springLayout.putConstraint(SpringLayout.NORTH, lblDivisioneUtili, 17, SpringLayout.SOUTH, tableSocio);
        springLayout.putConstraint(SpringLayout.WEST, lblDivisioneUtili, 0, SpringLayout.WEST, lblSocio);
        lblDivisioneUtili.setFont(new Font("Tahoma", Font.BOLD, 40));
        frmSocioGui.getContentPane().add(lblDivisioneUtili);
        
        tableDivUtile = new JTable() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        JScrollPane scrollDivUtile = new JScrollPane(tableDivUtile);
        springLayout.putConstraint(SpringLayout.SOUTH, scrollDivUtile, 309, SpringLayout.NORTH, btnVisSocio);
        springLayout.putConstraint(SpringLayout.NORTH, scrollDivUtile, 0, SpringLayout.NORTH, lblDivisioneUtili);
        springLayout.putConstraint(SpringLayout.WEST, scrollDivUtile, 20, SpringLayout.EAST, lblDivisioneUtili);
        springLayout.putConstraint(SpringLayout.EAST, scrollDivUtile, 546, SpringLayout.EAST, lblDivisioneUtili);
        springLayout.putConstraint(SpringLayout.NORTH, tableDivUtile, 6, SpringLayout.SOUTH, lblDivisioneUtili);
        springLayout.putConstraint(SpringLayout.WEST, tableDivUtile, 0, SpringLayout.WEST, tableSocio);
        springLayout.putConstraint(SpringLayout.SOUTH, tableDivUtile, -67, SpringLayout.SOUTH, frmSocioGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, tableDivUtile, 0, SpringLayout.EAST, tableSocio);
        frmSocioGui.getContentPane().add(scrollDivUtile);
        
        JButton btnVisDivUtili = new JButton("VISUALIZZA");
        btnVisDivUtili.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = null;
                try {
                    rS = opSocio.getRS_UtiliSoci();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                tableDivUtile.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnVisDivUtili, 8, SpringLayout.SOUTH, lblDivisioneUtili);
        springLayout.putConstraint(SpringLayout.WEST, btnVisDivUtili, 0, SpringLayout.WEST, lblSocio);
        springLayout.putConstraint(SpringLayout.SOUTH, btnVisDivUtili, 38, SpringLayout.SOUTH, lblDivisioneUtili);
        springLayout.putConstraint(SpringLayout.EAST, btnVisDivUtili, 0, SpringLayout.EAST, lblSocio);
        frmSocioGui.getContentPane().add(btnVisDivUtili);
        
        JLabel lblRappAmmSocio = new JLabel("Visualizza Rapporto Amministrazione:");
        springLayout.putConstraint(SpringLayout.NORTH, lblRappAmmSocio, 35, SpringLayout.SOUTH, scrollDivUtile);
        springLayout.putConstraint(SpringLayout.WEST, lblRappAmmSocio, 0, SpringLayout.WEST, lblSocio);
        lblRappAmmSocio.setFont(new Font("Tahoma", Font.BOLD, 20));
        frmSocioGui.getContentPane().add(lblRappAmmSocio);
        
        JLabel lblCodSocio = new JLabel("Codice Socio:");
        lblCodSocio.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblCodSocio, 19, SpringLayout.SOUTH, lblRappAmmSocio);
        springLayout.putConstraint(SpringLayout.WEST, lblCodSocio, 0, SpringLayout.WEST, lblSocio);
        frmSocioGui.getContentPane().add(lblCodSocio);
        
        textRappAmmSocio = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textRappAmmSocio, 16, SpringLayout.SOUTH, lblRappAmmSocio);
        springLayout.putConstraint(SpringLayout.WEST, textRappAmmSocio, 6, SpringLayout.EAST, lblCodSocio);
        springLayout.putConstraint(SpringLayout.EAST, textRappAmmSocio, 151, SpringLayout.EAST, lblCodSocio);
        frmSocioGui.getContentPane().add(textRappAmmSocio);
        textRappAmmSocio.setColumns(10);
        
        JButton btnVisRappCodSocio = new JButton("VISUALIZZA");
        btnVisRappCodSocio.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = opAmm.getRS_RapSing(textRappAmmSocio.getText());
                tableSocio.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.WEST, btnVisRappCodSocio, 10, SpringLayout.EAST, textRappAmmSocio);
        springLayout.putConstraint(SpringLayout.SOUTH, btnVisRappCodSocio, 0, SpringLayout.SOUTH, lblCodSocio);
        frmSocioGui.getContentPane().add(btnVisRappCodSocio);
    }
    
    public void run() {
        try {
            SocioGUI window = new SocioGUI();
            window.frmSocioGui.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
