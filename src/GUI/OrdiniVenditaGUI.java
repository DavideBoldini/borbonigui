package GUI;

import javax.swing.JFrame;
import javax.swing.SpringLayout;

import Ordine.OperationOrdini;
import Ordine.Ordine;
import ProdottoVenduto.OperationPVend;
import ProdottoVenduto.PVend;
import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import javax.swing.JScrollPane;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class OrdiniVenditaGUI {

    private JFrame frmOrdiniVenditaGui;
    private JTable tableOrdiniVendita;
    private JTable tableProdVenduti;
    private JTextField textAnno;
    private JTextField textQuantità;
    private JTextField textImporto;
    private JTextField textCodUffVend;
    private JTextField textVisCodOrd;
    
    private OperationOrdini opOrdV = new OperationOrdini();
    private OperationPVend opPVen = new OperationPVend();
    private JTextField textCodOrdine;
    private JTextField textCodProd;
    private JTextField textCodProdDel;
    private JTextField textQ;
    
    

    /**
     * Create the application.
     */
    public OrdiniVenditaGUI() throws SQLException, ClassNotFoundException{
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmOrdiniVenditaGui = new JFrame();
        frmOrdiniVenditaGui.setTitle("Ordini Vendita GUI");
        frmOrdiniVenditaGui.setResizable(false);
        frmOrdiniVenditaGui.setBounds(100, 100, 1240, 710);
        frmOrdiniVenditaGui.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        SpringLayout springLayout = new SpringLayout();
        frmOrdiniVenditaGui.getContentPane().setLayout(springLayout);
        
        JLabel lblTitle = new JLabel("Ordini Vendita");
        springLayout.putConstraint(SpringLayout.NORTH, lblTitle, 10, SpringLayout.NORTH, frmOrdiniVenditaGui.getContentPane());
        lblTitle.setFont(new Font("Tahoma", Font.BOLD, 40));
        frmOrdiniVenditaGui.getContentPane().add(lblTitle);
        
        JButton btnOrdiniAcquisto = new JButton("ORDINI ACQUISTO >>");
        springLayout.putConstraint(SpringLayout.EAST, lblTitle, -277, SpringLayout.WEST, btnOrdiniAcquisto);
        springLayout.putConstraint(SpringLayout.NORTH, btnOrdiniAcquisto, 0, SpringLayout.NORTH, lblTitle);
        springLayout.putConstraint(SpringLayout.SOUTH, btnOrdiniAcquisto, -16, SpringLayout.SOUTH, lblTitle);
        springLayout.putConstraint(SpringLayout.EAST, btnOrdiniAcquisto, -64, SpringLayout.EAST, frmOrdiniVenditaGui.getContentPane());
        btnOrdiniAcquisto.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    OrdiniAcquistoGUI oAGUI = new OrdiniAcquistoGUI();
                    oAGUI.run();
                } catch (ClassNotFoundException | SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        btnOrdiniAcquisto.setBackground(Color.YELLOW);
        frmOrdiniVenditaGui.getContentPane().add(btnOrdiniAcquisto);
        
        tableOrdiniVendita = new JTable() {
            public boolean isCellEditable(int row,int column){  
                return false; 
              }
        };
        JScrollPane scrollVendita = new JScrollPane(tableOrdiniVendita);
        springLayout.putConstraint(SpringLayout.EAST, scrollVendita, -63, SpringLayout.EAST, frmOrdiniVenditaGui.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, tableOrdiniVendita, 113, SpringLayout.NORTH, frmOrdiniVenditaGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, tableOrdiniVendita, -45, SpringLayout.EAST, frmOrdiniVenditaGui.getContentPane());
        frmOrdiniVenditaGui.getContentPane().add(scrollVendita);
        
        tableProdVenduti = new JTable() {
            public boolean isCellEditable(int row,int column){  
                return false; 
              }
        };
        JScrollPane scrollProdVend = new JScrollPane(tableProdVenduti);
        springLayout.putConstraint(SpringLayout.WEST, scrollProdVend, 0, SpringLayout.WEST, scrollVendita);
        springLayout.putConstraint(SpringLayout.SOUTH, scrollProdVend, -82, SpringLayout.SOUTH, frmOrdiniVenditaGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, scrollProdVend, -70, SpringLayout.EAST, frmOrdiniVenditaGui.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, tableProdVenduti, 96, SpringLayout.SOUTH, tableOrdiniVendita);
        springLayout.putConstraint(SpringLayout.SOUTH, tableProdVenduti, -82, SpringLayout.SOUTH, frmOrdiniVenditaGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, tableProdVenduti, -35, SpringLayout.EAST, frmOrdiniVenditaGui.getContentPane());
        frmOrdiniVenditaGui.getContentPane().add(scrollProdVend);
        
        JLabel lblOrdiniVendita = new JLabel("Ordini Vendita:");
        springLayout.putConstraint(SpringLayout.NORTH, scrollVendita, 6, SpringLayout.SOUTH, lblOrdiniVendita);
        springLayout.putConstraint(SpringLayout.WEST, lblOrdiniVendita, 0, SpringLayout.WEST, tableOrdiniVendita);
        springLayout.putConstraint(SpringLayout.SOUTH, lblOrdiniVendita, -6, SpringLayout.NORTH, tableOrdiniVendita);
        lblOrdiniVendita.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmOrdiniVenditaGui.getContentPane().add(lblOrdiniVendita);
        
        JLabel lblProdottiVenduti = new JLabel("Prodotti Venduti:");
        springLayout.putConstraint(SpringLayout.SOUTH, lblProdottiVenduti, -283, SpringLayout.SOUTH, frmOrdiniVenditaGui.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, scrollProdVend, 6, SpringLayout.SOUTH, lblProdottiVenduti);
        springLayout.putConstraint(SpringLayout.WEST, lblProdottiVenduti, 0, SpringLayout.WEST, tableOrdiniVendita);
        lblProdottiVenduti.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmOrdiniVenditaGui.getContentPane().add(lblProdottiVenduti);
        
        JLabel lblCreaOrdine = new JLabel("Crea Ordine Vendita:");
        springLayout.putConstraint(SpringLayout.WEST, lblCreaOrdine, 10, SpringLayout.WEST, frmOrdiniVenditaGui.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, lblCreaOrdine, -589, SpringLayout.SOUTH, frmOrdiniVenditaGui.getContentPane());
        lblCreaOrdine.setFont(new Font("Tahoma", Font.BOLD, 20));
        frmOrdiniVenditaGui.getContentPane().add(lblCreaOrdine);
        
        textAnno = new JTextField();
        textAnno.setText("2020");
        textAnno.setEnabled(false);
        frmOrdiniVenditaGui.getContentPane().add(textAnno);
        textAnno.setColumns(10);
        
        textQuantità = new JTextField();
        textQuantità.setText("0");
        springLayout.putConstraint(SpringLayout.WEST, tableOrdiniVendita, 357, SpringLayout.EAST, textQuantità);
        textQuantità.setEnabled(false);
        frmOrdiniVenditaGui.getContentPane().add(textQuantità);
        textQuantità.setColumns(10);
        
        textImporto = new JTextField();
        textImporto.setText("0");
        textImporto.setEnabled(false);
        frmOrdiniVenditaGui.getContentPane().add(textImporto);
        textImporto.setColumns(10);
        
        textCodUffVend = new JTextField();
        springLayout.putConstraint(SpringLayout.WEST, scrollVendita, 304, SpringLayout.EAST, textCodUffVend);
        frmOrdiniVenditaGui.getContentPane().add(textCodUffVend);
        textCodUffVend.setColumns(10);
        
        JLabel lblVisOrdine = new JLabel("Visualizza Ordine Vendita");
        springLayout.putConstraint(SpringLayout.WEST, lblVisOrdine, 0, SpringLayout.WEST, lblCreaOrdine);
        springLayout.putConstraint(SpringLayout.SOUTH, lblVisOrdine, -336, SpringLayout.SOUTH, frmOrdiniVenditaGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, tableProdVenduti, 298, SpringLayout.EAST, lblVisOrdine);
        lblVisOrdine.setFont(new Font("Tahoma", Font.BOLD, 20));
        frmOrdiniVenditaGui.getContentPane().add(lblVisOrdine);
        
        textVisCodOrd = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textVisCodOrd, 7, SpringLayout.SOUTH, lblVisOrdine);
        springLayout.putConstraint(SpringLayout.EAST, textVisCodOrd, -304, SpringLayout.WEST, lblProdottiVenduti);
        frmOrdiniVenditaGui.getContentPane().add(textVisCodOrd);
        textVisCodOrd.setColumns(10);
        
        JLabel lblVisCodOrd = new JLabel("Codice Ordine:");
        springLayout.putConstraint(SpringLayout.WEST, textVisCodOrd, 19, SpringLayout.EAST, lblVisCodOrd);
        springLayout.putConstraint(SpringLayout.NORTH, lblVisCodOrd, 6, SpringLayout.SOUTH, lblVisOrdine);
        springLayout.putConstraint(SpringLayout.WEST, lblVisCodOrd, 0, SpringLayout.WEST, lblCreaOrdine);
        lblVisCodOrd.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmOrdiniVenditaGui.getContentPane().add(lblVisCodOrd);
        
        JButton btnVisOrdiniVend = new JButton("VISUALIZZA");
        springLayout.putConstraint(SpringLayout.NORTH, lblProdottiVenduti, 31, SpringLayout.SOUTH, btnVisOrdiniVend);
        btnVisOrdiniVend.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {              
                ResultSet rS = opOrdV.getRS_Ord("ordine_vendita");
                tableOrdiniVendita.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.SOUTH, scrollVendita, -6, SpringLayout.NORTH, btnVisOrdiniVend);
        springLayout.putConstraint(SpringLayout.NORTH, btnVisOrdiniVend, 309, SpringLayout.NORTH, frmOrdiniVenditaGui.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, tableOrdiniVendita, -6, SpringLayout.NORTH, btnVisOrdiniVend);
        springLayout.putConstraint(SpringLayout.WEST, btnVisOrdiniVend, 0, SpringLayout.WEST, tableOrdiniVendita);
        frmOrdiniVenditaGui.getContentPane().add(btnVisOrdiniVend);
        
        JButton btnVisCodOrd = new JButton("VISUALIZZA");
        springLayout.putConstraint(SpringLayout.WEST, btnVisCodOrd, 0, SpringLayout.WEST, lblCreaOrdine);
        springLayout.putConstraint(SpringLayout.SOUTH, btnVisCodOrd, 0, SpringLayout.SOUTH, lblProdottiVenduti);
        btnVisCodOrd.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               ResultSet rS = opOrdV.getRS_OrdSing("ordine_vendita", textVisCodOrd.getText());
               tableOrdiniVendita.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        frmOrdiniVenditaGui.getContentPane().add(btnVisCodOrd);
        
        JLabel lblAnno = new JLabel("Anno:");
        springLayout.putConstraint(SpringLayout.NORTH, textAnno, 0, SpringLayout.NORTH, lblAnno);
        springLayout.putConstraint(SpringLayout.WEST, textAnno, 4, SpringLayout.EAST, lblAnno);
        springLayout.putConstraint(SpringLayout.WEST, lblAnno, 0, SpringLayout.WEST, lblCreaOrdine);
        lblAnno.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmOrdiniVenditaGui.getContentPane().add(lblAnno);
        
        JLabel lblQuantità = new JLabel("Quantit\u00E0 TOT:");
        springLayout.putConstraint(SpringLayout.NORTH, textQuantità, 0, SpringLayout.NORTH, lblQuantità);
        springLayout.putConstraint(SpringLayout.WEST, textQuantità, 9, SpringLayout.EAST, lblQuantità);
        springLayout.putConstraint(SpringLayout.NORTH, lblQuantità, 14, SpringLayout.SOUTH, textAnno);
        springLayout.putConstraint(SpringLayout.WEST, lblQuantità, 0, SpringLayout.WEST, lblCreaOrdine);
        lblQuantità.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmOrdiniVenditaGui.getContentPane().add(lblQuantità);
        
        JLabel lblImporto = new JLabel("Importo:");
        springLayout.putConstraint(SpringLayout.NORTH, textImporto, 0, SpringLayout.NORTH, lblImporto);
        springLayout.putConstraint(SpringLayout.WEST, textImporto, 6, SpringLayout.EAST, lblImporto);
        springLayout.putConstraint(SpringLayout.WEST, lblImporto, 0, SpringLayout.WEST, lblCreaOrdine);
        lblImporto.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmOrdiniVenditaGui.getContentPane().add(lblImporto);
        
        JLabel lblCodUffVend = new JLabel("Codice Ufficio Vendite:");
        springLayout.putConstraint(SpringLayout.SOUTH, lblImporto, -17, SpringLayout.NORTH, lblCodUffVend);
        springLayout.putConstraint(SpringLayout.WEST, lblCodUffVend, 10, SpringLayout.WEST, frmOrdiniVenditaGui.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, textCodUffVend, 0, SpringLayout.NORTH, lblCodUffVend);
        springLayout.putConstraint(SpringLayout.WEST, textCodUffVend, 6, SpringLayout.EAST, lblCodUffVend);
        lblCodUffVend.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmOrdiniVenditaGui.getContentPane().add(lblCodUffVend);
        
        JButton btnInserisci = new JButton("INSERISCI");
        springLayout.putConstraint(SpringLayout.NORTH, btnInserisci, 269, SpringLayout.NORTH, frmOrdiniVenditaGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, btnInserisci, 10, SpringLayout.WEST, frmOrdiniVenditaGui.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, lblCodUffVend, -17, SpringLayout.NORTH, btnInserisci);
        btnInserisci.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                Ordine oV = new Ordine(textCodUffVend.getText());
                try {
                    opOrdV.insertOrdine(oV);
                    ResultSet rS = opOrdV.getRS_Ord("ordine_vendita");
                    tableOrdiniVendita.setModel(DbUtils.resultSetToTableModel(rS));
                    textCodOrdine.setText(opOrdV.getRS_NewOrderVend());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }                              
            }
        });
        frmOrdiniVenditaGui.getContentPane().add(btnInserisci);
        
        JButton btnReset = new JButton("RESET");
        springLayout.putConstraint(SpringLayout.NORTH, btnReset, 0, SpringLayout.NORTH, btnInserisci);
        springLayout.putConstraint(SpringLayout.WEST, btnReset, 20, SpringLayout.EAST, btnInserisci);
        btnReset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textCodUffVend.setText("");
            }
        });
        frmOrdiniVenditaGui.getContentPane().add(btnReset);
        
        JLabel lblAddProdOrd = new JLabel("Aggiungi Prodotto ad ultimo Ordine:");
        springLayout.putConstraint(SpringLayout.NORTH, lblAddProdOrd, 11, SpringLayout.SOUTH, btnVisCodOrd);
        springLayout.putConstraint(SpringLayout.WEST, lblAddProdOrd, 0, SpringLayout.WEST, lblCreaOrdine);
        lblAddProdOrd.setFont(new Font("Tahoma", Font.BOLD, 20));
        frmOrdiniVenditaGui.getContentPane().add(lblAddProdOrd);
        
        JLabel lblDelProdVenduto = new JLabel("Cancella Prodotto da ultimo Ordine:");
        springLayout.putConstraint(SpringLayout.WEST, lblDelProdVenduto, 0, SpringLayout.WEST, lblCreaOrdine);
        lblDelProdVenduto.setFont(new Font("Tahoma", Font.BOLD, 20));
        frmOrdiniVenditaGui.getContentPane().add(lblDelProdVenduto);
        
        JLabel lblCodiceOrdine = new JLabel("Codice Ordine:");
        springLayout.putConstraint(SpringLayout.NORTH, lblAnno, 16, SpringLayout.SOUTH, lblCodiceOrdine);
        springLayout.putConstraint(SpringLayout.NORTH, lblCodiceOrdine, 8, SpringLayout.SOUTH, lblCreaOrdine);
        springLayout.putConstraint(SpringLayout.WEST, lblCodiceOrdine, 0, SpringLayout.WEST, lblCreaOrdine);
        lblCodiceOrdine.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmOrdiniVenditaGui.getContentPane().add(lblCodiceOrdine);
        
        textCodOrdine = new JTextField();
        textCodOrdine.setText(opOrdV.getRS_NewOrderVend());
        springLayout.putConstraint(SpringLayout.NORTH, textCodOrdine, 6, SpringLayout.SOUTH, lblCreaOrdine);
        springLayout.putConstraint(SpringLayout.WEST, textCodOrdine, 6, SpringLayout.EAST, lblCodiceOrdine);
        textCodOrdine.setEnabled(false);
        frmOrdiniVenditaGui.getContentPane().add(textCodOrdine);
        textCodOrdine.setColumns(10);
        
        JLabel lblCodProd = new JLabel("Codice Prodotto:");
        springLayout.putConstraint(SpringLayout.NORTH, lblCodProd, 6, SpringLayout.SOUTH, lblAddProdOrd);
        springLayout.putConstraint(SpringLayout.WEST, lblCodProd, 0, SpringLayout.WEST, lblCreaOrdine);
        lblCodProd.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmOrdiniVenditaGui.getContentPane().add(lblCodProd);
        
        textCodProd = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textCodProd, 6, SpringLayout.SOUTH, lblAddProdOrd);
        springLayout.putConstraint(SpringLayout.WEST, textCodProd, -86, SpringLayout.EAST, lblCreaOrdine);
        springLayout.putConstraint(SpringLayout.EAST, textCodProd, 0, SpringLayout.EAST, textCodUffVend);
        frmOrdiniVenditaGui.getContentPane().add(textCodProd);
        textCodProd.setColumns(10);
        
        JButton btnAggiungi = new JButton("AGGIUNGI");
        springLayout.putConstraint(SpringLayout.NORTH, lblDelProdVenduto, 16, SpringLayout.SOUTH, btnAggiungi);
        springLayout.putConstraint(SpringLayout.NORTH, btnAggiungi, 6, SpringLayout.SOUTH, lblCodProd);
        springLayout.putConstraint(SpringLayout.WEST, btnAggiungi, 0, SpringLayout.WEST, lblCreaOrdine);
        btnAggiungi.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                PVend p = new PVend(textCodProd.getText(), textQ.getText());
                Integer lastOrdine = Integer.parseInt(opOrdV.getRS_NewOrderVend())-1;
                try {
                    opPVen.insertPVend(lastOrdine.toString(), p);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                ResultSet rS = opOrdV.getRS_Ord("ordine_vendita");
                ResultSet rS_PVen = opPVen.getRS_PVendInOrd(lastOrdine.toString());
                tableOrdiniVendita.setModel(DbUtils.resultSetToTableModel(rS));
                tableProdVenduti.setModel(DbUtils.resultSetToTableModel(rS_PVen));
            }
        });
        frmOrdiniVenditaGui.getContentPane().add(btnAggiungi);
        
        JLabel lblCodProdDel = new JLabel("Codice Prodotto:");
        springLayout.putConstraint(SpringLayout.NORTH, lblCodProdDel, 6, SpringLayout.SOUTH, lblDelProdVenduto);
        springLayout.putConstraint(SpringLayout.WEST, lblCodProdDel, 0, SpringLayout.WEST, lblCreaOrdine);
        lblCodProdDel.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmOrdiniVenditaGui.getContentPane().add(lblCodProdDel);
        
        textCodProdDel = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textCodProdDel, 6, SpringLayout.SOUTH, lblDelProdVenduto);
        springLayout.putConstraint(SpringLayout.WEST, textCodProdDel, 20, SpringLayout.EAST, lblCodProdDel);
        springLayout.putConstraint(SpringLayout.EAST, textCodProdDel, 0, SpringLayout.EAST, lblVisOrdine);
        textCodProdDel.setColumns(10);
        frmOrdiniVenditaGui.getContentPane().add(textCodProdDel);
        
        JButton btnDelCodProd = new JButton("ELIMINA");
        springLayout.putConstraint(SpringLayout.NORTH, btnDelCodProd, 6, SpringLayout.SOUTH, lblCodProdDel);
        springLayout.putConstraint(SpringLayout.WEST, btnDelCodProd, 0, SpringLayout.WEST, lblCreaOrdine);
        btnDelCodProd.addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent e) {
                Integer lastOrd = Integer.parseInt(opOrdV.getRS_NewOrderVend())-1;
                try {
                    opPVen.deletePVendFromOrder(lastOrd.toString(), textCodProdDel.getText());
                    ResultSet rS_Ord = opOrdV.getRS_Ord("ordine_vendita");
                    ResultSet rS_PVend = opPVen.getRS_PVend();
                    tableOrdiniVendita.setModel(DbUtils.resultSetToTableModel(rS_Ord));
                    tableProdVenduti.setModel(DbUtils.resultSetToTableModel(rS_PVend));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        frmOrdiniVenditaGui.getContentPane().add(btnDelCodProd);
        
        JLabel lblQ = new JLabel("Quantit\u00E0:");
        springLayout.putConstraint(SpringLayout.NORTH, lblQ, 6, SpringLayout.SOUTH, lblAddProdOrd);
        lblQ.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmOrdiniVenditaGui.getContentPane().add(lblQ);
        
        textQ = new JTextField();
        springLayout.putConstraint(SpringLayout.WEST, textQ, 363, SpringLayout.WEST, frmOrdiniVenditaGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, lblQ, -6, SpringLayout.WEST, textQ);
        springLayout.putConstraint(SpringLayout.NORTH, textQ, 0, SpringLayout.NORTH, lblCodProd);
        frmOrdiniVenditaGui.getContentPane().add(textQ);
        textQ.setColumns(10);
        
        JButton btnVisualizzaPVend = new JButton("VISUALIZZA");
        btnVisualizzaPVend.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = opPVen.getRS_PVend();
                tableProdVenduti.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnVisualizzaPVend, 6, SpringLayout.SOUTH, scrollProdVend);
        springLayout.putConstraint(SpringLayout.WEST, btnVisualizzaPVend, 0, SpringLayout.WEST, scrollVendita);
        frmOrdiniVenditaGui.getContentPane().add(btnVisualizzaPVend);
        
        JButton btnResiReclami = new JButton("RESI E RECLAMI >>");
        btnResiReclami.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResiReclamiGUI reResGUI;
                try {
                    reResGUI = new ResiReclamiGUI();
                    reResGUI.run();
                } catch (ClassNotFoundException | SQLException e1) {
                    e1.printStackTrace();
                }              
            }
        });
        
        springLayout.putConstraint(SpringLayout.NORTH, btnResiReclami, 6, SpringLayout.SOUTH, btnOrdiniAcquisto);
        springLayout.putConstraint(SpringLayout.WEST, btnResiReclami, 0, SpringLayout.WEST, btnOrdiniAcquisto);
        springLayout.putConstraint(SpringLayout.SOUTH, btnResiReclami, -10, SpringLayout.SOUTH, lblCreaOrdine);
        springLayout.putConstraint(SpringLayout.EAST, btnResiReclami, 0, SpringLayout.EAST, btnOrdiniAcquisto);
        frmOrdiniVenditaGui.getContentPane().add(btnResiReclami);
        
        JLabel lblAltro = new JLabel("Altro:");
        lblAltro.setFont(new Font("Tahoma", Font.BOLD, 20));
        springLayout.putConstraint(SpringLayout.NORTH, lblAltro, 0, SpringLayout.NORTH, btnVisualizzaPVend);
        springLayout.putConstraint(SpringLayout.WEST, lblAltro, 0, SpringLayout.WEST, lblCreaOrdine);
        frmOrdiniVenditaGui.getContentPane().add(lblAltro);
        
        JButton btnRappVendite = new JButton("Rapporto Vendite Prodotti \"Interni\" ed \"Esterni\"");
        btnRappVendite.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = opOrdV.getRS_RapportoVendEsterniInterni();
                tableOrdiniVendita.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnRappVendite, 0, SpringLayout.NORTH, btnVisualizzaPVend);
        springLayout.putConstraint(SpringLayout.WEST, btnRappVendite, 13, SpringLayout.EAST, lblAltro);
        frmOrdiniVenditaGui.getContentPane().add(btnRappVendite);
        
        JButton btnProdEstivo = new JButton("Visualizzazione Prodotti Esterni periodo \"Estivo\"");
        btnProdEstivo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = opOrdV.getRS_EsterniVendInEstate();
                tableOrdiniVendita.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnProdEstivo, 7, SpringLayout.SOUTH, btnRappVendite);
        springLayout.putConstraint(SpringLayout.EAST, btnProdEstivo, 0, SpringLayout.EAST, btnRappVendite);
        frmOrdiniVenditaGui.getContentPane().add(btnProdEstivo);
    }
    
    public void run() {
        try {
            OrdiniVenditaGUI window = new OrdiniVenditaGUI();
            window.frmOrdiniVenditaGui.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
