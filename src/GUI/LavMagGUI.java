package GUI;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SpringLayout;
import javax.swing.table.DefaultTableModel;

import LavoratoreMagazzino.LavMag;
import LavoratoreMagazzino.OperationLavMag;
import Magazzino.Magazzino;
import Magazzino.OperationMagazzino;
import net.proteanit.sql.DbUtils;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.SystemColor;

//
public class LavMagGUI {

    private JFrame frmLavoratoriMagazzinoGui;
    private JTextField textCodLav;
    private JTextField textCF;
    private JTextField textNome;
    private JTextField textCognome;
    private JTextField textD_Nascita;
    private JTextField textTelefono;
    private JTextField textVia;
    private JTextField textCivico;
    private JTextField textCitt�;
    private JTextField textSalario;
    private JTextField textRicerca;
    private JTextField textAvg;
    private JTextField textDelCodLav;
    private JTable tableLavMag;
    private JTable tableMag;
    private JTextField textDelMag;
    private JTextField textInsertMagazzino;
    private JTextField textInsertNewMagazzino;

    private OperationLavMag opLavMag = new OperationLavMag();
    private OperationMagazzino opMag = new OperationMagazzino();
    private List<JTextField> textFieldsList = new ArrayList<JTextField>();
    private List<String> textFieldsStrings = new ArrayList<String>();

    /**
     * Create the application.
     * 
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public LavMagGUI() throws ClassNotFoundException, SQLException {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    @SuppressWarnings("serial")
    private void initialize() {
        frmLavoratoriMagazzinoGui = new JFrame();
        frmLavoratoriMagazzinoGui.setTitle("Lavoratori Magazzino GUI");
        frmLavoratoriMagazzinoGui.setAlwaysOnTop(true);
        frmLavoratoriMagazzinoGui.setResizable(false);
        frmLavoratoriMagazzinoGui.setBounds(100, 100, 1242, 714);
        frmLavoratoriMagazzinoGui.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        SpringLayout springLayout = new SpringLayout();
        frmLavoratoriMagazzinoGui.getContentPane().setLayout(springLayout);

        JLabel lblNewLabel = new JLabel("LAVORATORI MAGAZZINO");
        lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 40));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblNewLabel);

        JLabel lblCodLavoratore = new JLabel("Codice Lavoratore:");
        lblCodLavoratore.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblCodLavoratore, 83, SpringLayout.NORTH,
                frmLavoratoriMagazzinoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblCodLavoratore, 10, SpringLayout.WEST, frmLavoratoriMagazzinoGui.getContentPane());
        frmLavoratoriMagazzinoGui.getContentPane().add(lblCodLavoratore);

        JLabel lblCodiceFiscale = new JLabel("Codice Fiscale:");
        springLayout.putConstraint(SpringLayout.NORTH, lblCodiceFiscale, 22, SpringLayout.SOUTH, lblCodLavoratore);
        springLayout.putConstraint(SpringLayout.WEST, lblCodiceFiscale, 0, SpringLayout.WEST, lblCodLavoratore);
        lblCodiceFiscale.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblCodiceFiscale);

        JLabel lblNome = new JLabel("Nome:");
        springLayout.putConstraint(SpringLayout.NORTH, lblNome, 16, SpringLayout.SOUTH, lblCodiceFiscale);
        springLayout.putConstraint(SpringLayout.WEST, lblNome, 10, SpringLayout.WEST, frmLavoratoriMagazzinoGui.getContentPane());
        lblNome.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblNome);

        JLabel lblDataNascita = new JLabel("Data Nascita (AAAA-MM-GG):");
        springLayout.putConstraint(SpringLayout.WEST, lblDataNascita, 0, SpringLayout.WEST, lblCodLavoratore);
        lblDataNascita.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblDataNascita);

        textCodLav = new JTextField();
        textFieldsList.add(textCodLav);
        springLayout.putConstraint(SpringLayout.NORTH, textCodLav, 0, SpringLayout.NORTH, lblCodLavoratore);
        springLayout.putConstraint(SpringLayout.WEST, textCodLav, 6, SpringLayout.EAST, lblCodLavoratore);
        springLayout.putConstraint(SpringLayout.EAST, textCodLav, 158, SpringLayout.EAST, lblCodLavoratore);
        frmLavoratoriMagazzinoGui.getContentPane().add(textCodLav);
        textCodLav.setColumns(10);

        textCF = new JTextField();
        textFieldsList.add(textCF);
        springLayout.putConstraint(SpringLayout.NORTH, textCF, 16, SpringLayout.SOUTH, textCodLav);
        springLayout.putConstraint(SpringLayout.WEST, textCF, 6, SpringLayout.EAST, lblCodiceFiscale);
        springLayout.putConstraint(SpringLayout.EAST, textCF, 151, SpringLayout.EAST, lblCodiceFiscale);
        textCF.setColumns(10);
        frmLavoratoriMagazzinoGui.getContentPane().add(textCF);

        textNome = new JTextField();
        textFieldsList.add(textNome);
        springLayout.putConstraint(SpringLayout.NORTH, lblDataNascita, 14, SpringLayout.SOUTH, textNome);
        springLayout.putConstraint(SpringLayout.NORTH, textNome, 0, SpringLayout.NORTH, lblNome);
        springLayout.putConstraint(SpringLayout.WEST, textNome, 6, SpringLayout.EAST, lblNome);
        textNome.setColumns(10);
        frmLavoratoriMagazzinoGui.getContentPane().add(textNome);

        JLabel lblCognome = new JLabel("Cognome:");
        springLayout.putConstraint(SpringLayout.WEST, lblCognome, 6, SpringLayout.EAST, textNome);
        springLayout.putConstraint(SpringLayout.SOUTH, lblCognome, 0, SpringLayout.SOUTH, lblNome);
        lblCognome.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblCognome);

        textCognome = new JTextField();
        textFieldsList.add(textCognome);
        springLayout.putConstraint(SpringLayout.NORTH, textCognome, 0, SpringLayout.NORTH, lblNome);
        springLayout.putConstraint(SpringLayout.WEST, textCognome, 6, SpringLayout.EAST, lblCognome);
        textCognome.setColumns(10);
        frmLavoratoriMagazzinoGui.getContentPane().add(textCognome);

        textD_Nascita = new JTextField();
        textFieldsList.add(textD_Nascita);
        springLayout.putConstraint(SpringLayout.NORTH, textD_Nascita, 11, SpringLayout.SOUTH, textCognome);
        springLayout.putConstraint(SpringLayout.WEST, textD_Nascita, 6, SpringLayout.EAST, lblDataNascita);
        springLayout.putConstraint(SpringLayout.EAST, textD_Nascita, 158, SpringLayout.EAST, lblDataNascita);
        textD_Nascita.setColumns(10);
        frmLavoratoriMagazzinoGui.getContentPane().add(textD_Nascita);

        JLabel lblRuolo = new JLabel("Ruolo:");
        springLayout.putConstraint(SpringLayout.NORTH, lblRuolo, 16, SpringLayout.SOUTH, lblDataNascita);
        springLayout.putConstraint(SpringLayout.WEST, lblRuolo, 0, SpringLayout.WEST, lblCodLavoratore);
        lblRuolo.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblRuolo);

        JRadioButton rdbtnOperaio = new JRadioButton("Operaio");
        rdbtnOperaio.setActionCommand("Operaio");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnOperaio, -1, SpringLayout.NORTH, lblRuolo);
        springLayout.putConstraint(SpringLayout.WEST, rdbtnOperaio, 0, SpringLayout.WEST, textNome);
        frmLavoratoriMagazzinoGui.getContentPane().add(rdbtnOperaio);

        JRadioButton rdbtnMagazziniere = new JRadioButton("Magazziniere");
        rdbtnMagazziniere.setActionCommand("Magazziniere");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnMagazziniere, -1, SpringLayout.NORTH, lblRuolo);
        springLayout.putConstraint(SpringLayout.WEST, rdbtnMagazziniere, 0, SpringLayout.WEST, textCodLav);
        frmLavoratoriMagazzinoGui.getContentPane().add(rdbtnMagazziniere);

        JRadioButton rdbtnCapoTurno = new JRadioButton("Capo Turno");
        rdbtnCapoTurno.setActionCommand("Capo Turno");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnCapoTurno, -1, SpringLayout.NORTH, lblRuolo);
        frmLavoratoriMagazzinoGui.getContentPane().add(rdbtnCapoTurno);

        ButtonGroup group = new ButtonGroup();
        group.add(rdbtnOperaio);
        group.add(rdbtnMagazziniere);
        group.add(rdbtnCapoTurno);

        JLabel lblTelefono = new JLabel("Telefono:");
        springLayout.putConstraint(SpringLayout.NORTH, lblTelefono, 18, SpringLayout.SOUTH, lblRuolo);
        springLayout.putConstraint(SpringLayout.WEST, lblTelefono, 0, SpringLayout.WEST, lblCodLavoratore);
        lblTelefono.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblTelefono);

        textTelefono = new JTextField();
        textFieldsList.add(textTelefono);
        springLayout.putConstraint(SpringLayout.WEST, textTelefono, 6, SpringLayout.EAST, lblTelefono);
        springLayout.putConstraint(SpringLayout.SOUTH, textTelefono, 0, SpringLayout.SOUTH, lblTelefono);
        springLayout.putConstraint(SpringLayout.EAST, textTelefono, 19, SpringLayout.EAST, lblCognome);
        textTelefono.setColumns(10);
        frmLavoratoriMagazzinoGui.getContentPane().add(textTelefono);

        JLabel lblVia = new JLabel("Via:");
        springLayout.putConstraint(SpringLayout.NORTH, lblVia, 22, SpringLayout.SOUTH, lblTelefono);
        springLayout.putConstraint(SpringLayout.WEST, lblVia, 0, SpringLayout.WEST, lblCodLavoratore);
        lblVia.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblVia);

        textVia = new JTextField();
        textFieldsList.add(textVia);
        springLayout.putConstraint(SpringLayout.NORTH, textVia, 0, SpringLayout.NORTH, lblVia);
        springLayout.putConstraint(SpringLayout.WEST, textVia, 6, SpringLayout.EAST, lblVia);
        springLayout.putConstraint(SpringLayout.EAST, textVia, 137, SpringLayout.EAST, lblVia);
        textVia.setColumns(10);
        frmLavoratoriMagazzinoGui.getContentPane().add(textVia);

        JLabel lblCivico = new JLabel("Civico:");
        springLayout.putConstraint(SpringLayout.NORTH, lblCivico, 0, SpringLayout.NORTH, lblVia);
        springLayout.putConstraint(SpringLayout.WEST, lblCivico, 6, SpringLayout.EAST, textVia);
        lblCivico.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblCivico);

        textCivico = new JTextField();
        textFieldsList.add(textCivico);
        springLayout.putConstraint(SpringLayout.NORTH, textCivico, 0, SpringLayout.NORTH, lblVia);
        springLayout.putConstraint(SpringLayout.WEST, textCivico, 0, SpringLayout.WEST, textCognome);
        springLayout.putConstraint(SpringLayout.EAST, textCivico, -14, SpringLayout.EAST, textCognome);
        textCivico.setColumns(10);
        frmLavoratoriMagazzinoGui.getContentPane().add(textCivico);

        JLabel lblCitt� = new JLabel("Citt\u00E0:");
        springLayout.putConstraint(SpringLayout.NORTH, lblCitt�, 24, SpringLayout.SOUTH, textVia);
        springLayout.putConstraint(SpringLayout.WEST, lblCitt�, 0, SpringLayout.WEST, lblCodLavoratore);
        lblCitt�.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblCitt�);

        textCitt� = new JTextField();
        textFieldsList.add(textCitt�);
        springLayout.putConstraint(SpringLayout.NORTH, textCitt�, 0, SpringLayout.NORTH, lblCitt�);
        springLayout.putConstraint(SpringLayout.WEST, textCitt�, -13, SpringLayout.WEST, textNome);
        springLayout.putConstraint(SpringLayout.EAST, textCitt�, 0, SpringLayout.EAST, lblDataNascita);
        textCitt�.setColumns(10);
        frmLavoratoriMagazzinoGui.getContentPane().add(textCitt�);

        JLabel lblSalario = new JLabel("Salario: ");
        springLayout.putConstraint(SpringLayout.NORTH, lblSalario, 20, SpringLayout.SOUTH, lblCitt�);
        springLayout.putConstraint(SpringLayout.WEST, lblSalario, 0, SpringLayout.WEST, lblCodLavoratore);
        lblSalario.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblSalario);

        textSalario = new JTextField();
        textFieldsList.add(textSalario);
        springLayout.putConstraint(SpringLayout.NORTH, textSalario, 0, SpringLayout.NORTH, lblSalario);
        springLayout.putConstraint(SpringLayout.EAST, textSalario, 0, SpringLayout.EAST, textNome);
        textSalario.setColumns(10);
        frmLavoratoriMagazzinoGui.getContentPane().add(textSalario);

        JLabel lblMagazzino = new JLabel("Magazzino:");
        springLayout.putConstraint(SpringLayout.NORTH, lblMagazzino, 0, SpringLayout.NORTH, lblSalario);
        springLayout.putConstraint(SpringLayout.EAST, lblMagazzino, 0, SpringLayout.EAST, textTelefono);
        lblMagazzino.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblMagazzino);

        textInsertMagazzino = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textInsertMagazzino, 0, SpringLayout.NORTH, lblSalario);
        springLayout.putConstraint(SpringLayout.WEST, textInsertMagazzino, 26, SpringLayout.EAST, lblMagazzino);
        textFieldsList.add(textInsertMagazzino);
        frmLavoratoriMagazzinoGui.getContentPane().add(textInsertMagazzino);
        textInsertMagazzino.setColumns(10);

        JButton btnInserisci = new JButton("INSERISCI");
        btnInserisci.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (JTextField jTextField : textFieldsList) {
                    if (jTextField.getText().length() == 0) {
                        textFieldsStrings.add(null);
                    } else {
                        textFieldsStrings.add(jTextField.getText());
                    }
                }
                textFieldsStrings.add(group.getSelection().getActionCommand());
                LavMag lMag = new LavMag(textFieldsStrings);
                try {
                    boolean check = opLavMag.insertLavMag(lMag);
                    if (!check) {
                        textFieldsStrings.clear();
                    }else {
                        ResultSet rS = opLavMag.getRS_LavMag();
                        ResultSet rSMag = opMag.getRS_Mag();
                        tableLavMag.setModel(DbUtils.resultSetToTableModel(rS));
                        tableMag.setModel(DbUtils.resultSetToTableModel(rSMag));
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                textFieldsStrings.clear();
            }
            
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnInserisci, 6, SpringLayout.SOUTH, lblSalario);
        springLayout.putConstraint(SpringLayout.WEST, btnInserisci, 0, SpringLayout.WEST, lblCodLavoratore);
        frmLavoratoriMagazzinoGui.getContentPane().add(btnInserisci);

        JButton btnReset = new JButton("RESET");
        btnReset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (JTextField jTextField : textFieldsList) {
                    jTextField.setText("");
                }
                textFieldsStrings.clear();
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnReset, 0, SpringLayout.NORTH, btnInserisci);
        springLayout.putConstraint(SpringLayout.EAST, btnReset, 0, SpringLayout.EAST, textVia);
        frmLavoratoriMagazzinoGui.getContentPane().add(btnReset);

        JLabel lblLavMag = new JLabel("Lavoratori Magazzino:");
        springLayout.putConstraint(SpringLayout.NORTH, lblLavMag, 65, SpringLayout.NORTH, frmLavoratoriMagazzinoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, lblNewLabel, -6, SpringLayout.NORTH, lblLavMag);
        lblLavMag.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblLavMag);

        JLabel lblDelLavMag = new JLabel("Eliminazione Lavoratori magazzino:");
        springLayout.putConstraint(SpringLayout.WEST, lblDelLavMag, 0, SpringLayout.WEST, lblLavMag);
        springLayout.putConstraint(SpringLayout.SOUTH, lblDelLavMag, -111, SpringLayout.SOUTH, frmLavoratoriMagazzinoGui.getContentPane());
        lblDelLavMag.setFont(new Font("Tahoma", Font.BOLD, 20));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblDelLavMag);

        JLabel lblMagDel = new JLabel("Magazzino:");
        springLayout.putConstraint(SpringLayout.NORTH, lblMagDel, 580, SpringLayout.NORTH, frmLavoratoriMagazzinoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblMagDel, 0, SpringLayout.WEST, lblLavMag);
        lblMagDel.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblMagDel);

        JLabel lblRicercaOP = new JLabel("Ricerca Operai:");
        springLayout.putConstraint(SpringLayout.NORTH, lblRicercaOP, 451, SpringLayout.NORTH, frmLavoratoriMagazzinoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblRicercaOP, 0, SpringLayout.WEST, lblCodLavoratore);
        lblRicercaOP.setFont(new Font("Tahoma", Font.BOLD, 20));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblRicercaOP);

        JLabel lblSalMedio = new JLabel("Salario Medio:");
        springLayout.putConstraint(SpringLayout.WEST, lblSalMedio, 0, SpringLayout.WEST, lblCodLavoratore);
        springLayout.putConstraint(SpringLayout.SOUTH, lblSalMedio, -142, SpringLayout.SOUTH, frmLavoratoriMagazzinoGui.getContentPane());
        lblSalMedio.setFont(new Font("Tahoma", Font.BOLD, 20));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblSalMedio);

        JLabel lblDelLavMagCod = new JLabel("Elimina Lavoratore magazzino:");
        springLayout.putConstraint(SpringLayout.WEST, lblDelLavMagCod, 0, SpringLayout.WEST, lblCodLavoratore);
        lblDelLavMagCod.setFont(new Font("Tahoma", Font.BOLD, 20));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblDelLavMagCod);

        JLabel lblMagRicerca = new JLabel("Magazzino:");
        springLayout.putConstraint(SpringLayout.NORTH, lblMagRicerca, 6, SpringLayout.SOUTH, lblRicercaOP);
        springLayout.putConstraint(SpringLayout.WEST, lblMagRicerca, 0, SpringLayout.WEST, lblCodLavoratore);
        lblMagRicerca.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblMagRicerca);

        textRicerca = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textRicerca, -26, SpringLayout.NORTH, lblSalMedio);
        springLayout.putConstraint(SpringLayout.WEST, textRicerca, 6, SpringLayout.EAST, lblMagRicerca);
        springLayout.putConstraint(SpringLayout.SOUTH, textRicerca, -6, SpringLayout.NORTH, lblSalMedio);
        springLayout.putConstraint(SpringLayout.EAST, textRicerca, 0, SpringLayout.EAST, lblDataNascita);
        textRicerca.setColumns(10);
        frmLavoratoriMagazzinoGui.getContentPane().add(textRicerca);

        textAvg = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textAvg, 549, SpringLayout.NORTH, frmLavoratoriMagazzinoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, textAvg, 0, SpringLayout.EAST, lblDataNascita);
        textAvg.setColumns(10);
        frmLavoratoriMagazzinoGui.getContentPane().add(textAvg);

        JLabel lblMagAvg = new JLabel("Magazzino:");
        springLayout.putConstraint(SpringLayout.NORTH, lblMagAvg, 6, SpringLayout.SOUTH, lblSalMedio);
        springLayout.putConstraint(SpringLayout.WEST, textAvg, 6, SpringLayout.EAST, lblMagAvg);
        springLayout.putConstraint(SpringLayout.WEST, lblMagAvg, 0, SpringLayout.WEST, lblCodLavoratore);
        lblMagAvg.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblMagAvg);

        JLabel lblDelCodLav = new JLabel("Codice Lavoratore:");
        springLayout.putConstraint(SpringLayout.NORTH, lblDelCodLav, 610, SpringLayout.NORTH, frmLavoratoriMagazzinoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, lblDelLavMagCod, -6, SpringLayout.NORTH, lblDelCodLav);
        springLayout.putConstraint(SpringLayout.WEST, lblDelCodLav, 0, SpringLayout.WEST, lblCodLavoratore);
        lblDelCodLav.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblDelCodLav);

        textDelCodLav = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textDelCodLav, 0, SpringLayout.NORTH, lblDelCodLav);
        springLayout.putConstraint(SpringLayout.WEST, textDelCodLav, 0, SpringLayout.WEST, textCodLav);
        springLayout.putConstraint(SpringLayout.EAST, textDelCodLav, -1, SpringLayout.EAST, textCF);
        textDelCodLav.setColumns(10);
        frmLavoratoriMagazzinoGui.getContentPane().add(textDelCodLav);

        tableLavMag = new JTable() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        springLayout.putConstraint(SpringLayout.NORTH, tableLavMag, 6, SpringLayout.SOUTH, lblLavMag);
        springLayout.putConstraint(SpringLayout.WEST, tableLavMag, 0, SpringLayout.WEST, lblLavMag);
        springLayout.putConstraint(SpringLayout.SOUTH, tableLavMag, 0, SpringLayout.SOUTH, textSalario);
        springLayout.putConstraint(SpringLayout.EAST, tableLavMag, -82, SpringLayout.EAST, frmLavoratoriMagazzinoGui.getContentPane());
        JScrollPane scrollPaneLavMag = new JScrollPane(tableLavMag);
        springLayout.putConstraint(SpringLayout.NORTH, scrollPaneLavMag, 3, SpringLayout.SOUTH, lblLavMag);
        springLayout.putConstraint(SpringLayout.WEST, scrollPaneLavMag, 76, SpringLayout.EAST, textD_Nascita);
        frmLavoratoriMagazzinoGui.getContentPane().add(scrollPaneLavMag);

        JButton btnVisualizzaLavMag = new JButton("VISUALIZZA");
        springLayout.putConstraint(SpringLayout.SOUTH, scrollPaneLavMag, -12, SpringLayout.NORTH, btnVisualizzaLavMag);
        springLayout.putConstraint(SpringLayout.WEST, btnVisualizzaLavMag, 0, SpringLayout.WEST, lblLavMag);
        springLayout.putConstraint(SpringLayout.SOUTH, btnVisualizzaLavMag, 0, SpringLayout.SOUTH, lblCitt�);
        btnVisualizzaLavMag.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = opLavMag.getRS_LavMag();
                tableLavMag.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        frmLavoratoriMagazzinoGui.getContentPane().add(btnVisualizzaLavMag);

        JButton btnDelMag = new JButton("ELIMINA");
        btnDelMag.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                opLavMag.MassiveDelete(textDelMag.getText());
                ResultSet rS = opLavMag.getRS_LavMag();
                tableLavMag.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnDelMag, -1, SpringLayout.NORTH, lblMagDel);
        frmLavoratoriMagazzinoGui.getContentPane().add(btnDelMag);

        JButton btnDelCodLav = new JButton("ELIMINA");
        btnDelCodLav.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                opLavMag.DeleteLavMag(textDelCodLav.getText());
                ResultSet rS = opLavMag.getRS_LavMag();
                tableLavMag.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnDelCodLav, -1, SpringLayout.NORTH, lblDelCodLav);
        springLayout.putConstraint(SpringLayout.WEST, btnDelCodLav, 6, SpringLayout.EAST, textDelCodLav);
        frmLavoratoriMagazzinoGui.getContentPane().add(btnDelCodLav);

        JButton btnRicerca = new JButton("RICERCA");
        btnRicerca.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = opLavMag.getRS_LavOP(textRicerca.getText());
                tableLavMag.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnRicerca, -1, SpringLayout.NORTH, lblMagRicerca);
        springLayout.putConstraint(SpringLayout.EAST, btnRicerca, 0, SpringLayout.EAST, textCodLav);
        frmLavoratoriMagazzinoGui.getContentPane().add(btnRicerca);

        JButton btnSalMedio = new JButton("MOSTRA");
        btnSalMedio.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = opLavMag.getRS_MvgSalario(textAvg.getText());
                tableLavMag.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnSalMedio, 45, SpringLayout.SOUTH, btnRicerca);
        springLayout.putConstraint(SpringLayout.WEST, btnSalMedio, 0, SpringLayout.WEST, textD_Nascita);
        frmLavoratoriMagazzinoGui.getContentPane().add(btnSalMedio);

        JLabel lblMag = new JLabel("Magazzini:");
        springLayout.putConstraint(SpringLayout.NORTH, lblMag, 0, SpringLayout.NORTH, lblSalario);
        springLayout.putConstraint(SpringLayout.WEST, lblMag, 0, SpringLayout.WEST, lblLavMag);
        lblMag.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblMag);

        tableMag = new JTable() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        springLayout.putConstraint(SpringLayout.NORTH, tableMag, 9, SpringLayout.NORTH, lblDelLavMag);
        springLayout.putConstraint(SpringLayout.WEST, tableMag, 173, SpringLayout.EAST, lblDelLavMag);
        springLayout.putConstraint(SpringLayout.SOUTH, tableMag, 0, SpringLayout.SOUTH, lblDelLavMagCod);
        springLayout.putConstraint(SpringLayout.EAST, tableMag, 700, SpringLayout.EAST, lblDelLavMag);
        JScrollPane scrollPaneMag = new JScrollPane(tableMag);
        springLayout.putConstraint(SpringLayout.NORTH, scrollPaneMag, 6, SpringLayout.SOUTH, lblMag);
        springLayout.putConstraint(SpringLayout.WEST, scrollPaneMag, 0, SpringLayout.WEST, lblLavMag);
        springLayout.putConstraint(SpringLayout.EAST, scrollPaneMag, -258, SpringLayout.EAST, frmLavoratoriMagazzinoGui.getContentPane());
        frmLavoratoriMagazzinoGui.getContentPane().add(scrollPaneMag);

        JButton btnVisualizzaMag = new JButton("VISUALIZZA");
        springLayout.putConstraint(SpringLayout.SOUTH, scrollPaneMag, -12, SpringLayout.NORTH, btnVisualizzaMag);
        springLayout.putConstraint(SpringLayout.NORTH, btnVisualizzaMag, 0, SpringLayout.NORTH, lblSalMedio);
        springLayout.putConstraint(SpringLayout.WEST, btnVisualizzaMag, 0, SpringLayout.WEST, lblLavMag);
        btnVisualizzaMag.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    OperationMagazzino opMag = new OperationMagazzino();
                    ResultSet rS = opMag.getRS_Mag();
                    tableMag.setModel(DbUtils.resultSetToTableModel(rS));
                } catch (ClassNotFoundException | SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        frmLavoratoriMagazzinoGui.getContentPane().add(btnVisualizzaMag);

        JLabel lblInsertLav = new JLabel("Inserisci Lavoratore magazzino:");
        springLayout.putConstraint(SpringLayout.WEST, lblLavMag, 113, SpringLayout.EAST, lblInsertLav);
        springLayout.putConstraint(SpringLayout.WEST, lblNewLabel, 36, SpringLayout.EAST, lblInsertLav);
        springLayout.putConstraint(SpringLayout.EAST, rdbtnCapoTurno, 0, SpringLayout.EAST, lblInsertLav);
        springLayout.putConstraint(SpringLayout.WEST, lblInsertLav, 0, SpringLayout.WEST, lblCodLavoratore);
        springLayout.putConstraint(SpringLayout.SOUTH, lblInsertLav, -11, SpringLayout.NORTH, lblCodLavoratore);
        lblInsertLav.setFont(new Font("Tahoma", Font.BOLD, 20));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblInsertLav);

        textDelMag = new JTextField();
        springLayout.putConstraint(SpringLayout.WEST, btnDelMag, 6, SpringLayout.EAST, textDelMag);
        springLayout.putConstraint(SpringLayout.NORTH, textDelMag, 0, SpringLayout.NORTH, lblMagDel);
        springLayout.putConstraint(SpringLayout.WEST, textDelMag, 6, SpringLayout.EAST, lblMagDel);
        textDelMag.setColumns(10);
        frmLavoratoriMagazzinoGui.getContentPane().add(textDelMag);

        JLabel lblInserisciMagazzino = new JLabel("Inserisci Magazzino:");
        springLayout.putConstraint(SpringLayout.NORTH, lblInserisciMagazzino, 0, SpringLayout.NORTH, lblDelLavMag);
        lblInserisciMagazzino.setFont(new Font("Tahoma", Font.BOLD, 20));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblInserisciMagazzino);

        JButton btnDirigenteMag = new JButton("DIRIGENTE MAGAZZINO >>");
        springLayout.putConstraint(SpringLayout.WEST, btnDirigenteMag, 94, SpringLayout.EAST, lblNewLabel);
        springLayout.putConstraint(SpringLayout.EAST, btnDirigenteMag, -38, SpringLayout.EAST, frmLavoratoriMagazzinoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, scrollPaneLavMag, 0, SpringLayout.EAST, btnDirigenteMag);
        btnDirigenteMag.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DirigenteMagazzinoGUI dMag;
                try {
                    dMag = new DirigenteMagazzinoGUI();
                    dMag.run();
                } catch (ClassNotFoundException | SQLException e1) {
                    e1.printStackTrace();
                }

            }
        });
        btnDirigenteMag.setBackground(Color.ORANGE);
        springLayout.putConstraint(SpringLayout.NORTH, btnDirigenteMag, 23, SpringLayout.NORTH, lblNewLabel);
        frmLavoratoriMagazzinoGui.getContentPane().add(btnDirigenteMag);

        textInsertNewMagazzino = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textInsertNewMagazzino, 0, SpringLayout.NORTH, lblMagDel);
        textInsertNewMagazzino.setColumns(10);
        frmLavoratoriMagazzinoGui.getContentPane().add(textInsertNewMagazzino);

        JButton btnInsertNewMagazzino = new JButton("INSERISCI");
        btnInsertNewMagazzino.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Magazzino mag = new Magazzino(textInsertNewMagazzino.getText(), textInsertNewDimensione.getText());
                try {
                    opMag.insertMag(mag);
                    ResultSet rS = opMag.getRS_Mag();
                    tableMag.setModel(DbUtils.resultSetToTableModel(rS));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        springLayout.putConstraint(SpringLayout.EAST, btnInsertNewMagazzino, 0, SpringLayout.EAST,
                textInsertNewMagazzino);
        frmLavoratoriMagazzinoGui.getContentPane().add(btnInsertNewMagazzino);

        JLabel lblInsertNewMag = new JLabel("Codice Magazzino:");
        springLayout.putConstraint(SpringLayout.EAST, lblInsertNewMag, -213, SpringLayout.EAST, frmLavoratoriMagazzinoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, textInsertNewMagazzino, 6, SpringLayout.EAST, lblInsertNewMag);
        springLayout.putConstraint(SpringLayout.WEST, lblInserisciMagazzino, 0, SpringLayout.WEST, lblInsertNewMag);
        springLayout.putConstraint(SpringLayout.NORTH, lblInsertNewMag, 0, SpringLayout.NORTH, lblMagDel);
        lblInsertNewMag.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblInsertNewMag);

        JLabel lblInsertNewDimensione = new JLabel("Dimensione:");
        springLayout.putConstraint(SpringLayout.NORTH, lblInsertNewDimensione, 0, SpringLayout.NORTH, lblDelCodLav);
        lblInsertNewDimensione.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmLavoratoriMagazzinoGui.getContentPane().add(lblInsertNewDimensione);

        textInsertNewDimensione = new JTextField();
        springLayout.putConstraint(SpringLayout.WEST, textInsertNewDimensione, 1029, SpringLayout.WEST,
                frmLavoratoriMagazzinoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, lblInsertNewDimensione, -4, SpringLayout.WEST,
                textInsertNewDimensione);
        springLayout.putConstraint(SpringLayout.NORTH, btnInsertNewMagazzino, 11, SpringLayout.SOUTH,
                textInsertNewDimensione);
        springLayout.putConstraint(SpringLayout.NORTH, textInsertNewDimensione, 0, SpringLayout.NORTH, lblDelCodLav);
        frmLavoratoriMagazzinoGui.getContentPane().add(textInsertNewDimensione);
        textInsertNewDimensione.setColumns(10);

        JButton btnTurni = new JButton("TURNI >>");
        btnTurni.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TurnoGUI tGUI;
                try {
                    tGUI = new TurnoGUI();
                    tGUI.run();
                } catch (ClassNotFoundException | SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        btnTurni.setBackground(Color.GREEN);
        btnTurni.setForeground(SystemColor.windowText);
        springLayout.putConstraint(SpringLayout.NORTH, btnTurni, -108, SpringLayout.NORTH, lblInserisciMagazzino);
        springLayout.putConstraint(SpringLayout.WEST, btnTurni, 0, SpringLayout.WEST, textInsertNewMagazzino);
        springLayout.putConstraint(SpringLayout.SOUTH, btnTurni, 0, SpringLayout.SOUTH, lblRicercaOP);
        springLayout.putConstraint(SpringLayout.EAST, btnTurni, -38, SpringLayout.EAST, frmLavoratoriMagazzinoGui.getContentPane());
        frmLavoratoriMagazzinoGui.getContentPane().add(btnTurni);

    }

    // RUN
    public void run() {
        try {
            LavMagGUI window = new LavMagGUI();
            window.frmLavoratoriMagazzinoGui.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("serial")
    DefaultTableModel tableModel = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    };
    private JTextField textInsertNewDimensione;

}
