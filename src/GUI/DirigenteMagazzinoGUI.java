package GUI;

import javax.swing.JFrame;
import javax.swing.SpringLayout;

import Dirigenti.DirMag;
import Dirigenti.OperationDirMag;
import Magazzino.OperationMagazzino;
import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import javax.swing.JScrollPane;

import java.awt.Font;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DirigenteMagazzinoGUI {

    private JFrame frmDirigenteMagazzinoGui;
    private JTextField textCodLav;
    private JTextField textCF;
    private JTextField textNome;
    private JTextField textCognome;
    private JTextField textD_Nascita;
    private JTextField textTelefono;
    private JTextField textVia;
    private JTextField textCivico;
    private JTextField textCitt�;
    private JTextField textSalario;
    private JTextField textMagazzino;
    private JTable DirMagTable;
    private JTable tableMag;
    private JTextField textDelDirMag;

    private OperationDirMag oDirMag = new OperationDirMag();
    private OperationMagazzino oMag = new OperationMagazzino();

    private List<JTextField> textFieldsList = new ArrayList<JTextField>();
    private List<String> textFieldsString = new ArrayList<String>();

    /**
     * Create the application.
     */
    public DirigenteMagazzinoGUI() throws ClassNotFoundException, SQLException {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmDirigenteMagazzinoGui = new JFrame();
        frmDirigenteMagazzinoGui.setTitle("Dirigente Magazzino GUI");
        frmDirigenteMagazzinoGui.setAlwaysOnTop(true);
        frmDirigenteMagazzinoGui.setResizable(false);
        frmDirigenteMagazzinoGui.setBounds(100, 100, 1240, 710);
        frmDirigenteMagazzinoGui.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        SpringLayout springLayout = new SpringLayout();
        frmDirigenteMagazzinoGui.getContentPane().setLayout(springLayout);

        JLabel lblTitle = new JLabel("DIRIGENTE MAGAZZINO");
        lblTitle.setForeground(Color.DARK_GRAY);
        springLayout.putConstraint(SpringLayout.NORTH, lblTitle, 10, SpringLayout.NORTH, frmDirigenteMagazzinoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, lblTitle, -360, SpringLayout.EAST, frmDirigenteMagazzinoGui.getContentPane());
        lblTitle.setFont(new Font("Tahoma", Font.BOLD, 40));
        frmDirigenteMagazzinoGui.getContentPane().add(lblTitle);

        JLabel lblSubTitle = new JLabel("Inserisci Dirigente Magazzino:");
        lblSubTitle.setFont(new Font("Tahoma", Font.BOLD, 20));
        springLayout.putConstraint(SpringLayout.NORTH, lblSubTitle, 72, SpringLayout.NORTH, frmDirigenteMagazzinoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblSubTitle, 10, SpringLayout.WEST, frmDirigenteMagazzinoGui.getContentPane());
        frmDirigenteMagazzinoGui.getContentPane().add(lblSubTitle);

        JLabel lblCodLav = new JLabel("Codice Lavoratore:");
        lblCodLav.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblCodLav, 16, SpringLayout.SOUTH, lblSubTitle);
        springLayout.putConstraint(SpringLayout.WEST, lblCodLav, 10, SpringLayout.WEST, frmDirigenteMagazzinoGui.getContentPane());
        frmDirigenteMagazzinoGui.getContentPane().add(lblCodLav);

        textCodLav = new JTextField();
        textFieldsList.add(textCodLav);
        springLayout.putConstraint(SpringLayout.NORTH, textCodLav, 13, SpringLayout.SOUTH, lblSubTitle);
        springLayout.putConstraint(SpringLayout.WEST, textCodLav, 6, SpringLayout.EAST, lblCodLav);
        springLayout.putConstraint(SpringLayout.EAST, textCodLav, 137, SpringLayout.EAST, lblCodLav);
        frmDirigenteMagazzinoGui.getContentPane().add(textCodLav);
        textCodLav.setColumns(10);

        JLabel lblCF = new JLabel("Codice Fiscale:");
        lblCF.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblCF, 25, SpringLayout.SOUTH, lblCodLav);
        springLayout.putConstraint(SpringLayout.WEST, lblCF, 0, SpringLayout.WEST, lblSubTitle);
        frmDirigenteMagazzinoGui.getContentPane().add(lblCF);

        textCF = new JTextField();
        textFieldsList.add(textCF);
        springLayout.putConstraint(SpringLayout.NORTH, textCF, 22, SpringLayout.SOUTH, lblCodLav);
        springLayout.putConstraint(SpringLayout.WEST, textCF, 6, SpringLayout.EAST, lblCF);
        frmDirigenteMagazzinoGui.getContentPane().add(textCF);
        textCF.setColumns(10);

        JLabel lblNome = new JLabel("Nome:");
        lblNome.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblNome, 20, SpringLayout.SOUTH, lblCF);
        springLayout.putConstraint(SpringLayout.WEST, lblNome, 0, SpringLayout.WEST, lblSubTitle);
        frmDirigenteMagazzinoGui.getContentPane().add(lblNome);

        textNome = new JTextField();
        textFieldsList.add(textNome);
        springLayout.putConstraint(SpringLayout.NORTH, textNome, 20, SpringLayout.SOUTH, lblCF);
        springLayout.putConstraint(SpringLayout.WEST, textNome, 6, SpringLayout.EAST, lblNome);
        frmDirigenteMagazzinoGui.getContentPane().add(textNome);
        textNome.setColumns(10);

        JLabel lblCognome = new JLabel("Cognome:");
        springLayout.putConstraint(SpringLayout.EAST, textNome, -12, SpringLayout.WEST, lblCognome);
        springLayout.putConstraint(SpringLayout.WEST, lblCognome, 173, SpringLayout.WEST, frmDirigenteMagazzinoGui.getContentPane());
        lblCognome.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblCognome, 0, SpringLayout.NORTH, lblNome);
        frmDirigenteMagazzinoGui.getContentPane().add(lblCognome);

        textCognome = new JTextField();
        textFieldsList.add(textCognome);
        springLayout.putConstraint(SpringLayout.NORTH, textCognome, 62, SpringLayout.SOUTH, textCodLav);
        springLayout.putConstraint(SpringLayout.WEST, textCognome, 6, SpringLayout.EAST, lblCognome);
        springLayout.putConstraint(SpringLayout.EAST, textCognome, 105, SpringLayout.EAST, lblCognome);
        frmDirigenteMagazzinoGui.getContentPane().add(textCognome);
        textCognome.setColumns(10);

        JLabel lblD_Nascita = new JLabel("Data Nascita (AAAA-MM-GG):");
        springLayout.putConstraint(SpringLayout.NORTH, lblD_Nascita, 19, SpringLayout.SOUTH, textNome);
        springLayout.putConstraint(SpringLayout.WEST, lblD_Nascita, 0, SpringLayout.WEST, lblSubTitle);
        lblD_Nascita.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmDirigenteMagazzinoGui.getContentPane().add(lblD_Nascita);

        textD_Nascita = new JTextField();
        textFieldsList.add(textD_Nascita);
        springLayout.putConstraint(SpringLayout.NORTH, textD_Nascita, 0, SpringLayout.NORTH, lblD_Nascita);
        springLayout.putConstraint(SpringLayout.WEST, textD_Nascita, 6, SpringLayout.EAST, lblD_Nascita);
        springLayout.putConstraint(SpringLayout.SOUTH, textD_Nascita, 3, SpringLayout.SOUTH, lblD_Nascita);
        springLayout.putConstraint(SpringLayout.EAST, textD_Nascita, 0, SpringLayout.EAST, textCognome);
        frmDirigenteMagazzinoGui.getContentPane().add(textD_Nascita);
        textD_Nascita.setColumns(10);

        JLabel lblTelefono = new JLabel("Telefono:");
        springLayout.putConstraint(SpringLayout.NORTH, lblTelefono, 22, SpringLayout.SOUTH, lblD_Nascita);
        springLayout.putConstraint(SpringLayout.WEST, lblTelefono, 10, SpringLayout.WEST, frmDirigenteMagazzinoGui.getContentPane());
        lblTelefono.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmDirigenteMagazzinoGui.getContentPane().add(lblTelefono);

        textTelefono = new JTextField();
        textFieldsList.add(textTelefono);
        springLayout.putConstraint(SpringLayout.NORTH, textTelefono, 22, SpringLayout.SOUTH, lblD_Nascita);
        springLayout.putConstraint(SpringLayout.WEST, textTelefono, 6, SpringLayout.EAST, lblTelefono);
        springLayout.putConstraint(SpringLayout.EAST, textTelefono, 128, SpringLayout.EAST, lblTelefono);
        frmDirigenteMagazzinoGui.getContentPane().add(textTelefono);
        textTelefono.setColumns(10);

        JLabel lblVia = new JLabel("Via:");
        lblVia.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblVia, 21, SpringLayout.SOUTH, lblTelefono);
        springLayout.putConstraint(SpringLayout.WEST, lblVia, 0, SpringLayout.WEST, lblSubTitle);
        frmDirigenteMagazzinoGui.getContentPane().add(lblVia);

        textVia = new JTextField();
        textFieldsList.add(textVia);
        springLayout.putConstraint(SpringLayout.NORTH, textVia, 15, SpringLayout.SOUTH, textTelefono);
        springLayout.putConstraint(SpringLayout.WEST, textVia, 6, SpringLayout.EAST, lblVia);
        springLayout.putConstraint(SpringLayout.EAST, textVia, 0, SpringLayout.EAST, textTelefono);
        frmDirigenteMagazzinoGui.getContentPane().add(textVia);
        textVia.setColumns(10);

        JLabel lblCivico = new JLabel("Civico:");
        springLayout.putConstraint(SpringLayout.EAST, textCF, 0, SpringLayout.EAST, lblCivico);
        lblCivico.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.WEST, lblCivico, 0, SpringLayout.WEST, textD_Nascita);
        springLayout.putConstraint(SpringLayout.SOUTH, lblCivico, 0, SpringLayout.SOUTH, lblVia);
        frmDirigenteMagazzinoGui.getContentPane().add(lblCivico);

        textCivico = new JTextField();
        textFieldsList.add(textCivico);
        springLayout.putConstraint(SpringLayout.NORTH, textCivico, 54, SpringLayout.SOUTH, textD_Nascita);
        springLayout.putConstraint(SpringLayout.WEST, textCivico, 6, SpringLayout.EAST, lblCivico);
        springLayout.putConstraint(SpringLayout.EAST, textCivico, 62, SpringLayout.EAST, lblCivico);
        frmDirigenteMagazzinoGui.getContentPane().add(textCivico);
        textCivico.setColumns(10);

        JLabel lblCitt� = new JLabel("Citt\u00E0:");
        lblCitt�.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblCitt�, 21, SpringLayout.SOUTH, lblVia);
        springLayout.putConstraint(SpringLayout.WEST, lblCitt�, 0, SpringLayout.WEST, lblSubTitle);
        frmDirigenteMagazzinoGui.getContentPane().add(lblCitt�);

        textCitt� = new JTextField();
        textFieldsList.add(textCitt�);
        springLayout.putConstraint(SpringLayout.NORTH, textCitt�, 21, SpringLayout.SOUTH, textVia);
        springLayout.putConstraint(SpringLayout.WEST, textCitt�, 7, SpringLayout.EAST, lblCitt�);
        springLayout.putConstraint(SpringLayout.EAST, textCitt�, 20, SpringLayout.EAST, textTelefono);
        frmDirigenteMagazzinoGui.getContentPane().add(textCitt�);
        textCitt�.setColumns(10);

        JLabel lblSalario = new JLabel("Salario:");
        lblSalario.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblSalario, 24, SpringLayout.SOUTH, textCitt�);
        springLayout.putConstraint(SpringLayout.WEST, lblSalario, 0, SpringLayout.WEST, lblSubTitle);
        frmDirigenteMagazzinoGui.getContentPane().add(lblSalario);

        textSalario = new JTextField();
        textFieldsList.add(textSalario);
        springLayout.putConstraint(SpringLayout.NORTH, textSalario, 0, SpringLayout.NORTH, lblSalario);
        springLayout.putConstraint(SpringLayout.WEST, textSalario, 0, SpringLayout.WEST, textNome);
        frmDirigenteMagazzinoGui.getContentPane().add(textSalario);
        textSalario.setColumns(10);

        JLabel lblMagazzino = new JLabel("Magazzino:");
        springLayout.putConstraint(SpringLayout.NORTH, lblMagazzino, 0, SpringLayout.NORTH, lblSalario);
        lblMagazzino.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmDirigenteMagazzinoGui.getContentPane().add(lblMagazzino);

        textMagazzino = new JTextField();
        textFieldsList.add(textMagazzino);
        springLayout.putConstraint(SpringLayout.WEST, textMagazzino, 278, SpringLayout.WEST, frmDirigenteMagazzinoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, lblMagazzino, -10, SpringLayout.WEST, textMagazzino);
        springLayout.putConstraint(SpringLayout.NORTH, textMagazzino, 0, SpringLayout.NORTH, lblSalario);
        frmDirigenteMagazzinoGui.getContentPane().add(textMagazzino);
        textMagazzino.setColumns(10);

        JButton btnInserisci = new JButton("INSERISCI");
        btnInserisci.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (JTextField jTextField : textFieldsList) {
                    if (jTextField.getText().length() == 0) {
                        textFieldsString.add(null);
                    } else {
                        textFieldsString.add(jTextField.getText());
                    }
                }

                DirMag dMag = new DirMag(textFieldsString);
                try {
                    boolean check = oDirMag.insertDirMag(dMag);
                    if (!check) {
                        textFieldsString.clear();
                    }else {
                        ResultSet rS = oDirMag.getRS_DirMag();
                        DirMagTable.setModel(DbUtils.resultSetToTableModel(rS));
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                textFieldsString.clear();
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnInserisci, 19, SpringLayout.SOUTH, textSalario);
        springLayout.putConstraint(SpringLayout.WEST, btnInserisci, 10, SpringLayout.WEST, frmDirigenteMagazzinoGui.getContentPane());
        frmDirigenteMagazzinoGui.getContentPane().add(btnInserisci);

        JButton btnReset = new JButton("RESET");
        springLayout.putConstraint(SpringLayout.WEST, btnReset, 22, SpringLayout.EAST, btnInserisci);
        btnReset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (JTextField jTextField : textFieldsList) {
                    jTextField.setText("");
                }
                textFieldsString.clear();
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnReset, 0, SpringLayout.NORTH, btnInserisci);
        frmDirigenteMagazzinoGui.getContentPane().add(btnReset);

        JLabel lblDirMagTable = new JLabel("Dirigente Magazzino:");
        lblDirMagTable.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.WEST, lblDirMagTable, 181, SpringLayout.EAST, lblSubTitle);
        springLayout.putConstraint(SpringLayout.SOUTH, lblDirMagTable, 0, SpringLayout.SOUTH, lblSubTitle);
        frmDirigenteMagazzinoGui.getContentPane().add(lblDirMagTable);

        DirMagTable = new JTable() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        JScrollPane scrollPaneDirMag = new JScrollPane(DirMagTable);
        springLayout.putConstraint(SpringLayout.NORTH, scrollPaneDirMag, -168, SpringLayout.NORTH, lblTelefono);
        springLayout.putConstraint(SpringLayout.WEST, scrollPaneDirMag, 149, SpringLayout.EAST, textCognome);
        springLayout.putConstraint(SpringLayout.EAST, scrollPaneDirMag, -23, SpringLayout.EAST, frmDirigenteMagazzinoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, DirMagTable, 16, SpringLayout.SOUTH, lblDirMagTable);
        springLayout.putConstraint(SpringLayout.WEST, DirMagTable, 149, SpringLayout.EAST, textCognome);
        springLayout.putConstraint(SpringLayout.SOUTH, DirMagTable, 219, SpringLayout.SOUTH, lblDirMagTable);
        springLayout.putConstraint(SpringLayout.EAST, DirMagTable, 719, SpringLayout.EAST, textCognome);
        frmDirigenteMagazzinoGui.getContentPane().add(scrollPaneDirMag);

        JLabel lblMagazzinoTable = new JLabel("Magazzino:");
        springLayout.putConstraint(SpringLayout.NORTH, lblMagazzinoTable, 57, SpringLayout.SOUTH, DirMagTable);
        springLayout.putConstraint(SpringLayout.WEST, lblMagazzinoTable, 0, SpringLayout.WEST, lblDirMagTable);
        lblMagazzinoTable.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmDirigenteMagazzinoGui.getContentPane().add(lblMagazzinoTable);

        JButton btnVisualizzaDirMag = new JButton("VISUALIZZA");
        btnVisualizzaDirMag.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                ResultSet rS = oDirMag.getRS_DirMag();
                DirMagTable.setModel(DbUtils.resultSetToTableModel(rS));

            }
        });
        springLayout.putConstraint(SpringLayout.SOUTH, scrollPaneDirMag, -6, SpringLayout.NORTH, btnVisualizzaDirMag);
        springLayout.putConstraint(SpringLayout.NORTH, btnVisualizzaDirMag, 6, SpringLayout.SOUTH, DirMagTable);
        springLayout.putConstraint(SpringLayout.WEST, btnVisualizzaDirMag, 0, SpringLayout.WEST, lblDirMagTable);
        frmDirigenteMagazzinoGui.getContentPane().add(btnVisualizzaDirMag);

        tableMag = new JTable() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        JScrollPane scrollPaneTableMag = new JScrollPane(tableMag);
        springLayout.putConstraint(SpringLayout.NORTH, scrollPaneTableMag, 6, SpringLayout.SOUTH, lblMagazzinoTable);
        springLayout.putConstraint(SpringLayout.WEST, scrollPaneTableMag, 0, SpringLayout.WEST, lblDirMagTable);
        springLayout.putConstraint(SpringLayout.EAST, scrollPaneTableMag, 0, SpringLayout.EAST, scrollPaneDirMag);
        springLayout.putConstraint(SpringLayout.NORTH, tableMag, 6, SpringLayout.SOUTH, lblMagazzinoTable);
        springLayout.putConstraint(SpringLayout.WEST, tableMag, 162, SpringLayout.EAST, textMagazzino);
        springLayout.putConstraint(SpringLayout.SOUTH, tableMag, 189, SpringLayout.SOUTH, lblMagazzinoTable);
        springLayout.putConstraint(SpringLayout.EAST, tableMag, 732, SpringLayout.EAST, textMagazzino);
        frmDirigenteMagazzinoGui.getContentPane().add(scrollPaneTableMag);

        JButton btnVisualizzaMag = new JButton("VISUALIZZA");
        btnVisualizzaMag.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = oMag.getRS_Mag();
                tableMag.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.SOUTH, scrollPaneTableMag, -6, SpringLayout.NORTH, btnVisualizzaMag);
        springLayout.putConstraint(SpringLayout.NORTH, btnVisualizzaMag, 6, SpringLayout.SOUTH, tableMag);
        springLayout.putConstraint(SpringLayout.EAST, btnVisualizzaMag, 0, SpringLayout.EAST, btnVisualizzaDirMag);
        frmDirigenteMagazzinoGui.getContentPane().add(btnVisualizzaMag);

        JLabel lblDelDirMag = new JLabel("Elimina Dirigente Magazzino:");
        lblDelDirMag.setFont(new Font("Tahoma", Font.BOLD, 20));
        springLayout.putConstraint(SpringLayout.NORTH, lblDelDirMag, 40, SpringLayout.SOUTH, btnInserisci);
        springLayout.putConstraint(SpringLayout.WEST, lblDelDirMag, 0, SpringLayout.WEST, lblSubTitle);
        frmDirigenteMagazzinoGui.getContentPane().add(lblDelDirMag);

        textDelDirMag = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textDelDirMag, 6, SpringLayout.SOUTH, lblDelDirMag);
        springLayout.putConstraint(SpringLayout.WEST, textDelDirMag, 0, SpringLayout.WEST, textCodLav);
        springLayout.putConstraint(SpringLayout.EAST, textDelDirMag, 0, SpringLayout.EAST, lblCivico);
        frmDirigenteMagazzinoGui.getContentPane().add(textDelDirMag);
        textDelDirMag.setColumns(10);

        JLabel lblDelCodLav = new JLabel("Codice Lavoratore:");
        springLayout.putConstraint(SpringLayout.NORTH, lblDelCodLav, 6, SpringLayout.SOUTH, lblDelDirMag);
        springLayout.putConstraint(SpringLayout.WEST, lblDelCodLav, 0, SpringLayout.WEST, lblSubTitle);
        lblDelCodLav.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmDirigenteMagazzinoGui.getContentPane().add(lblDelCodLav);

        JButton btnDelete = new JButton("ELIMINA");
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                oDirMag.DeleteDirMag(textDelDirMag.getText());
                ResultSet rS = oDirMag.getRS_DirMag();
                DirMagTable.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnDelete, 6, SpringLayout.SOUTH, lblDelDirMag);
        springLayout.putConstraint(SpringLayout.WEST, btnDelete, 0, SpringLayout.WEST, textCivico);
        frmDirigenteMagazzinoGui.getContentPane().add(btnDelete);
    }

    public void run() {
        try {
            DirigenteMagazzinoGUI window = new DirigenteMagazzinoGUI();
            window.frmDirigenteMagazzinoGui.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
