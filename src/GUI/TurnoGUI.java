package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SpringLayout;

import Turno.OperationTurno;
import Turno.Turno;
import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import java.awt.Font;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TurnoGUI {

    private JFrame frmTurnoGui;
    private JTextField textGiorno;
    private JTextField textCodCapoTurno;
    private JTable tableTurno;
    private JTextField textNumero;
    private JTextField textAggNumero;
    private JTextField textNewCapoTurno;

    private List<JTextField> fieldsList = new ArrayList<JTextField>();
    private List<String> stringsfieldList = new ArrayList<String>();
    private OperationTurno opTurno = new OperationTurno();

    /**
     * Create the application.
     */
    public TurnoGUI() throws SQLException, ClassNotFoundException {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmTurnoGui = new JFrame();
        frmTurnoGui.setTitle("Turno GUI");
        frmTurnoGui.setResizable(false);
        frmTurnoGui.setAlwaysOnTop(true);
        frmTurnoGui.setBounds(100, 100, 975, 590);
        frmTurnoGui.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        SpringLayout springLayout = new SpringLayout();
        frmTurnoGui.getContentPane().setLayout(springLayout);

        JLabel lblTitle = new JLabel("TURNO");
        springLayout.putConstraint(SpringLayout.NORTH, lblTitle, 10, SpringLayout.NORTH, frmTurnoGui.getContentPane());
        lblTitle.setFont(new Font("Tahoma", Font.BOLD, 40));
        frmTurnoGui.getContentPane().add(lblTitle);

        JLabel lblInsertTurno = new JLabel("Inserisci Turno:");
        lblInsertTurno.setFont(new Font("Tahoma", Font.BOLD, 20));
        springLayout.putConstraint(SpringLayout.NORTH, lblInsertTurno, 73, SpringLayout.NORTH, frmTurnoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblInsertTurno, 10, SpringLayout.WEST, frmTurnoGui.getContentPane());
        frmTurnoGui.getContentPane().add(lblInsertTurno);

        JLabel lblGiorno = new JLabel("Giorno:");
        lblGiorno.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblGiorno, 15, SpringLayout.SOUTH, lblInsertTurno);
        springLayout.putConstraint(SpringLayout.WEST, lblGiorno, 0, SpringLayout.WEST, lblInsertTurno);
        frmTurnoGui.getContentPane().add(lblGiorno);

        JLabel lblFasciaOraria = new JLabel("Fascia Oraria:");
        lblFasciaOraria.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblFasciaOraria, 20, SpringLayout.SOUTH, lblGiorno);
        springLayout.putConstraint(SpringLayout.WEST, lblFasciaOraria, 0, SpringLayout.WEST, lblInsertTurno);
        frmTurnoGui.getContentPane().add(lblFasciaOraria);

        JLabel lblCodCapoTurno = new JLabel("Codice Capo Turno:");
        lblCodCapoTurno.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblCodCapoTurno, 23, SpringLayout.SOUTH, lblFasciaOraria);
        springLayout.putConstraint(SpringLayout.WEST, lblCodCapoTurno, 0, SpringLayout.WEST, lblInsertTurno);
        frmTurnoGui.getContentPane().add(lblCodCapoTurno);

        textGiorno = new JTextField();
        fieldsList.add(textGiorno);
        springLayout.putConstraint(SpringLayout.NORTH, textGiorno, 0, SpringLayout.NORTH, lblGiorno);
        springLayout.putConstraint(SpringLayout.WEST, textGiorno, 6, SpringLayout.EAST, lblGiorno);
        frmTurnoGui.getContentPane().add(textGiorno);
        textGiorno.setColumns(10);

        JRadioButton rdbtnFasciaPomeriggio = new JRadioButton("13:30 - 21:30");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnFasciaPomeriggio, -1, SpringLayout.NORTH, lblFasciaOraria);
        rdbtnFasciaPomeriggio.setActionCommand("13:30 - 21:30");
        frmTurnoGui.getContentPane().add(rdbtnFasciaPomeriggio);

        JRadioButton rdbtnFasciaMattina = new JRadioButton("5:30 - 13:30");
        rdbtnFasciaMattina.setActionCommand("5:30 - 13:30");
        springLayout.putConstraint(SpringLayout.NORTH, rdbtnFasciaMattina, -1, SpringLayout.NORTH, lblFasciaOraria);
        springLayout.putConstraint(SpringLayout.WEST, rdbtnFasciaMattina, 6, SpringLayout.EAST, lblFasciaOraria);
        frmTurnoGui.getContentPane().add(rdbtnFasciaMattina);

        ButtonGroup group = new ButtonGroup();
        group.add(rdbtnFasciaMattina);
        group.add(rdbtnFasciaPomeriggio);

        textCodCapoTurno = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textCodCapoTurno, 18, SpringLayout.SOUTH, rdbtnFasciaPomeriggio);
        fieldsList.add(textCodCapoTurno);
        springLayout.putConstraint(SpringLayout.WEST, textCodCapoTurno, 6, SpringLayout.EAST, lblCodCapoTurno);
        springLayout.putConstraint(SpringLayout.EAST, textCodCapoTurno, 127, SpringLayout.EAST, lblCodCapoTurno);
        frmTurnoGui.getContentPane().add(textCodCapoTurno);
        textCodCapoTurno.setColumns(10);

        JButton btnInserisci = new JButton("INSERISCI");
        btnInserisci.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (JTextField jTextField : fieldsList) {
                    if (jTextField.getText().length() == 0) {
                        stringsfieldList.add(null);
                    } else {
                        stringsfieldList.add(jTextField.getText());
                    }
                }
                stringsfieldList.add(group.getSelection().getActionCommand());
                Turno t = new Turno(stringsfieldList.get(0), stringsfieldList.get(1), stringsfieldList.get(2));
                try {
                    boolean check = opTurno.insertTurno(t);
                    if (!check) {
                        stringsfieldList.clear();
                    }else {
                        ResultSet rS = opTurno.getRS_Turno();
                        tableTurno.setModel(DbUtils.resultSetToTableModel(rS));
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                stringsfieldList.clear();
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnInserisci, 25, SpringLayout.SOUTH, lblCodCapoTurno);
        springLayout.putConstraint(SpringLayout.WEST, btnInserisci, 0, SpringLayout.WEST, lblInsertTurno);
        frmTurnoGui.getContentPane().add(btnInserisci);

        JButton btnReset = new JButton("RESET");
        btnReset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (JTextField jTextField : fieldsList) {
                    jTextField.setText("");
                }
                stringsfieldList.clear();
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnReset, 0, SpringLayout.NORTH, btnInserisci);
        springLayout.putConstraint(SpringLayout.EAST, btnReset, 0, SpringLayout.EAST, lblInsertTurno);
        frmTurnoGui.getContentPane().add(btnReset);

        JLabel lblTurnoTable = new JLabel("Turno:");
        springLayout.putConstraint(SpringLayout.WEST, lblTitle, 0, SpringLayout.WEST, lblTurnoTable);
        springLayout.putConstraint(SpringLayout.NORTH, lblTurnoTable, 6, SpringLayout.NORTH, lblInsertTurno);
        lblTurnoTable.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmTurnoGui.getContentPane().add(lblTurnoTable);

        tableTurno = new JTable() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        JScrollPane scrollTurno = new JScrollPane(tableTurno);
        springLayout.putConstraint(SpringLayout.NORTH, scrollTurno, 6, SpringLayout.SOUTH, lblTurnoTable);
        springLayout.putConstraint(SpringLayout.WEST, scrollTurno, 0, SpringLayout.WEST, lblTitle);
        springLayout.putConstraint(SpringLayout.EAST, rdbtnFasciaPomeriggio, -60, SpringLayout.WEST, tableTurno);
        springLayout.putConstraint(SpringLayout.NORTH, tableTurno, 20, SpringLayout.SOUTH, lblTurnoTable);
        springLayout.putConstraint(SpringLayout.WEST, tableTurno, 90, SpringLayout.EAST, textCodCapoTurno);
        springLayout.putConstraint(SpringLayout.SOUTH, tableTurno, -239, SpringLayout.SOUTH, frmTurnoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, tableTurno, -120, SpringLayout.EAST, frmTurnoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblTurnoTable, 0, SpringLayout.WEST, tableTurno);
        frmTurnoGui.getContentPane().add(scrollTurno);

        JLabel lblVisTurno = new JLabel("Visualizzazione Turno:");
        springLayout.putConstraint(SpringLayout.SOUTH, scrollTurno, 0, SpringLayout.SOUTH, lblVisTurno);
        lblVisTurno.setFont(new Font("Tahoma", Font.BOLD, 20));
        springLayout.putConstraint(SpringLayout.NORTH, lblVisTurno, 41, SpringLayout.SOUTH, btnInserisci);
        springLayout.putConstraint(SpringLayout.WEST, lblVisTurno, 0, SpringLayout.WEST, lblInsertTurno);
        frmTurnoGui.getContentPane().add(lblVisTurno);

        JLabel lblNumero = new JLabel("Numero:");
        lblNumero.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblNumero, 11, SpringLayout.SOUTH, lblVisTurno);
        springLayout.putConstraint(SpringLayout.WEST, lblNumero, 0, SpringLayout.WEST, lblInsertTurno);
        frmTurnoGui.getContentPane().add(lblNumero);

        textNumero = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textNumero, 0, SpringLayout.NORTH, lblNumero);
        springLayout.putConstraint(SpringLayout.WEST, textNumero, 6, SpringLayout.EAST, lblNumero);
        frmTurnoGui.getContentPane().add(textNumero);
        textNumero.setColumns(10);

        JButton btnVisTurno = new JButton("VISUALIZZA");
        btnVisTurno.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = opTurno.getRS_turno(textNumero.getText());
                tableTurno.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnVisTurno, -1, SpringLayout.NORTH, lblNumero);
        springLayout.putConstraint(SpringLayout.WEST, btnVisTurno, 9, SpringLayout.EAST, textNumero);
        frmTurnoGui.getContentPane().add(btnVisTurno);

        JLabel lblAggTurno = new JLabel("Aggiornamento Turno:");
        lblAggTurno.setFont(new Font("Tahoma", Font.BOLD, 20));
        springLayout.putConstraint(SpringLayout.NORTH, lblAggTurno, 39, SpringLayout.SOUTH, lblNumero);
        springLayout.putConstraint(SpringLayout.WEST, lblAggTurno, 0, SpringLayout.WEST, lblInsertTurno);
        frmTurnoGui.getContentPane().add(lblAggTurno);

        JLabel lblNumeroAgg = new JLabel("Numero:");
        springLayout.putConstraint(SpringLayout.NORTH, lblNumeroAgg, 6, SpringLayout.SOUTH, lblAggTurno);
        springLayout.putConstraint(SpringLayout.WEST, lblNumeroAgg, 0, SpringLayout.WEST, lblInsertTurno);
        lblNumeroAgg.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmTurnoGui.getContentPane().add(lblNumeroAgg);

        textAggNumero = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textAggNumero, 6, SpringLayout.SOUTH, lblAggTurno);
        springLayout.putConstraint(SpringLayout.WEST, textAggNumero, 0, SpringLayout.WEST, textNumero);
        frmTurnoGui.getContentPane().add(textAggNumero);
        textAggNumero.setColumns(10);

        JLabel lblNewCapoTurno = new JLabel("Nuovo Capo Turno:");
        lblNewCapoTurno.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblNewCapoTurno, 15, SpringLayout.SOUTH, lblNumeroAgg);
        springLayout.putConstraint(SpringLayout.WEST, lblNewCapoTurno, 0, SpringLayout.WEST, lblInsertTurno);
        frmTurnoGui.getContentPane().add(lblNewCapoTurno);

        textNewCapoTurno = new JTextField();
        springLayout.putConstraint(SpringLayout.WEST, textNewCapoTurno, 0, SpringLayout.WEST, textCodCapoTurno);
        springLayout.putConstraint(SpringLayout.SOUTH, textNewCapoTurno, 0, SpringLayout.SOUTH, lblNewCapoTurno);
        frmTurnoGui.getContentPane().add(textNewCapoTurno);
        textNewCapoTurno.setColumns(10);

        JButton btnUpdate = new JButton("AGGIORNA");
        btnUpdate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    opTurno.updateTurno(textAggNumero.getText(), textNewCapoTurno.getText());
                    ResultSet rS = opTurno.getRS_Turno();
                    tableTurno.setModel(DbUtils.resultSetToTableModel(rS));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnUpdate, 6, SpringLayout.SOUTH, lblNewCapoTurno);
        springLayout.putConstraint(SpringLayout.WEST, btnUpdate, 0, SpringLayout.WEST, lblInsertTurno);
        frmTurnoGui.getContentPane().add(btnUpdate);

        JButton btnVisTurnoTable = new JButton("VISUALIZZA");
        btnVisTurnoTable.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = opTurno.getRS_Turno();
                tableTurno.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.EAST, scrollTurno, 408, SpringLayout.EAST, btnVisTurnoTable);
        springLayout.putConstraint(SpringLayout.NORTH, btnVisTurnoTable, 0, SpringLayout.NORTH, lblNumero);
        springLayout.putConstraint(SpringLayout.WEST, btnVisTurnoTable, 0, SpringLayout.WEST, lblTitle);
        frmTurnoGui.getContentPane().add(btnVisTurnoTable);

    }

    public void run() {
        try {
            TurnoGUI window = new TurnoGUI();
            window.frmTurnoGui.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
