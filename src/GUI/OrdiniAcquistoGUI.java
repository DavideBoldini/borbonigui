package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SpringLayout;

import Ordine.OperationOrdini;
import Ordine.Ordine;
import PezzoAcquistato.OperationPAcq;
import PezzoAcquistato.PAcquistato;
import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import javax.swing.JScrollPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class OrdiniAcquistoGUI {

    private JFrame frmOrdiniAcquistoGui;
    private JTextField textCodOrdine;
    private JTextField textAnno;
    private JTextField textQuantit�TOT;
    private JTextField textImporto;
    private JTextField textCodUffAcq;
    private JTable tableOrdAcq;
    private JTable tablePezziAcq;
    private JTextField textVisCodAcq;
    private JTextField textAddPezzoOrd;
    private JTextField textQuantit�Pezzo;
    
    private OperationOrdini oOrd = new OperationOrdini();
    private OperationPAcq oPAcq = new OperationPAcq();

    /**
     * Create the application.
     */
    public OrdiniAcquistoGUI() throws SQLException, ClassNotFoundException{
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmOrdiniAcquistoGui = new JFrame();
        frmOrdiniAcquistoGui.setTitle("Ordini Acquisto GUI");
        frmOrdiniAcquistoGui.setResizable(false);
        frmOrdiniAcquistoGui.setBounds(100, 100, 1240, 710);
        frmOrdiniAcquistoGui.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        SpringLayout springLayout = new SpringLayout();
        frmOrdiniAcquistoGui.getContentPane().setLayout(springLayout);
        
        JLabel lblTitle = new JLabel("ORDINI ACQUISTO");
        springLayout.putConstraint(SpringLayout.NORTH, lblTitle, 10, SpringLayout.NORTH, frmOrdiniAcquistoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, lblTitle, -416, SpringLayout.EAST, frmOrdiniAcquistoGui.getContentPane());
        lblTitle.setFont(new Font("Tahoma", Font.BOLD, 40));
        frmOrdiniAcquistoGui.getContentPane().add(lblTitle);
        
        JLabel lblSubTitle = new JLabel("Crea Ordine Acquisto:");
        lblSubTitle.setFont(new Font("Tahoma", Font.BOLD, 20));
        springLayout.putConstraint(SpringLayout.NORTH, lblSubTitle, 74, SpringLayout.NORTH, frmOrdiniAcquistoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, lblSubTitle, 10, SpringLayout.WEST, frmOrdiniAcquistoGui.getContentPane());
        frmOrdiniAcquistoGui.getContentPane().add(lblSubTitle);
        
        JLabel lblCodOrdine = new JLabel("Codice Ordine:");
        lblCodOrdine.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblCodOrdine, 10, SpringLayout.SOUTH, lblSubTitle);
        springLayout.putConstraint(SpringLayout.WEST, lblCodOrdine, 0, SpringLayout.WEST, lblSubTitle);
        frmOrdiniAcquistoGui.getContentPane().add(lblCodOrdine);
        
        textCodOrdine = new JTextField();
        textCodOrdine.setEnabled(false);
        textCodOrdine.setText(oOrd.getRS_NewOrderAcq());
        springLayout.putConstraint(SpringLayout.NORTH, textCodOrdine, 6, SpringLayout.SOUTH, lblSubTitle);
        springLayout.putConstraint(SpringLayout.WEST, textCodOrdine, 6, SpringLayout.EAST, lblCodOrdine);
        frmOrdiniAcquistoGui.getContentPane().add(textCodOrdine);
        textCodOrdine.setColumns(10);
        
        JLabel lblAnno = new JLabel("Anno:");
        springLayout.putConstraint(SpringLayout.WEST, lblAnno, 0, SpringLayout.WEST, lblSubTitle);
        lblAnno.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmOrdiniAcquistoGui.getContentPane().add(lblAnno);
        
        textAnno = new JTextField();
        textAnno.setText("2020");
        springLayout.putConstraint(SpringLayout.NORTH, lblAnno, 0, SpringLayout.NORTH, textAnno);
        springLayout.putConstraint(SpringLayout.NORTH, textAnno, 25, SpringLayout.SOUTH, lblCodOrdine);
        springLayout.putConstraint(SpringLayout.WEST, textAnno, 58, SpringLayout.WEST, frmOrdiniAcquistoGui.getContentPane());
        textAnno.setEnabled(false);
        frmOrdiniAcquistoGui.getContentPane().add(textAnno);
        textAnno.setColumns(10);
        
        JLabel lblQuantit�TOT = new JLabel("Quantit\u00E0 TOT:");
        lblQuantit�TOT.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblQuantit�TOT, 18, SpringLayout.SOUTH, textAnno);
        springLayout.putConstraint(SpringLayout.WEST, lblQuantit�TOT, 10, SpringLayout.WEST, frmOrdiniAcquistoGui.getContentPane());
        frmOrdiniAcquistoGui.getContentPane().add(lblQuantit�TOT);
        
        textQuantit�TOT = new JTextField();
        textQuantit�TOT.setEnabled(false);
        textQuantit�TOT.setText("0");
        springLayout.putConstraint(SpringLayout.WEST, textQuantit�TOT, 0, SpringLayout.WEST, textCodOrdine);
        springLayout.putConstraint(SpringLayout.SOUTH, textQuantit�TOT, 0, SpringLayout.SOUTH, lblQuantit�TOT);
        frmOrdiniAcquistoGui.getContentPane().add(textQuantit�TOT);
        textQuantit�TOT.setColumns(10);
        
        JLabel lblImporto = new JLabel("Importo:");
        lblImporto.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblImporto, 29, SpringLayout.SOUTH, lblQuantit�TOT);
        springLayout.putConstraint(SpringLayout.WEST, lblImporto, 0, SpringLayout.WEST, lblSubTitle);
        frmOrdiniAcquistoGui.getContentPane().add(lblImporto);
        
        textImporto = new JTextField();
        textImporto.setText("0");
        springLayout.putConstraint(SpringLayout.NORTH, textImporto, 0, SpringLayout.NORTH, lblImporto);
        springLayout.putConstraint(SpringLayout.WEST, textImporto, 6, SpringLayout.EAST, lblImporto);
        textImporto.setEnabled(false);
        frmOrdiniAcquistoGui.getContentPane().add(textImporto);
        textImporto.setColumns(10);
        
        JLabel lblCodUffAcq = new JLabel("Codice Ufficio Acquisti:");
        lblCodUffAcq.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblCodUffAcq, 21, SpringLayout.SOUTH, lblImporto);
        springLayout.putConstraint(SpringLayout.WEST, lblCodUffAcq, 0, SpringLayout.WEST, lblSubTitle);
        frmOrdiniAcquistoGui.getContentPane().add(lblCodUffAcq);
        
        textCodUffAcq = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textCodUffAcq, 0, SpringLayout.NORTH, lblCodUffAcq);
        springLayout.putConstraint(SpringLayout.WEST, textCodUffAcq, 6, SpringLayout.EAST, lblCodUffAcq);
        frmOrdiniAcquistoGui.getContentPane().add(textCodUffAcq);
        textCodUffAcq.setColumns(10);
        
        JButton btnInserisci = new JButton("INSERISCI");
        btnInserisci.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Ordine ordA = new Ordine(textCodUffAcq.getText());
                try {
                    oOrd.insertOrdine(ordA);
                    ResultSet rS = oOrd.getRS_Ord("ordine_acquisto");
                    tableOrdAcq.setModel(DbUtils.resultSetToTableModel(rS));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                textCodOrdine.setText(oOrd.getRS_NewOrderAcq());               
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnInserisci, 22, SpringLayout.SOUTH, lblCodUffAcq);
        springLayout.putConstraint(SpringLayout.WEST, btnInserisci, 0, SpringLayout.WEST, lblSubTitle);
        frmOrdiniAcquistoGui.getContentPane().add(btnInserisci);
        
        JButton btnReset = new JButton("RESET");
        btnReset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textCodUffAcq.setText("");
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnReset, 0, SpringLayout.NORTH, btnInserisci);
        springLayout.putConstraint(SpringLayout.WEST, btnReset, 0, SpringLayout.WEST, textCodOrdine);
        frmOrdiniAcquistoGui.getContentPane().add(btnReset);
        
        JLabel lblOrdAcq = new JLabel("Ordini Acquisto:");
        springLayout.putConstraint(SpringLayout.NORTH, lblOrdAcq, 6, SpringLayout.NORTH, lblSubTitle);
        springLayout.putConstraint(SpringLayout.WEST, lblOrdAcq, 278, SpringLayout.EAST, lblSubTitle);
        lblOrdAcq.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmOrdiniAcquistoGui.getContentPane().add(lblOrdAcq);
        
        tableOrdAcq = new JTable() {
            public boolean isCellEditable(int row,int column){  
                return false; 
              }
        };
        JScrollPane scrollTableOrdAcq = new JScrollPane(tableOrdAcq);
        springLayout.putConstraint(SpringLayout.NORTH, scrollTableOrdAcq, 2, SpringLayout.NORTH, lblCodOrdine);
        springLayout.putConstraint(SpringLayout.WEST, scrollTableOrdAcq, 0, SpringLayout.WEST, lblOrdAcq);
        springLayout.putConstraint(SpringLayout.EAST, scrollTableOrdAcq, -131, SpringLayout.EAST, frmOrdiniAcquistoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, tableOrdAcq, 6, SpringLayout.SOUTH, lblOrdAcq);
        springLayout.putConstraint(SpringLayout.WEST, tableOrdAcq, 0, SpringLayout.WEST, lblOrdAcq);
        springLayout.putConstraint(SpringLayout.SOUTH, tableOrdAcq, -347, SpringLayout.SOUTH, frmOrdiniAcquistoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, tableOrdAcq, -588, SpringLayout.EAST, frmOrdiniAcquistoGui.getContentPane());
        frmOrdiniAcquistoGui.getContentPane().add(scrollTableOrdAcq);
        
        JLabel lblPezziAcq = new JLabel("Pezzi Acquistati:");
        springLayout.putConstraint(SpringLayout.SOUTH, lblPezziAcq, -273, SpringLayout.SOUTH, frmOrdiniAcquistoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, lblPezziAcq, 0, SpringLayout.EAST, lblOrdAcq);
        lblPezziAcq.setFont(new Font("Tahoma", Font.BOLD, 14));
        frmOrdiniAcquistoGui.getContentPane().add(lblPezziAcq);
        
        JButton btnVisOrdAcq = new JButton("VISUALIZZA");
        springLayout.putConstraint(SpringLayout.SOUTH, scrollTableOrdAcq, -6, SpringLayout.NORTH, btnVisOrdAcq);
        springLayout.putConstraint(SpringLayout.NORTH, btnVisOrdAcq, 6, SpringLayout.SOUTH, tableOrdAcq);
        btnVisOrdAcq.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = oOrd.getRS_Ord("ordine_acquisto");
                tableOrdAcq.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.WEST, btnVisOrdAcq, 0, SpringLayout.WEST, lblOrdAcq);
        frmOrdiniAcquistoGui.getContentPane().add(btnVisOrdAcq);
        
        tablePezziAcq = new JTable() {
            public boolean isCellEditable(int row,int column){  
                return false; 
              }
        };
        JScrollPane scrollPezziAcq = new JScrollPane(tablePezziAcq);
        springLayout.putConstraint(SpringLayout.NORTH, scrollPezziAcq, 6, SpringLayout.SOUTH, lblPezziAcq);
        springLayout.putConstraint(SpringLayout.WEST, scrollPezziAcq, 0, SpringLayout.WEST, lblOrdAcq);
        springLayout.putConstraint(SpringLayout.EAST, scrollPezziAcq, -131, SpringLayout.EAST, frmOrdiniAcquistoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, tablePezziAcq, 6, SpringLayout.SOUTH, lblPezziAcq);
        springLayout.putConstraint(SpringLayout.SOUTH, tablePezziAcq, -90, SpringLayout.SOUTH, frmOrdiniAcquistoGui.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, tablePezziAcq, -94, SpringLayout.EAST, frmOrdiniAcquistoGui.getContentPane());
        frmOrdiniAcquistoGui.getContentPane().add(scrollPezziAcq);
        
        JButton btnVisPezziAcq = new JButton("VISUALIZZA");
        btnVisPezziAcq.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = oPAcq.getRS_PAcq();
                tablePezziAcq.setModel(DbUtils.resultSetToTableModel(rS));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnVisPezziAcq, 6, SpringLayout.SOUTH, tablePezziAcq);
        springLayout.putConstraint(SpringLayout.EAST, btnVisPezziAcq, 0, SpringLayout.EAST, btnVisOrdAcq);
        frmOrdiniAcquistoGui.getContentPane().add(btnVisPezziAcq);
        
        JLabel lblVisOrdAcq = new JLabel("Visualizza Ordine Acquisto:");
        lblVisOrdAcq.setFont(new Font("Tahoma", Font.BOLD, 20));
        springLayout.putConstraint(SpringLayout.NORTH, lblVisOrdAcq, 33, SpringLayout.SOUTH, btnInserisci);
        springLayout.putConstraint(SpringLayout.WEST, lblVisOrdAcq, 0, SpringLayout.WEST, lblSubTitle);
        frmOrdiniAcquistoGui.getContentPane().add(lblVisOrdAcq);
        
        JLabel lblVisCodAcq = new JLabel("Codice Ordine:");
        springLayout.putConstraint(SpringLayout.SOUTH, scrollPezziAcq, 177, SpringLayout.NORTH, lblVisCodAcq);
        lblVisCodAcq.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblVisCodAcq, 11, SpringLayout.SOUTH, lblVisOrdAcq);
        springLayout.putConstraint(SpringLayout.WEST, lblVisCodAcq, 0, SpringLayout.WEST, lblSubTitle);
        frmOrdiniAcquistoGui.getContentPane().add(lblVisCodAcq);
        
        textVisCodAcq = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textVisCodAcq, -3, SpringLayout.NORTH, tablePezziAcq);
        springLayout.putConstraint(SpringLayout.EAST, textVisCodAcq, 0, SpringLayout.EAST, textCodOrdine);
        frmOrdiniAcquistoGui.getContentPane().add(textVisCodAcq);
        textVisCodAcq.setColumns(10);
        
        JButton btnVisCodAcq = new JButton("VISUALIZZA");
        btnVisCodAcq.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultSet rS = oOrd.getRS_OrdSing("ordine_acquisto", textVisCodAcq.getText());
                ResultSet rS2 = oPAcq.getRS_PAcqInOrd(textVisCodAcq.getText());
                tableOrdAcq.setModel(DbUtils.resultSetToTableModel(rS));
                tablePezziAcq.setModel(DbUtils.resultSetToTableModel(rS2));
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnVisCodAcq, 6, SpringLayout.SOUTH, lblVisCodAcq);
        springLayout.putConstraint(SpringLayout.WEST, btnVisCodAcq, 0, SpringLayout.WEST, lblSubTitle);
        frmOrdiniAcquistoGui.getContentPane().add(btnVisCodAcq);
        
        JLabel lblAddPezzoOrd = new JLabel("Aggiungi Pezzo ad ultimo Ordine:");
        springLayout.putConstraint(SpringLayout.WEST, tablePezziAcq, 132, SpringLayout.EAST, lblAddPezzoOrd);
        springLayout.putConstraint(SpringLayout.NORTH, lblAddPezzoOrd, 463, SpringLayout.NORTH, frmOrdiniAcquistoGui.getContentPane());
        lblAddPezzoOrd.setFont(new Font("Tahoma", Font.BOLD, 20));
        springLayout.putConstraint(SpringLayout.WEST, lblAddPezzoOrd, 0, SpringLayout.WEST, lblSubTitle);
        frmOrdiniAcquistoGui.getContentPane().add(lblAddPezzoOrd);
        
        JLabel lblCodPezzoAdd = new JLabel("Codice Pezzo:");
        lblCodPezzoAdd.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.NORTH, lblCodPezzoAdd, 20, SpringLayout.SOUTH, lblAddPezzoOrd);
        springLayout.putConstraint(SpringLayout.WEST, lblCodPezzoAdd, 0, SpringLayout.WEST, lblSubTitle);
        frmOrdiniAcquistoGui.getContentPane().add(lblCodPezzoAdd);
        
        textAddPezzoOrd = new JTextField();
        springLayout.putConstraint(SpringLayout.WEST, textAddPezzoOrd, 6, SpringLayout.EAST, lblCodPezzoAdd);
        springLayout.putConstraint(SpringLayout.SOUTH, textAddPezzoOrd, 0, SpringLayout.SOUTH, lblCodPezzoAdd);
        frmOrdiniAcquistoGui.getContentPane().add(textAddPezzoOrd);
        textAddPezzoOrd.setColumns(10);
        
        JLabel lblQuantit�Pezzo = new JLabel("Quantit\u00E0:");
        lblQuantit�Pezzo.setFont(new Font("Tahoma", Font.BOLD, 14));
        springLayout.putConstraint(SpringLayout.WEST, lblQuantit�Pezzo, 17, SpringLayout.EAST, textAddPezzoOrd);
        springLayout.putConstraint(SpringLayout.SOUTH, lblQuantit�Pezzo, 0, SpringLayout.SOUTH, lblCodPezzoAdd);
        frmOrdiniAcquistoGui.getContentPane().add(lblQuantit�Pezzo);
        
        textQuantit�Pezzo = new JTextField();
        springLayout.putConstraint(SpringLayout.WEST, textQuantit�Pezzo, 12, SpringLayout.EAST, lblQuantit�Pezzo);
        springLayout.putConstraint(SpringLayout.SOUTH, textQuantit�Pezzo, 0, SpringLayout.SOUTH, lblCodPezzoAdd);
        frmOrdiniAcquistoGui.getContentPane().add(textQuantit�Pezzo);
        textQuantit�Pezzo.setColumns(10);
        
        JButton btnAddPezzoOrd = new JButton("AGGIUNGI");
        btnAddPezzoOrd.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                PAcquistato pAcq = new PAcquistato(textAddPezzoOrd.getText(), textQuantit�Pezzo.getText());
                Integer lastOrdine = Integer.parseInt(oOrd.getRS_NewOrderAcq())-1;
                try {
                    oPAcq.insertPAcq(lastOrdine.toString(), pAcq);
                    int lastOrder = Integer.parseInt(oOrd.getRS_NewOrderAcq())-1;
                    ResultSet rS = oOrd.getRS_OrdSing("ordine_acquisto", Integer.toString(lastOrder));
                    ResultSet rS2 = oPAcq.getRS_PAcqInOrd(Integer.toString(lastOrder));
                    tableOrdAcq.setModel(DbUtils.resultSetToTableModel(rS));
                    tablePezziAcq.setModel(DbUtils.resultSetToTableModel(rS2));
                    
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        springLayout.putConstraint(SpringLayout.NORTH, btnAddPezzoOrd, 13, SpringLayout.SOUTH, lblCodPezzoAdd);
        springLayout.putConstraint(SpringLayout.WEST, btnAddPezzoOrd, 0, SpringLayout.WEST, lblSubTitle);
        frmOrdiniAcquistoGui.getContentPane().add(btnAddPezzoOrd);
    }
    
    public void run() {
        try {
            OrdiniAcquistoGUI window = new OrdiniAcquistoGUI();
            window.frmOrdiniAcquistoGui.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
